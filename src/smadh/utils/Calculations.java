/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package smadh.utils;

/**
 *
 * @author raxon
 */
public final class Calculations {
    public static double calculateDistanceBetweenPoints(int startX,int startY,int endX, int endY){
       return Math.sqrt(Math.pow(startX - endX, 2) + Math.pow(startY - endY, 2));
    }
}
