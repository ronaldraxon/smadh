package smadh.tracer.behavior;

import BESA.ExceptionBESA;
import BESA.Kernell.Agent.Event.DataBESA;
import BESA.Kernell.Agent.Event.EventBESA;
import BESA.Kernell.Agent.GuardBESA;
import BESA.Kernell.Agent.StateBESA;
import BESA.Kernell.System.Directory.AgHandlerBESA;
import BESA.Log.ReportBESA;
import java.util.Map;
import javafx.util.Pair;
import smadh.config.SmadhConstants;
import smadh.control.behavior.ReportTracerGuard;
import smadh.data.ActionData;
import smadh.data.TracerActionData;
import smadh.strategies.Movements;
import smadh.tracer.state.TracerState;
import smadh.utils.Calculations;
import smadh.world.behavior.tracerInteractions.SenseQueryTracerGuard;
import smadh.world.behavior.tracerInteractions.UpdateTracerGuard;

/**
 *
 * @author Raxon
 */
public class TracerSensorGuard extends GuardBESA{

    @Override
    public boolean funcEvalBool(StateBESA objEvalBool) {
        return true;
    }
    
    @Override
    public void funcExecGuard(EventBESA ebesa) {
        TracerActionData data = (TracerActionData) ebesa.getData();
        TracerState state = (TracerState) this.getAgent().getState();
        switch (data.getAction()) {
            case "enable":
                changeToReadyStatus(state);
            break;    
            case "searchAndMark":
                if(isStatusDifferentThanRest(state)){
                    if (isEnergyUnder1Point(state)|| isCharging(state)){
                        reChargeDrone(state);
                    }else if(isJobFinished(state)){
                        returnToBase(state);
                    }else{
                        useDetector(state);
                    }
                }
            break;
            case "evaluate":
                if(isStatusDifferentThanRest(state)){
                    if (isEnergyUnder1Point(state)|| isCharging(state)){
                        reChargeDrone(state);
                    }else if(isJobFinished(state)){
                        returnToBase(state);
                    }else{
                        evaluateAndProceed(data,state);
                    }
                }
            break;
        }
    }
    
    private boolean isStatusDifferentThanRest(TracerState state){
        return !state.getStatus().equals(SmadhConstants.STATUS_REST);
    }
    
    private boolean isJobFinished(TracerState state){
        return state.getDroneMap().size()>SmadhConstants.BOTS_TRACERS_MAX_EXPLORATION_SCORE;
    }
    
    private boolean isEnergyUnder1Point(TracerState state){
        return state.getFuel()<1;
    }
    
    private boolean isCharging(TracerState state){
        return state.getStatus().equals(SmadhConstants.STATUS_CHARGING);
    }
    
    private void reChargeDrone(TracerState state){
        DataBESA dataAction=new ActionData(this.getAgent().getAlias(), "");
        System.out.println(state.getStatus());
        if(!state.getStatus().equals(SmadhConstants.STATUS_CHARGING)){
            state.setStatus(SmadhConstants.STATUS_CHARGING);
            dataAction = new ActionData(this.getAgent().getAlias(), "charging");
        }
        state.setFuel(state.getFuel()+SmadhConstants.BOTS_REFUEL_UNITS_PER_PT);
        if(state.getFuel()>=SmadhConstants.BOTS_TRACERS_FUEL_MAX_CAPACITY){
            state.setStatus(SmadhConstants.STATUS_READY);
            dataAction = new ActionData(this.getAgent().getAlias(), "operating");
        }
        
        reportAction(dataAction);
    }
    
    private void useDetector(TracerState state){
        if(state.getStatus().equals(SmadhConstants.STATUS_READY)){
            changeToDetectingStatus(state);
            consumeEnergyPerPeriod(state);
            senseWorld(state);
        }
    }
    
    private void evaluateAndProceed(TracerActionData data, TracerState state){
        if(data.getX()==-1){
            goAnywhere(state);
        }else{
            getCloseToTarget(data,state);
        }
        insertPointIntoDronesMap(state);
        changeToReadyStatus(state);
    }
    
    private void goAnywhere(TracerState state){
        changeToMovingStatus(state);
        getNewPosition(state);
        consumeEnergyPerPeriod(state);
        moveDroneAndReport(state);
        insertPointIntoDronesMap(state);
    }
    
    private void changeToRestStatus(TracerState state){
        state.setStatus(SmadhConstants.STATUS_REST);
    }
    
    private void changeToDetectingStatus(TracerState state){
        state.setStatus(SmadhConstants.STATUS_DETECTING);
    }
    
    private void changeToReadyStatus(TracerState state){
        state.setStatus(SmadhConstants.STATUS_READY);
    }
    
    private void changeToMovingStatus(TracerState state){
        state.setStatus(SmadhConstants.STATUS_MOVING);
    }
    
    private void changeToReturnToBaseStatus(TracerState state){
        state.setStatus(SmadhConstants.STATUS_RETURNING_TO_BASE);
    }
    
    private void getNewPosition(TracerState state){
        Map <String,Integer> newPosition = Movements.simpleRandomMovement(state.getX(), state.getY());
        state.setX(newPosition.get("x"));
        state.setY(newPosition.get("y"));
    }
    
    private void consumeEnergyPerPeriod(TracerState state){
        state.setFuel(state.getFuel()-SmadhConstants.BOTS_TRACERS_FUEL_CONSUMPTION_PER_PT);
    }
    
    private void moveDroneAndReport(TracerState state){
        DataBESA dataAction;
        dataAction = new ActionData(this.getAgent().getAlias(), "move", state.getX(), state.getY());
        reportAction(dataAction);
    }
    
    private void getCloseToTarget(TracerActionData data, TracerState state){
        changeToMovingStatus(state);
        Pair<Integer,Integer> newStep = getOneStepCloserToTarget(data,state);
        DataBESA dataAction;
        double distance=Calculations.calculateDistanceBetweenPoints(data.getX(), data.getY(), state.getX(), state.getY());
        if(distance ==0 && !data.isMarkedTarget()){
            dataAction = new ActionData(this.getAgent().getAlias(), "mark",state.getX(), state.getY(),data.getEntityType());
        }else{
            dataAction = new ActionData(this.getAgent().getAlias(), "move", newStep.getKey(), newStep.getValue());
            state.setX(newStep.getKey());
            state.setY(newStep.getValue());
        }
        reportAction(dataAction);
        System.out.println("TAMAÑO MAPA "+ state.getDroneMap().size());
    }
    
    private void insertPointIntoDronesMap(TracerState state){
        state.setPointToDroneMap();
    }
    
    private Pair<Integer,Integer> getOneStepCloserToTarget(TracerActionData data, TracerState state){
        int newx = state.getX();
        int newy = state.getY(); 
        int minex = data.getX();
        int miney = data.getY();
        if(minex - newx > 0){
            newx = newx + 1;
        }else if (minex - newx < 0){
            newx = newx - 1;
        }
        if(miney - newy > 0){
            newy = newy + 1;
        }else if(miney - newy < 0){
            newy = newy - 1;
        }
        return new Pair(newx,newy);
    }
    
    private Pair<Integer,Integer> getOneStepCloserToOrigin(TracerState state){
        int newx = state.getX();
        int newy = state.getY(); 
        int destinationx = state.getInitialx();
        int destinationy = state.getInitialy();
        if(destinationx - newx > 0){
            newx = newx + 1;
        }else if (destinationx - newx < 0){
            newx = newx - 1;
        }
        if(destinationy - newy > 0){
            newy = newy + 1;
        }else if(destinationy - newy < 0){
            newy = newy - 1;
        }
        return new Pair(newx,newy);
    }
    
    private void senseWorld(TracerState state){
        DataBESA dataAction;
        dataAction = new TracerActionData(this.getAgent().getAlias(),"sense",state.getX(),state.getY(),
                                                  SmadhConstants.TRACER_DETECTION_RADIUS,"Mines");
        EventBESA senseQueryToWorld = new EventBESA(SenseQueryTracerGuard.class.getName(),dataAction);
        AgHandlerBESA ah;
        try {
            ah = getAgent().getAdmLocal().getHandlerByAlias("WORLD");
            ah.sendEvent(senseQueryToWorld);
        } catch (ExceptionBESA e) {
          ReportBESA.error(e);
        }
    }
    
    private void reportAction(DataBESA dataAction){
        EventBESA graphicUpdateEvent = new EventBESA(UpdateTracerGuard.class.getName(), dataAction);
        EventBESA controlReportEvent = new EventBESA(ReportTracerGuard.class.getName(), dataAction);
        AgHandlerBESA ah;
        try {
            ah = getAgent().getAdmLocal().getHandlerByAlias("WORLD");
            ah.sendEvent(graphicUpdateEvent);
            ah = getAgent().getAdmLocal().getHandlerByAlias("CONTROL");
            ah.sendEvent(controlReportEvent);
        } catch (ExceptionBESA e) {
            ReportBESA.error(e);
        }
    }
    
    private void returnToBase(TracerState state){
        changeToReturnToBaseStatus(state);
        Pair<Integer,Integer> newStep = getOneStepCloserToOrigin(state);
        DataBESA dataAction;
        double distance=Calculations.calculateDistanceBetweenPoints(state.getInitialx(), state.getInitialy(), state.getX(), state.getY());
        if(distance == 0){
            changeToRestStatus(state);
            dataAction = new ActionData(this.getAgent().getAlias(), "rest");
        }else{
            dataAction = new ActionData(this.getAgent().getAlias(), "move", newStep.getKey(), newStep.getValue());
            state.setX(newStep.getKey());
            state.setY(newStep.getValue());
        }
        reportAction(dataAction);
    }
}
