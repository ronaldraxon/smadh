package smadh.tracer.behavior;

import BESA.ExceptionBESA;
import BESA.Kernell.Agent.Event.DataBESA;
import BESA.Kernell.Agent.Event.EventBESA;
import BESA.Kernell.Agent.GuardBESA;
import BESA.Kernell.Agent.StateBESA;
import BESA.Kernell.System.Directory.AgHandlerBESA;
import BESA.Log.ReportBESA;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import javafx.util.Pair;
import smadh.config.SmadhConstants;
import smadh.control.ExplorationPoint;
import smadh.control.ExplorationRoute;
import smadh.control.behavior.CommunicationTracerGuard;
import smadh.control.behavior.ReportTracerGuard;
import smadh.data.ActionData;
import smadh.data.TracerActionData;
import smadh.strategies.Movements;
import smadh.tracer.state.TracerState;
import smadh.utils.Calculations;
import smadh.world.behavior.tracerInteractions.SenseQueryTracerGuard;
import smadh.world.behavior.tracerInteractions.UpdateTracerGuard;

/**
 *
 * @author Raxon
 */
public class TracerCooperativeActionGuard extends GuardBESA{

    @Override
    public boolean funcEvalBool(StateBESA objEvalBool) {
        return true;
    }
    
    @Override
    public void funcExecGuard(EventBESA ebesa) {
        TracerActionData data = (TracerActionData) ebesa.getData();
        TracerState state = (TracerState) this.getAgent().getState();
        System.out.println(state.getStatus());
        switch (data.getAction()) {
            case "enable":
                changeToReadyStatus(state);
            break;
            case "searchAndMark":
                if(isStatusDifferentThanRest(state)|| isStatusDifferentThanRequestingRoute(state)){
                    if (isEnergyUnder1Point(state)|| isCharging(state)){
                        reChargeDrone(state);
                    }else if(isJobFinished(state)|| isStatisReturningToBase(state)){
                        returnToBase(state);
                    }else{
                        searchAndMark(state);
                    }
                }
            break;
            case "assignRoute":
                assignRoute(data,state);
            break;
            case "returnToBase":
                returnToBase(state);
            break;
            case "evaluate":
                if(isStatusDifferentThanRest(state)){
                    if (isEnergyUnder1Point(state)|| isCharging(state)){
                        reChargeDrone(state);
                    }else if(isJobFinished(state)|| isStatisReturningToBase(state)){
                        returnToBase(state);
                    }else{
                        evaluate(data,state);
                    }
                }
            break;
        }
    }
    
    private boolean isStatusDifferentThanRest(TracerState state){
        return !state.getStatus().equals(SmadhConstants.STATUS_REST);
    }
    
    private boolean isStatisReturningToBase(TracerState state){
        return state.getStatus().equals(SmadhConstants.STATUS_RETURNING_TO_BASE);
    }
    
    private boolean isStatusDifferentThanRequestingRoute(TracerState state){
        return !state.getStatus().equals(SmadhConstants.STATUS_REQUESTING_ROUTE);
    }
    
    private boolean isJobFinished(TracerState state){
        return state.getDroneMap().size()>SmadhConstants.BOTS_TRACERS_MAX_EXPLORATION_SCORE;
    }
    
    private boolean isEnergyUnder1Point(TracerState state){
        return state.getFuel()<1;
    }
    
    private boolean isCharging(TracerState state){
        return state.getStatus().equals(SmadhConstants.STATUS_CHARGING);
    }
    
    private void reChargeDrone(TracerState state){
        DataBESA dataAction=new ActionData(this.getAgent().getAlias(), "");
        System.out.println(state.getStatus());
        if(!state.getStatus().equals(SmadhConstants.STATUS_CHARGING)){
            state.setStatus(SmadhConstants.STATUS_CHARGING);
            dataAction = new ActionData(this.getAgent().getAlias(), "charging");
        }
        state.setFuel(state.getFuel()+SmadhConstants.BOTS_REFUEL_UNITS_PER_PT);
        if(state.getFuel()>=SmadhConstants.BOTS_TRACERS_FUEL_MAX_CAPACITY){
            state.setStatus(SmadhConstants.STATUS_READY);
            dataAction = new ActionData(this.getAgent().getAlias(), "operating");
        }
        
        reportAction(dataAction);
    }
    
    private void searchAndMark(TracerState state){
            if(state.getStatus().equals(SmadhConstants.STATUS_READY)){
            changeToDetectingStatus(state);
            consumeEnergyPerPeriod(state);
            senseWorld(state);
         }
    }
    
    private void assignRoute(TracerActionData data, TracerState state){
        if(!state.isAssignedToARoute()){
            state.setExplorationRoute(data.getExplorationRoute());
            state.setAssignedToARoute(true);
            changeToReadyStatus(state);
        }
        
    }
    
    private void evaluate(TracerActionData data, TracerState state){
        if(data.getX()==-1){
            if(state.isAssignedToARoute()){
                goToNextRoutePoint(state);
            }else{
                requestRoute(state);
            }
        }else{
            getCloseToTarget(data,state);
        }
        changeToReadyStatus(state);
    }
    
    private void proceed(TracerState state){
    }
    
    
    private void requestRoute(TracerState state){
        changeToRequestRouteStatus(state);
        DataBESA dataAction;       
        dataAction = new ActionData(this.getAgent().getAlias(), "requestRoute");
        requestInfoToControl(dataAction);
    }
    
    private void goAnywhere(TracerState state){
        changeToMovingStatus(state);
        getNewPosition(state);
        consumeEnergyPerPeriod(state);
        moveDroneAndReport(state);
        insertPointIntoDronesMap(state);
    }
    
    private void goToNextRoutePoint(TracerState state){
        changeToMovingStatus(state);
        setNextRoutePointToTarget(state);
        getCloseToRoutePoint(state);
        //getNewPosition(state);
        consumeEnergyPerPeriod(state);
        //moveDroneAndReport(state);
        insertPointIntoDronesMap(state);
    }
    
    private void setNextRoutePointToTarget(TracerState state){
        ExplorationRoute er = state.getExplorationRoute();
        List<ExplorationPoint> explorationPoints = er.getExplorationpPoints();
        ExplorationPoint epoint = explorationPoints.stream()
                                                   .filter(ep-> !ep.isExplored())
                                                   .findFirst().orElse(null);
        if(Objects.isNull(epoint)){
            er.setCompleted(true);
            state.setExplorationRoute(er);
            state.setAssignedToARoute(false);
            DataBESA dataAction;
            dataAction = new ActionData(this.getAgent().getAlias(),"reportCompletedRoute");
            reportCompletedExplorationRoute(dataAction);
            requestRoute(state);
            //state.setTargetx(state.getInitialx());
            //state.setTargety(state.getInitialy());
        }else{
            state.setTargetpointIndex(explorationPoints.indexOf(epoint));
            state.setTargetx(epoint.getX());
            state.setTargety(epoint.getY());
        }
                
    }
    
    private void changeToRequestRouteStatus(TracerState state){
        state.setStatus(SmadhConstants.STATUS_REQUESTING_ROUTE);
    }
    
    private void changeToRestStatus(TracerState state){
        state.setStatus(SmadhConstants.STATUS_REST);
    }
    
    private void changeToDetectingStatus(TracerState state){
        state.setStatus(SmadhConstants.STATUS_DETECTING);
    }
    
    private void changeToReadyStatus(TracerState state){
        state.setStatus(SmadhConstants.STATUS_READY);
    }
    
    private void changeToMovingStatus(TracerState state){
        state.setStatus(SmadhConstants.STATUS_MOVING);
    }
    
    private void changeToReturnToBaseStatus(TracerState state){
        state.setStatus(SmadhConstants.STATUS_RETURNING_TO_BASE);
    }
    
    private void getNewPosition(TracerState state){
        Map <String,Integer> newPosition = Movements.simpleRandomMovement(state.getX(), state.getY());
        state.setX(newPosition.get("x"));
        state.setY(newPosition.get("y"));
    }
    
    private void consumeEnergyPerPeriod(TracerState state){
        state.setFuel(state.getFuel()-SmadhConstants.BOTS_TRACERS_FUEL_CONSUMPTION_PER_PT);
    }
    
    private void moveDroneAndReport(TracerState state){
        DataBESA dataAction;
        dataAction = new ActionData(this.getAgent().getAlias(), "move", state.getX(), state.getY());
        reportAction(dataAction);
    }
    
    private void getCloseToTarget(TracerActionData data, TracerState state){
        changeToMovingStatus(state);
        Pair<Integer,Integer> newStep = getOneStepCloserToTarget(data,state);
        DataBESA dataAction;
        double distance=Calculations.calculateDistanceBetweenPoints(data.getX(), data.getY(), state.getX(), state.getY());
        if(distance ==0 && !data.isMarkedTarget()){
            dataAction = new ActionData(this.getAgent().getAlias(), "mark",state.getX(), state.getY(),data.getEntityType());
        }else{
            dataAction = new ActionData(this.getAgent().getAlias(), "move", newStep.getKey(), newStep.getValue());
            state.setX(newStep.getKey());
            state.setY(newStep.getValue());
        }
        reportAction(dataAction);
        System.out.println("TAMAÑO MAPA "+ state.getDroneMap().size());
    }
    
    private void getCloseToRoutePoint(TracerState state){
        Pair<Integer,Integer> newStep = getOneStepCloserToTarget(state);
        DataBESA dataAction;
        double distance=Calculations.calculateDistanceBetweenPoints(state.getTargetx(), state.getTargety(), state.getX(), state.getY());
        if(distance ==0){
            dataAction = new ActionData(this.getAgent().getAlias(), "onRoute");
            setPointAsExplored(state);
        }else{
            dataAction = new ActionData(this.getAgent().getAlias(), "move", newStep.getKey(), newStep.getValue());
            state.setX(newStep.getKey());
            state.setY(newStep.getValue());
        }
        reportAction(dataAction);
    }
    
    private void setPointAsExplored(TracerState state){
        ExplorationRoute er = state.getExplorationRoute();
        List<ExplorationPoint> explorationPoints = er.getExplorationpPoints();
        ExplorationPoint epoint = explorationPoints.get(state.getTargetpointIndex());
        epoint.setExplored(true);
        explorationPoints.add(state.getTargetpointIndex(), epoint);
        er.setExplorationPoints(explorationPoints);
        state.setExplorationRoute(er);
    }
    
    private void insertPointIntoDronesMap(TracerState state){
        state.setPointToDroneMap();
    }
    
    private Pair<Integer,Integer> getOneStepCloserToTarget(TracerActionData data, TracerState state){
        int newx = state.getX();
        int newy = state.getY(); 
        int minex = data.getX();
        int miney = data.getY();
        if(minex - newx > 0){
            newx = newx + 1;
        }else if (minex - newx < 0){
            newx = newx - 1;
        }
        if(miney - newy > 0){
            newy = newy + 1;
        }else if(miney - newy < 0){
            newy = newy - 1;
        }
        return new Pair(newx,newy);
    }
    
    private Pair<Integer,Integer> getOneStepCloserToTarget(TracerState state){
        int newx = state.getX();
        int newy = state.getY(); 
        int targetX = state.getTargetx();
        int targetY = state.getTargety();
        if(targetX - newx > 0){
            newx = newx + 1;
        }else if (targetX - newx < 0){
            newx = newx - 1;
        }
        if(targetY - newy > 0){
            newy = newy + 1;
        }else if(targetY - newy < 0){
            newy = newy - 1;
        }
        return new Pair(newx,newy);
    }
    
    
    private Pair<Integer,Integer> getOneStepCloserToOrigin(TracerState state){
        int newx = state.getX();
        int newy = state.getY(); 
        int destinationx = state.getInitialx();
        int destinationy = state.getInitialy();
        if(destinationx - newx > 0){
            newx = newx + 1;
        }else if (destinationx - newx < 0){
            newx = newx - 1;
        }
        if(destinationy - newy > 0){
            newy = newy + 1;
        }else if(destinationy - newy < 0){
            newy = newy - 1;
        }
        return new Pair(newx,newy);
    }
    
    private void senseWorld(TracerState state){
        DataBESA dataAction;
        dataAction = new TracerActionData(this.getAgent().getAlias(),"sense",state.getX(),state.getY(),
                                                  SmadhConstants.TRACER_DETECTION_RADIUS,"Mines");
        EventBESA senseQueryToWorld = new EventBESA(SenseQueryTracerGuard.class.getName(),dataAction);
        AgHandlerBESA ah;
        try {
            ah = getAgent().getAdmLocal().getHandlerByAlias("WORLD");
            ah.sendEvent(senseQueryToWorld);
        } catch (ExceptionBESA e) {
          ReportBESA.error(e);
        }
    }
    
    private void reportAction(DataBESA dataAction){
        EventBESA graphicUpdateEvent = new EventBESA(UpdateTracerGuard.class.getName(), dataAction);
        EventBESA controlReportEvent = new EventBESA(ReportTracerGuard.class.getName(), dataAction);
        AgHandlerBESA ah;
        try {
            ah = getAgent().getAdmLocal().getHandlerByAlias("WORLD");
            ah.sendEvent(graphicUpdateEvent);
            ah = getAgent().getAdmLocal().getHandlerByAlias("CONTROL");
            ah.sendEvent(controlReportEvent);
        } catch (ExceptionBESA e) {
            ReportBESA.error(e);
        }
    }
    
    private void requestInfoToControl(DataBESA dataAction){
        EventBESA requestInfoEvent = new EventBESA(CommunicationTracerGuard.class.getName(), dataAction);
        AgHandlerBESA ah;
        try {
            ah = getAgent().getAdmLocal().getHandlerByAlias("CONTROL");
            ah.sendEvent(requestInfoEvent);
        } catch (ExceptionBESA e) {
            ReportBESA.error(e);
        }
    }
    
    private void reportCompletedExplorationRoute(DataBESA dataAction){
        
        EventBESA requestInfoEvent = new EventBESA(CommunicationTracerGuard.class.getName(), dataAction);
        AgHandlerBESA ah;
        try {
            ah = getAgent().getAdmLocal().getHandlerByAlias("CONTROL");
            ah.sendEvent(requestInfoEvent);
        } catch (ExceptionBESA e) {
            ReportBESA.error(e);
        }
    }
    
    
    private void returnToBase(TracerState state){
        changeToReturnToBaseStatus(state);
        Pair<Integer,Integer> newStep = getOneStepCloserToOrigin(state);
        DataBESA dataAction;
        double distance=Calculations.calculateDistanceBetweenPoints(state.getInitialx(), state.getInitialy(), state.getX(), state.getY());
        if(distance == 0){
            if(!state.isOnRest()){
                    state.setOnRest(true);
                    changeToRestStatus(state);
                    dataAction = new ActionData(this.getAgent().getAlias(),SmadhConstants.BOTS_TRACERS_FUEL_MAX_CAPACITY - state.getFuel(), "rest");
                    reportAction(dataAction);
                }
            
        }else{
            dataAction = new ActionData(this.getAgent().getAlias(), "move", newStep.getKey(), newStep.getValue());
            state.setX(newStep.getKey());
            state.setY(newStep.getValue());
            reportAction(dataAction);
        }
        
    }
}
