package smadh.tracer.state;

import BESA.Kernell.Agent.StateBESA;
import java.util.HashMap;
import java.util.Map;
import javafx.util.Pair;
import smadh.config.SmadhConstants;
import smadh.control.ExplorationRoute;

/**
 *
 * @author Raxon
 */
public class TracerState extends  StateBESA{
    
    private Map<String,Pair<Integer,Integer>> droneMap;
    private ExplorationRoute explorationRoute;
    private boolean assignedToARoute;
    private String roleName;
    private String status;
    private int fuel;
    private int actionForce;
    private int fuelConsumptionPerPeriod;
    private int sizeMap;
    private int initialx;
    private int initialy;
    private int targetx;
    private int targety;
    private int targetpointIndex;
    private int x;
    private int y;
    private int previousX;
    private int previousY;
    private boolean onRest;



    public TracerState(int sizeMap,int x, int y) {
        this.sizeMap = sizeMap;
        this.roleName = "Tracer";
        this.initialx= x;
        this.initialy= y;   
        this.x = x;
        this.y = y;
        this.targetx = x;
        this.targety = y;
        this.previousX = x;
        this.previousY = y;
        this.droneMap = new HashMap<>();
        setPointToDroneMap();
        this.status = SmadhConstants.STATUS_READY;
        this.fuel = SmadhConstants.BOTS_TRACERS_FUEL_MAX_CAPACITY;
        this.actionForce = SmadhConstants.BOTS_TRACERS_ACTION_FORCE;
        this.fuelConsumptionPerPeriod = SmadhConstants.BOTS_TRACERS_FUEL_CONSUMPTION_PER_PT;  
    }
    
    public ExplorationRoute getExplorationRoute() {
        return explorationRoute;
    }

    public void setExplorationRoute(ExplorationRoute explorationRoute) {
        this.explorationRoute = explorationRoute;
    }
    
    public boolean isAssignedToARoute() {
        return assignedToARoute;
    }

    public void setAssignedToARoute(boolean assignedToARoute) {
        this.assignedToARoute = assignedToARoute;
    }
    
    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
    
    public int getFuel() {
        return fuel;
    }

    public void setFuel(int fuel) {
        this.fuel = fuel;
    }

    public int getActionForce() {
        return actionForce;
    }

    public void setActionForce(int actionForce) {
        this.actionForce = actionForce;
    }

    public int getFuelConsumptionPerPeriod() {
        return fuelConsumptionPerPeriod;
    }

    public void setFuelConsumptionPerPeriod(int fuelConsumptionPerPeriod) {
        this.fuelConsumptionPerPeriod = fuelConsumptionPerPeriod;
    }
    
    public int getSizeMap() {
        return sizeMap;
    }

    public void setSizeMap(int sizeMap) {
        this.sizeMap = sizeMap;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }
    
    public int getInitialx() {
        return initialx;
    }

    public void setInitialx(int initialx) {
        this.initialx = initialx;
    }

    public int getInitialy() {
        return initialy;
    }

    public void setInitialy(int initialy) {
        this.initialy = initialy;
    }
    
    public int getPreviousX() {
        return previousX;
    }

    public void setPreviousX(int previousX) {
        this.previousX = previousX;
    }

    public int getPreviousY() {
        return previousY;
    }

    public void setPreviousY(int previousY) {
        this.previousY = previousY;
    }
    
        public int getTargetx() {
        return targetx;
    }

    public void setTargetx(int targetx) {
        this.targetx = targetx;
    }

    public int getTargety() {
        return targety;
    }

    public void setTargety(int targety) {
        this.targety = targety;
    }
    public int getTargetpointIndex() {
        return targetpointIndex;
    }

    public void setTargetpointIndex(int targetpointIndex) {
        this.targetpointIndex = targetpointIndex;
    }
    
    public Map<String, Pair<Integer, Integer>> getDroneMap() {
        return droneMap;
    }

    public void setPointToDroneMap() {
        Pair<Integer,Integer> pair =new Pair(this.x,this.y);
        this.droneMap.put(this.x+";"+this.y, pair);
    }
    
    public boolean isOnRest() {
        return onRest;
    }

    public void setOnRest(boolean onRest) {
        this.onRest = onRest;
    }
    
}
