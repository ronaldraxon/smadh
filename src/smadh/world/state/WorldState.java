package smadh.world.state;

import BESA.Kernell.Agent.StateBESA;
import java.util.ArrayList;
import java.util.List;
import smadh.world.model.WorldMap;
import smadh.config.SmadhConstants;
import smadh.world.model.MineEntity;
import smadh.world.model.PersonEntity;
import smadh.world.model.Slot;
/**
 *
 * @author raxon
 */
public class WorldState extends StateBESA{
    private WorldMap map;
    private List<String> deminersAlias;
    private List<String> sweepersAlias;
    private List<String> tracersAlias;
    private List<String> watchMecsAlias;

    public WorldState() {
        map = new WorldMap();
        
        deminersAlias = new ArrayList<>();
        sweepersAlias = new ArrayList<>();
        tracersAlias = new ArrayList<>();
        watchMecsAlias = new ArrayList<>();
        WorldMap.openInJFrame(map,  SmadhConstants.JAFRAME_WIDTH,
                               SmadhConstants.JAFRAME_HEIGTH);        
    }

    public WorldMap getMap() {
        return map;
    }

    public void setMap(WorldMap map) {
        this.map = map;
    }

    public List<String> getDeminersAlias() {
        return deminersAlias;
    }

    public void setDeminersAlias(List<String> botsAlias) {
        this.deminersAlias = botsAlias;
    }

    
    public List<String> getSweepersAlias() {
        return sweepersAlias;
    }

    public void setSweepersAlias(List<String> sweepersAlias) {
        this.sweepersAlias = sweepersAlias;
    }
    
    public List<String> getTracersAlias() {
        return tracersAlias;
    }

    public void setTracersAlias(List<String> tracersAlias) {
        this.tracersAlias = tracersAlias;
    }
    
    public List<String> getWatchMecsAlias() {
        return watchMecsAlias;
    }

    public void setWatchMecsAlias(List<String> watchMecsAlias) {
        this.watchMecsAlias = watchMecsAlias;
    }
    
    public Slot demineByDeminer(String alias, int entityType,int x, int y) {
        return map.demineByDeminer(alias,entityType,x,y);
    }
    
    public Slot demineBySweeper(String alias, int entityType,int x, int y) {
        return map.demineBySweeper(alias,entityType,x,y);
    }
    
    public Slot clearByWatchMec(String alias, int entityType,int x, int y) {
        return map.clearByWatchMec(alias,x,y);
    }
    
    public Slot startClearingByWatchMec(String alias, int entityType,int x, int y) {
        return map.startClearingByWatchMec(alias,x,y);
    }
    
    
    
    public void putTracerInRest(String alias) {
        map.putTracerInRest(alias);
    }
    
    public void putDeminerInRest(String alias) {
        map.putDeminerInRest(alias);
    }
    
    public void putSweeperInRest(String alias) {
        map.putSweeperInRest(alias);
    }
    
    public void putWatchMecInRest(String alias) {
        map.putWatchMecInRest(alias);
    }
    
    
    
    public void mark(String alias) {
        map.markMine(alias);
    }

    public void moveDeminer(String alias, int x, int y) {
        map.moveDeminer(alias,x,y);
    }
    
    public void moveSweeper(String alias, int x, int y) {
        map.moveSweeper(alias,x,y);
    }
    
    public void moveTracer(String alias, int x, int y) {
        map.moveTracer(alias,x,y);
    }
    
    public void moveWatchMec(String alias, int x, int y) {
        map.moveWatchmec(alias,x,y);
    }
    
    public void moveWatchMecWithPerson(String alias, int x, int y) {
        map.moveWatchmecWithPerson(alias,x,y);
    }
    
    public void clearPersonInSafeArea(String alias, int x, int y){
        map.clearPersonInSafeArea(alias,x,y);
    }
    
    public void resetMap(int size, int numOfDust){
        map.cleanAllMines();
        map.createNewMap();
    }
    
    public MineEntity senseWorldForMines(int x, int y,double senseSize){
        MineEntity mineLocation;
        mineLocation = map.senseMinesInWorld(x,y,senseSize);
        return mineLocation;
    }
    
    public PersonEntity senseWorldForPersons(int x, int y,double senseSize){
        PersonEntity personLocation;
        personLocation = map.sensePersonsInWorld(x,y,senseSize);
        return personLocation;
    }
    
    public List<Slot> senseWorldForAllByDeminer(int x, int y){
        return map.senseAllInWorldByDeminer(x,y);
    }
    
    public List<Slot> senseWorldForAllByWatchMec(int x, int y){
        return map.senseAllInWorldByWatchMec(x,y);
    }
    
    public Slot tryToPresetSlot(String alias,int x, int y){
        return map.tryToPresetSlotDeminer(alias, x, y);
    }
    
    public Slot tryToPresetForWatchMec(String alias,int x, int y){
        return map.tryToPresetSlotForWatchMec(alias, x, y);
    }
    
    public List<Slot> senseWorldForAllBySweeper(int x, int y){
        return map.senseAllInWorldBySweeper(x,y);
    }
    
   public PersonEntity senseWorldForPersons(String alias, int x, int y,double senseSize){
        
        return null;
    }
}
