package smadh.world.behavior.tracerInteractions;

import BESA.Kernell.Agent.Event.EventBESA;
import BESA.Kernell.Agent.GuardBESA;
import smadh.data.ActionData;
import smadh.world.state.WorldState;

/**
 *
 * @author Raxon
 */
public class UpdateTracerGuard extends GuardBESA{

    
    @Override
    public void funcExecGuard(EventBESA ebesa) {
        ActionData data = (ActionData) ebesa.getData();
        WorldState state = (WorldState)this.getAgent().getState();
        switch (data.getAction()) {
            case "rest":
                state.putTracerInRest(data.getAlias());
                break;
            case "mark":
                state.mark(data.getAlias());
                break;
            case "move":
                state.moveTracer(data.getAlias(), data.getX(), data.getY());
                break;
        }
    }
    
}
