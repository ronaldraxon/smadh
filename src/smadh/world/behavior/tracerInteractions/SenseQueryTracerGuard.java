package smadh.world.behavior.tracerInteractions;

import BESA.ExceptionBESA;
import BESA.Kernell.Agent.Event.DataBESA;
import BESA.Kernell.Agent.Event.EventBESA;
import BESA.Kernell.Agent.GuardBESA;
import BESA.Kernell.System.Directory.AgHandlerBESA;
import BESA.Log.ReportBESA;
import smadh.config.SmadhConstants;
import smadh.data.TracerActionData;
import smadh.tracer.behavior.TracerCooperativeActionGuard;
import smadh.tracer.behavior.TracerSensorGuard;
import smadh.world.model.MineEntity;
import smadh.world.model.PersonEntity;
import smadh.world.state.WorldState;

/**
 *
 * @author Raxon
 */
public class SenseQueryTracerGuard extends GuardBESA{

    
    @Override
    public void funcExecGuard(EventBESA ebesa) {
        TracerActionData data = (TracerActionData) ebesa.getData();
        WorldState state = (WorldState)this.getAgent().getState();
        switch (data.getAction()) {
            case "sense":
                senseWorldAndAnswer(state,data);
                break;
        }
    }
    
    private void senseWorldAndAnswer(WorldState state,TracerActionData data){
        switch (data.getSenseType()) {
            case "Mines":
                senseWorldForMines(state,data);
                break;    
        }
    }
    
    private void senseWorldForMines(WorldState state,TracerActionData data){
                MineEntity mine = state.senseWorldForMines(data.getX(), data.getY(),data.getSenseSize());
                DataBESA dataAction;
                AgHandlerBESA ah;
                dataAction = new TracerActionData(mine.getXpos(),mine.getYpos(),"evaluate",mine.getType(),mine.isMarked());
                EventBESA queryAnswerToTracer;
                if(SmadhConstants.ENABLE_COOPERATION){
                    queryAnswerToTracer = new EventBESA(TracerCooperativeActionGuard.class.getName(),dataAction);
                }else{
                    queryAnswerToTracer = new EventBESA(TracerSensorGuard.class.getName(),dataAction);
                }
                
                try {
                    ah = getAgent().getAdmLocal().getHandlerByAlias(data.getAlias());
                    ah.sendEvent(queryAnswerToTracer);
                } catch (ExceptionBESA e) {
                    ReportBESA.error(e);
                }
    }
    
}
