/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package smadh.world.behavior.deminerInteractions;

import BESA.ExceptionBESA;
import BESA.Kernell.Agent.Event.DataBESA;
import BESA.Kernell.Agent.Event.EventBESA;
import BESA.Kernell.Agent.GuardBESA;
import BESA.Kernell.System.Directory.AgHandlerBESA;
import BESA.Log.ReportBESA;
import smadh.config.SmadhConstants;
import smadh.data.ActionData;
import smadh.data.DeminerActionData;
import smadh.deminer.behavior.DeminerCooperativeActionGuard;
import smadh.deminer.behavior.DeminerSensorGuard;
import smadh.world.model.Slot;
import smadh.world.state.WorldState;

/**
 *
 * @author Raxon
 */
public class UpdateDeminerGuard extends GuardBESA{

    
    @Override
    public void funcExecGuard(EventBESA ebesa) {
        ActionData data = (ActionData) ebesa.getData();
        WorldState state = (WorldState)this.getAgent().getState();
        switch (data.getAction()) {
            case "rest":
                liberateDeminer(data,state);
                break;
            case "cleared":
                liberateDeminer(data,state);
                break;
            case "demine":
                demine(data,state);
                break;
            case "move":
                moveDeminer(data,state);
                break;
        }
    }
    
    public void putDeminerInRest(ActionData data,WorldState state){
        
    }
    
    public void moveDeminer(ActionData data,WorldState state){
        state.moveDeminer(data.getAlias(), data.getX(), data.getY());
    }
    
    public void demine(ActionData data,WorldState state){
        Slot slot = state.demineByDeminer(data.getAlias(),data.getEntityType(),data.getX(),data.getY());
        if(slot.isInhabited()){
            DataBESA dataAction;
            dataAction = new DeminerActionData(slot.getEntityAlias(),"demine");
            EventBESA queryAnswerToDeminer;
            if(SmadhConstants.ENABLE_COOPERATION){
                queryAnswerToDeminer = new EventBESA(DeminerCooperativeActionGuard.class.getName(),dataAction);
            }else{
                queryAnswerToDeminer = new EventBESA(DeminerSensorGuard.class.getName(),dataAction);
            }
            AgHandlerBESA ah;
                try {
                    ah = getAgent().getAdmLocal().getHandlerByAlias(data.getAlias());
                    ah.sendEvent(queryAnswerToDeminer);
                } catch (ExceptionBESA e) {
                    ReportBESA.error(e);
                }
        }else{
            DataBESA dataAction;
            dataAction = new DeminerActionData(slot.getClearedByAlias(),"cleared",slot.getX(),slot.getY(),data.getEntityType());
            EventBESA queryAnswerToDeminer;
            if(SmadhConstants.ENABLE_COOPERATION){
                queryAnswerToDeminer = new EventBESA(DeminerCooperativeActionGuard.class.getName(),dataAction);
            }else{
                queryAnswerToDeminer = new EventBESA(DeminerSensorGuard.class.getName(),dataAction);
            }
            AgHandlerBESA ah;
                try {
                    ah = getAgent().getAdmLocal().getHandlerByAlias(data.getAlias());
                    ah.sendEvent(queryAnswerToDeminer);
                } catch (ExceptionBESA e) {
                    ReportBESA.error(e);
                }
        }
    }
    
    public void liberateDeminer(ActionData data,WorldState state){
        state.putDeminerInRest(data.getAlias());
    }
    
}
