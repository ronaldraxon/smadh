package smadh.world.behavior.deminerInteractions;

import BESA.ExceptionBESA;
import BESA.Kernell.Agent.Event.DataBESA;
import BESA.Kernell.Agent.Event.EventBESA;
import BESA.Kernell.Agent.GuardBESA;
import BESA.Kernell.System.Directory.AgHandlerBESA;
import BESA.Log.ReportBESA;
import java.util.List;
import java.util.Objects;
import smadh.config.SmadhConstants;
import smadh.data.DeminerActionData;
import smadh.data.DeminerActionData;
import smadh.deminer.behavior.DeminerCooperativeActionGuard;
import smadh.deminer.behavior.DeminerSensorGuard;
import smadh.world.model.MineEntity;
import smadh.world.model.Slot;
import smadh.world.state.WorldState;

/**
 *
 * @author Raxon
 */
public class SenseQueryDeminerGuard extends GuardBESA{

    
    @Override
    public void funcExecGuard(EventBESA ebesa) {
        DeminerActionData data = (DeminerActionData) ebesa.getData();
        WorldState state = (WorldState)this.getAgent().getState();
        switch (data.getAction()) {
            case "sense":
                senseWorldAndAnswer(state,data);
                break;
            case "tryToPresetSlot":
                tryTopresetSlot(state,data);
                break;
        }
    }
    
    private void tryTopresetSlot(WorldState state,DeminerActionData data){
        Slot slot = state.tryToPresetSlot(data.getAlias(),data.getX(),data.getY());
        DataBESA dataAction;
        if(Objects.isNull(slot)){
             dataAction = new DeminerActionData("waitToMoveLater");   
        }else{
             dataAction = new DeminerActionData("moveAllowed");
        }
        EventBESA queryAnswerToDeminer;
        if(SmadhConstants.ENABLE_COOPERATION){
                    queryAnswerToDeminer = new EventBESA(DeminerCooperativeActionGuard.class.getName(),
                                                            dataAction);
        }else{
                    queryAnswerToDeminer = new EventBESA(DeminerSensorGuard.class.getName(),
                                                            dataAction);
        }
        
        //EventBESA queryAnswerToDeminer = new EventBESA(DeminerSensorGuard.class.getName(),dataAction);
        AgHandlerBESA ah;
        try {
            ah = getAgent().getAdmLocal().getHandlerByAlias(data.getAlias());
            ah.sendEvent(queryAnswerToDeminer);
            } catch (ExceptionBESA e) {
              ReportBESA.error(e);
        }
    }
    
    private void senseWorldAndAnswer(WorldState state,DeminerActionData data){
        switch (data.getSenseType()) {
            case "All":
                senseWorldForAll(state,data);
                break;
        }
    }

    
    private void senseWorldForAll(WorldState state,DeminerActionData data){
                List<Slot> slots = state.senseWorldForAllByDeminer(data.getX(), data.getY());
                DataBESA dataAction;
                dataAction = new DeminerActionData(slots,"evaluate");
                EventBESA queryAnswerToDeminer;
                if(SmadhConstants.ENABLE_COOPERATION){
                    queryAnswerToDeminer = new EventBESA(DeminerCooperativeActionGuard.class.getName(),
                                                            dataAction);
                }else{
                    queryAnswerToDeminer = new EventBESA(DeminerSensorGuard.class.getName(),
                                                            dataAction);
                }
                
                AgHandlerBESA ah;
                try {
                    ah = getAgent().getAdmLocal().getHandlerByAlias(data.getAlias());
                    ah.sendEvent(queryAnswerToDeminer);
                } catch (ExceptionBESA e) {
                    ReportBESA.error(e);
                }
    }
    
}
