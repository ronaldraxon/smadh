/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package smadh.world.behavior.sweeperInteractions;

import BESA.ExceptionBESA;
import BESA.Kernell.Agent.Event.DataBESA;
import BESA.Kernell.Agent.Event.EventBESA;
import BESA.Kernell.Agent.GuardBESA;
import BESA.Kernell.System.Directory.AgHandlerBESA;
import BESA.Log.ReportBESA;
import smadh.config.SmadhConstants;
import smadh.data.ActionData;
import smadh.data.SweeperActionData;
import smadh.sweeper.behavior.SweeperCooperativeActionGuard;
import smadh.sweeper.behavior.SweeperSensorGuard;

import smadh.world.model.Slot;
import smadh.world.state.WorldState;

/**
 *
 * @author Raxon
 */
public class UpdateSweeperGuard extends GuardBESA{

    
        
    @Override
    public void funcExecGuard(EventBESA ebesa) {
        ActionData data = (ActionData) ebesa.getData();
        WorldState state = (WorldState)this.getAgent().getState();
        switch (data.getAction()) {
            case "rest":
                liberateSweeper(data,state);
                break;
            case "cleared":
                liberateSweeper(data,state);
                break;
            case "demine":
                demine(data,state);
                break;
            case "move":
                moveSweeper(data,state);
                break;
        }
    }
    
    public void putSweeperInRest(ActionData data,WorldState state){
        
    }
    
    public void moveSweeper(ActionData data,WorldState state){
        state.moveSweeper(data.getAlias(), data.getX(), data.getY());
    }
    
    public void demine(ActionData data,WorldState state){
        Slot slot = state.demineBySweeper(data.getAlias(),data.getEntityType(),data.getX(),data.getY());
        if(slot.isInhabited()){
            DataBESA dataAction;
            dataAction = new SweeperActionData(slot.getEntityAlias(),"demine");
            EventBESA queryAnswerToSweeper;
            if(SmadhConstants.ENABLE_COOPERATION){
                queryAnswerToSweeper = new EventBESA(SweeperCooperativeActionGuard.class.getName(),dataAction);
            }else{
                queryAnswerToSweeper = new EventBESA(SweeperSensorGuard.class.getName(),dataAction);
            }
            AgHandlerBESA ah;
                try {
                    ah = getAgent().getAdmLocal().getHandlerByAlias(data.getAlias());
                    ah.sendEvent(queryAnswerToSweeper);
                } catch (ExceptionBESA e) {
                    ReportBESA.error(e);
                }
        }else{
            DataBESA dataAction;
            dataAction = new SweeperActionData(slot.getClearedByAlias(),"cleared",slot.getX(),slot.getY(),data.getEntityType());
            EventBESA queryAnswerToSweeper;
            if(SmadhConstants.ENABLE_COOPERATION){
                queryAnswerToSweeper = new EventBESA(SweeperCooperativeActionGuard.class.getName(),dataAction);
            }else{
                queryAnswerToSweeper = new EventBESA(SweeperSensorGuard.class.getName(),dataAction);
            }
            AgHandlerBESA ah;
                try {
                    ah = getAgent().getAdmLocal().getHandlerByAlias(data.getAlias());
                    ah.sendEvent(queryAnswerToSweeper);
                } catch (ExceptionBESA e) {
                    ReportBESA.error(e);
                }
        }
    }
    
    public void liberateSweeper(ActionData data,WorldState state){
        state.putSweeperInRest(data.getAlias());
    }
}
