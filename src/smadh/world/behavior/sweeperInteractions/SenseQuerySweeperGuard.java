package smadh.world.behavior.sweeperInteractions;

import BESA.ExceptionBESA;
import BESA.Kernell.Agent.Event.DataBESA;
import BESA.Kernell.Agent.Event.EventBESA;
import BESA.Kernell.Agent.GuardBESA;
import BESA.Kernell.System.Directory.AgHandlerBESA;
import BESA.Log.ReportBESA;
import java.util.List;
import java.util.Objects;
import smadh.config.SmadhConstants;
import smadh.data.SweeperActionData;
import smadh.data.SweeperActionData;
import smadh.sweeper.behavior.SweeperCooperativeActionGuard;
import smadh.sweeper.behavior.SweeperSensorGuard;
import smadh.sweeper.behavior.SweeperSensorGuard;
import smadh.world.model.Slot;
import smadh.world.state.WorldState;

/**
 *
 * @author Raxon
 */
public class SenseQuerySweeperGuard extends GuardBESA{

    
 @Override
    public void funcExecGuard(EventBESA ebesa) {
        SweeperActionData data = (SweeperActionData) ebesa.getData();
        WorldState state = (WorldState)this.getAgent().getState();
        switch (data.getAction()) {
            case "sense":
                senseWorldAndAnswer(state,data);
                break;
            case "tryToPresetSlot":
                tryTopresetSlot(state,data);
                break;
        }
    }
    
    private void tryTopresetSlot(WorldState state,SweeperActionData data){
        Slot slot = state.tryToPresetSlot(data.getAlias(),data.getX(),data.getY());
        DataBESA dataAction;
        if(Objects.isNull(slot)){
             dataAction = new SweeperActionData("waitToMoveLater");   
        }else{
             dataAction = new SweeperActionData("moveAllowed");
        }
        EventBESA queryAnswerToSweeper;
        if(SmadhConstants.ENABLE_COOPERATION){
                    queryAnswerToSweeper = new EventBESA(SweeperCooperativeActionGuard.class.getName(),
                                                            dataAction);
        }else{
                    queryAnswerToSweeper = new EventBESA(SweeperSensorGuard.class.getName(),
                                                            dataAction);
        }
        
        //EventBESA queryAnswerToSweeper = new EventBESA(SweeperSensorGuard.class.getName(),dataAction);
        AgHandlerBESA ah;
        try {
            ah = getAgent().getAdmLocal().getHandlerByAlias(data.getAlias());
            ah.sendEvent(queryAnswerToSweeper);
            } catch (ExceptionBESA e) {
              ReportBESA.error(e);
        }
    }
    
    private void senseWorldAndAnswer(WorldState state,SweeperActionData data){
        switch (data.getSenseType()) {
            case "All":
                senseWorldForAll(state,data);
                break;
        }
    }

    
    private void senseWorldForAll(WorldState state,SweeperActionData data){
                List<Slot> slots = state.senseWorldForAllBySweeper(data.getX(), data.getY());
                DataBESA dataAction;
                dataAction = new SweeperActionData(slots,"evaluate");
                EventBESA queryAnswerToSweeper;
                if(SmadhConstants.ENABLE_COOPERATION){
                    queryAnswerToSweeper = new EventBESA(SweeperCooperativeActionGuard.class.getName(),
                                                            dataAction);
                }else{
                    queryAnswerToSweeper = new EventBESA(SweeperSensorGuard.class.getName(),
                                                            dataAction);
                }
                
                AgHandlerBESA ah;
                try {
                    ah = getAgent().getAdmLocal().getHandlerByAlias(data.getAlias());
                    ah.sendEvent(queryAnswerToSweeper);
                } catch (ExceptionBESA e) {
                    ReportBESA.error(e);
                }
    }
}
