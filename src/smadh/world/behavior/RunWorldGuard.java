package smadh.world.behavior;

import BESA.ExceptionBESA;
import BESA.Kernell.Agent.Event.DataBESA;
import BESA.Kernell.Agent.Event.EventBESA;
import BESA.Kernell.Agent.PeriodicGuardBESA;
import BESA.Kernell.System.Directory.AgHandlerBESA;
import BESA.Log.ReportBESA;
import smadh.config.SmadhConstants;
import smadh.deminer.behavior.DeminerSensorGuard;

import smadh.data.DeminerActionData;
import smadh.data.SweeperActionData;
import smadh.data.TracerActionData;
import smadh.data.WatchMecActionData;
import smadh.deminer.behavior.DeminerCooperativeActionGuard;
import smadh.sweeper.behavior.SweeperCooperativeActionGuard;
import smadh.sweeper.behavior.SweeperSensorGuard;
import smadh.tracer.behavior.TracerCooperativeActionGuard;
import smadh.tracer.behavior.TracerSensorGuard;
import smadh.watchmec.behavior.WatchMecCooperativeActionGuard;
import smadh.watchmec.behavior.WatchMecSensorGuard;
import smadh.world.state.WorldState;

/**
 *
 * @author Raxon
 */
public class RunWorldGuard extends PeriodicGuardBESA{

    @Override
    public void funcPeriodicExecGuard(EventBESA ebesa) {
        WorldState ws = (WorldState)this.getAgent().getState();
        if(SmadhConstants.ENABLE_COOPERATION){
            for (int i = 0; i < ws.getTracersAlias().size(); i++) {
                DataBESA data = new TracerActionData(ws.getTracersAlias().get(i),"searchAndMark");
                EventBESA event = new EventBESA(TracerCooperativeActionGuard.class.getName(), data);
                AgHandlerBESA ah;
                try {
                    ah = getAgent().getAdmLocal().getHandlerByAlias(ws.getTracersAlias().get(i));
                    ah.sendEvent(event);
                } catch (ExceptionBESA e) {
                    ReportBESA.error(e);
                }
            }
            for (int i = 0; i < ws.getDeminersAlias().size(); i++) {
            DataBESA data = new DeminerActionData(ws.getDeminersAlias().get(i),"search");
            EventBESA event = new EventBESA(DeminerCooperativeActionGuard.class.getName(), data);
            AgHandlerBESA ah;
                try {
                    ah = getAgent().getAdmLocal().getHandlerByAlias(ws.getDeminersAlias().get(i));
                    ah.sendEvent(event);
                } catch (ExceptionBESA e) {
                    ReportBESA.error(e);
                }
            }
            for (int i = 0; i < ws.getSweepersAlias().size(); i++) {
            DataBESA data = new SweeperActionData(ws.getSweepersAlias().get(i),"search");
            EventBESA event = new EventBESA(SweeperCooperativeActionGuard.class.getName(), data);
            AgHandlerBESA ah;
                try {
                    ah = getAgent().getAdmLocal().getHandlerByAlias(ws.getSweepersAlias().get(i));
                    ah.sendEvent(event);
                } catch (ExceptionBESA e) {
                    ReportBESA.error(e);
                }
            }
            for (int i = 0; i < ws.getWatchMecsAlias().size(); i++) {
                DataBESA data = new WatchMecActionData(ws.getWatchMecsAlias().get(i),"search");
                EventBESA event = new EventBESA(WatchMecCooperativeActionGuard.class.getName(), data);
                AgHandlerBESA ah;
                try {
                    ah = getAgent().getAdmLocal().getHandlerByAlias(ws.getWatchMecsAlias().get(i));
                    ah.sendEvent(event);
                } catch (ExceptionBESA e) {
                    ReportBESA.error(e);
                }
            }
            
        }else{
            for (int i = 0; i < ws.getTracersAlias().size(); i++) {
            DataBESA data = new TracerActionData(ws.getTracersAlias().get(i),"searchAndMark");
            EventBESA event = new EventBESA(TracerSensorGuard.class.getName(), data);
            AgHandlerBESA ah;
            try {
                ah = getAgent().getAdmLocal().getHandlerByAlias(ws.getTracersAlias().get(i));
                ah.sendEvent(event);
            } catch (ExceptionBESA e) {
                ReportBESA.error(e);
            }
        }
        for (int i = 0; i < ws.getDeminersAlias().size(); i++) {
            DataBESA data = new DeminerActionData(ws.getDeminersAlias().get(i),"search");
            EventBESA event = new EventBESA(DeminerSensorGuard.class.getName(), data);
            AgHandlerBESA ah;
            try {
                ah = getAgent().getAdmLocal().getHandlerByAlias(ws.getDeminersAlias().get(i));
                ah.sendEvent(event);
            } catch (ExceptionBESA e) {
                ReportBESA.error(e);
            }
        }
        for (int i = 0; i < ws.getSweepersAlias().size(); i++) {
            DataBESA data = new SweeperActionData(ws.getSweepersAlias().get(i),"search");
            EventBESA event = new EventBESA(SweeperSensorGuard.class.getName(), data);
            AgHandlerBESA ah;
            try {
                ah = getAgent().getAdmLocal().getHandlerByAlias(ws.getSweepersAlias().get(i));
                ah.sendEvent(event);
            } catch (ExceptionBESA e) {
                ReportBESA.error(e);
            }
        }
        for (int i = 0; i < ws.getWatchMecsAlias().size(); i++) {
            DataBESA data = new WatchMecActionData(ws.getWatchMecsAlias().get(i),"search");
            EventBESA event = new EventBESA(WatchMecSensorGuard.class.getName(), data);
            AgHandlerBESA ah;
            try {
                ah = getAgent().getAdmLocal().getHandlerByAlias(ws.getWatchMecsAlias().get(i));
                ah.sendEvent(event);
            } catch (ExceptionBESA e) {
                ReportBESA.error(e);
            }
        }
        }
        
    }
    
}
