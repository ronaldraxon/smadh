package smadh.world.behavior.watchmecInteractions;

import BESA.ExceptionBESA;
import BESA.Kernell.Agent.Event.DataBESA;
import BESA.Kernell.Agent.Event.EventBESA;
import BESA.Kernell.Agent.GuardBESA;
import BESA.Kernell.System.Directory.AgHandlerBESA;
import BESA.Log.ReportBESA;
import smadh.config.SmadhConstants;
import smadh.data.ActionData;
import smadh.data.DeminerActionData;
import smadh.data.WatchMecActionData;
import smadh.deminer.behavior.DeminerSensorGuard;
import smadh.watchmec.behavior.WatchMecCooperativeActionGuard;
import smadh.watchmec.behavior.WatchMecSensorGuard;
import smadh.world.model.Slot;
import smadh.world.state.WorldState;

/**
 *
 * @author Raxon
 */
public class UpdateWatchMecGuard extends GuardBESA{

    
    public void funcExecGuard(EventBESA ebesa) {
        ActionData data = (ActionData) ebesa.getData();
        WorldState state = (WorldState)this.getAgent().getState();
        switch (data.getAction()) {
            case "rest":
                state.putWatchMecInRest(data.getAlias());
                break;
            case "clear":
                clear(data,state);
                break;
            case "startClearing":
                startClearing(data,state);
                break;
            case "move":
                state.moveWatchMec(data.getAlias(), data.getX(), data.getY());
                break;
            case "moveWithPerson":
                state.moveWatchMecWithPerson(data.getAlias(), data.getX(), data.getY());
                break;
            case "clearPersonInSafeArea":
                state.clearPersonInSafeArea(data.getAlias(), data.getX(), data.getY());
                break;
        }
    }
    
    
    public void clear(ActionData data,WorldState state){
        Slot slot = state.clearByWatchMec(data.getAlias(),data.getEntityType(),data.getX(),data.getY());
        if(slot.isInhabited()){
            DataBESA dataAction;
            dataAction = new WatchMecActionData(slot.getEntityAlias(),"clear");// Movement to the base
            EventBESA queryAnswerToWatchMec;
                if(SmadhConstants.ENABLE_COOPERATION){
                    queryAnswerToWatchMec = new EventBESA(WatchMecCooperativeActionGuard.class.getName(),dataAction);
                }else{
                    queryAnswerToWatchMec = new EventBESA(WatchMecSensorGuard.class.getName(),dataAction);
                }
            AgHandlerBESA ah;
                try {
                    ah = getAgent().getAdmLocal().getHandlerByAlias(data.getAlias());
                    ah.sendEvent(queryAnswerToWatchMec);
                } catch (ExceptionBESA e) {
                    ReportBESA.error(e);
                }
        }else{
            DataBESA dataAction;
            //Change the watchmec pic to that one with the watchmec alone
            dataAction = new WatchMecActionData(slot.getClearedByAlias(),"cleared",slot.getX(),slot.getY(),data.getEntityType());
            EventBESA queryAnswerToWatchMec;
            if(SmadhConstants.ENABLE_COOPERATION){
                    queryAnswerToWatchMec = new EventBESA(WatchMecCooperativeActionGuard.class.getName(),dataAction);
                }else{
                    queryAnswerToWatchMec = new EventBESA(WatchMecSensorGuard.class.getName(),dataAction);
                }
            AgHandlerBESA ah;
                try {
                    ah = getAgent().getAdmLocal().getHandlerByAlias(data.getAlias());
                    ah.sendEvent(queryAnswerToWatchMec);
                } catch (ExceptionBESA e) {
                    ReportBESA.error(e);
                }
        }
    }
    
    public void startClearing(ActionData data,WorldState state){
        //Here is where the pic of the drone has to change to that one with the farmer
        Slot slot = state.startClearingByWatchMec(data.getAlias(),data.getEntityType(),data.getX(),data.getY());
            DataBESA dataAction;
            dataAction = new WatchMecActionData(slot.getEntityAlias(),"carryPerson");// Movement to the base
            EventBESA queryAnswerToWatchMec;
                if(SmadhConstants.ENABLE_COOPERATION){
                    queryAnswerToWatchMec = new EventBESA(WatchMecCooperativeActionGuard.class.getName(),dataAction);
                }else{
                    queryAnswerToWatchMec = new EventBESA(WatchMecSensorGuard.class.getName(),dataAction);
                }
            AgHandlerBESA ah;
                try {
                    ah = getAgent().getAdmLocal().getHandlerByAlias(data.getAlias());
                    ah.sendEvent(queryAnswerToWatchMec);
                } catch (ExceptionBESA e) {
                    ReportBESA.error(e);
                }

    }
    
}
