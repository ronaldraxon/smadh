package smadh.world.behavior.watchmecInteractions;

import smadh.world.behavior.tracerInteractions.*;
import BESA.ExceptionBESA;
import BESA.Kernell.Agent.Event.DataBESA;
import BESA.Kernell.Agent.Event.EventBESA;
import BESA.Kernell.Agent.GuardBESA;
import BESA.Kernell.System.Directory.AgHandlerBESA;
import BESA.Log.ReportBESA;
import java.util.List;
import java.util.Objects;
import smadh.config.SmadhConstants;
import smadh.data.DeminerActionData;
import smadh.data.TracerActionData;
import smadh.data.WatchMecActionData;
import smadh.deminer.behavior.DeminerCooperativeActionGuard;
import smadh.deminer.behavior.DeminerSensorGuard;
import smadh.tracer.behavior.TracerSensorGuard;
import smadh.watchmec.behavior.WatchMecCooperativeActionGuard;
import smadh.watchmec.behavior.WatchMecSensorGuard;
import smadh.world.model.MineEntity;
import smadh.world.model.PersonEntity;
import smadh.world.model.Slot;
import smadh.world.state.WorldState;

/**
 *
 * @author Raxon
 */
public class SenseQueryWatchMecGuard extends GuardBESA{

    
    @Override
    public void funcExecGuard(EventBESA ebesa) {
        WatchMecActionData data = (WatchMecActionData) ebesa.getData();
        WorldState state = (WorldState)this.getAgent().getState();
        switch (data.getAction()) {
            case "sense":
                senseWorldAndAnswer(state,data);
                break;
            case "senseForFreeSlots":
                senseWorldForSafeSlots(state,data);
                break;
            case "tryToPresetSlot":
                tryTopresetSlot(state,data);
                break;
            
        }
    } 
    
    private void tryTopresetSlot(WorldState state,WatchMecActionData data){
        Slot slot = state.tryToPresetForWatchMec(data.getAlias(),data.getX(),data.getY());
        DataBESA dataAction;
        if(Objects.isNull(slot)){
             dataAction = new WatchMecActionData("executeNextMoveToSavePerson",false);   
        }else{
             dataAction = new WatchMecActionData(slot.getX(),slot.getY(),"executeNextMoveToSavePerson",true); 
        }
        EventBESA queryAnswerToWatchMec = new EventBESA(WatchMecCooperativeActionGuard.class.getName(),dataAction);
        AgHandlerBESA ah;
        try {
            ah = getAgent().getAdmLocal().getHandlerByAlias(data.getAlias());
            ah.sendEvent(queryAnswerToWatchMec);
            } catch (ExceptionBESA e) {
              ReportBESA.error(e);
        }
    }
    
    private void senseWorldForSafeSlots(WorldState state,WatchMecActionData data){
        List<Slot> slots = state.senseWorldForAllByWatchMec(data.getX(), data.getY());
        DataBESA dataAction;
                dataAction = new WatchMecActionData(slots,"executeNextMoveToSavePerson");
                EventBESA queryAnswerToWatchmec = new EventBESA(WatchMecCooperativeActionGuard.class.getName(),dataAction);
                AgHandlerBESA ah;
                try {
                    ah = getAgent().getAdmLocal().getHandlerByAlias(data.getAlias());
                    ah.sendEvent(queryAnswerToWatchmec);
                } catch (ExceptionBESA e) {
                    ReportBESA.error(e);
                }
    }
    
    private void senseWorldAndAnswer(WorldState state,WatchMecActionData data){
        switch (data.getSenseType()) {
            case "Persons":
                senseWorldForPersons(state,data);
                break;    
        }
    }
    
    private void senseWorldForPersons(WorldState state,WatchMecActionData data){
        PersonEntity person = state.senseWorldForPersons(data.getX(), data.getY(),data.getSenseSize());
                DataBESA dataAction;
                dataAction = new WatchMecActionData(person.getXpos(),person.getYpos(),"evaluate",person.getType(),person.isMarked());
                EventBESA queryAnswerToWatchmec;
                if(SmadhConstants.ENABLE_COOPERATION){
                    queryAnswerToWatchmec = new EventBESA(WatchMecCooperativeActionGuard.class.getName(),dataAction);
                }else{
                    queryAnswerToWatchmec = new EventBESA(WatchMecSensorGuard.class.getName(),
                                                            dataAction);
                }
                AgHandlerBESA ah;
                try {
                    ah = getAgent().getAdmLocal().getHandlerByAlias(data.getAlias());
                    ah.sendEvent(queryAnswerToWatchmec);
                } catch (ExceptionBESA e) {
                    ReportBESA.error(e);
                }
    }
    
}
