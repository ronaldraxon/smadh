package smadh.world.model;

/**
 *
 * @author raxon
 */
    public class Slot {
    private boolean allocated;
    private String allocationType;
    private boolean inhabited;
    private String inhabitedType;
    private String entityAlias;
    private int entityType;
    private int x;
    private int y;
    private String clearedByAlias;

    public boolean isAllocated() {
        return allocated;
    }

    public void setAllocated(boolean allocated) {
        this.allocated = allocated;
    }

    public String getAllocationType() {
        return allocationType;
    }

    public void setAllocationType(String allocationType) {
        this.allocationType = allocationType;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public String getEntityAlias() {
        return entityAlias;
    }

    public void setEntityAlias(String entityAlias) {
        this.entityAlias = entityAlias;
    }
    
    public boolean isInhabited() {
        return inhabited;
    }

    public void setInhabited(boolean inhabited) {
        this.inhabited = inhabited;
    }

    public String getInhabitedType() {
        return inhabitedType;
    }

    public void setInhabitedType(String inhabitedType) {
        this.inhabitedType = inhabitedType;
    }
    
    public int getEntityType() {
        return entityType;
    }

    public void setEntityType(int entityType) {
        this.entityType = entityType;
    }
    
    public String getClearedByAlias() {
        return clearedByAlias;
    }

    public void setClearedByAlias(String clearedByAlias) {
        this.clearedByAlias = clearedByAlias;
    }
    
    public Slot(int x, int y) {
        this.clearedByAlias=null;
        this.allocated = false;
        this.x = x;
        this.y = y;
    }
   
    

}
