package smadh.world.model;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.MediaTracker;
import java.awt.Rectangle;
import java.awt.TexturePaint;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Random;

import java.util.stream.Collectors;
import javafx.util.Pair;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import javax.swing.JFrame;
import javax.swing.JPanel;
import smadh.config.SmadhConstants;
import smadh.deminer.state.DeminerState;
import smadh.sweeper.state.SweeperState;
import smadh.tracer.state.TracerState;
import smadh.utils.Calculations;
import smadh.watchmec.state.WatchMecState;

/*
 *
 * @author raxon
 * 
 */

public class WorldMap extends JPanel {

    private int height;
    private int width;
    private int squareSize;

    private int numOfMinesType1;
    private int numOfMinesType2;
    private int maxPersonsInFieldQty;
    //private Map<String,Slot> boardSlots;
    private List<Slot> boardSlots;
    private List<MineEntity> landMineEntities;
    private List<PersonEntity> personsInFieldEntities;

    public void setPersonsInFieldEntities(List<PersonEntity> personsInFieldEntities) {
        this.personsInFieldEntities = personsInFieldEntities;
    }

    private List<SmadhEntity> deminersGroup;
    private List<SmadhEntity> sweepersGroup;
    private List<SmadhEntity> tracersGroup;
    private List<SmadhEntity> watchmecsGroup;

    private BufferedImage backgroundImage = WorldMap.getBufferedImage("src/images/corozal.png", this);
    private BufferedImage gridblockImage = WorldMap.getBufferedImageWithAlpha("src/images/white_block.jpg", this, 0.1f);
    private BufferedImage basegroundImage = WorldMap.getBufferedImage("src/images/baseground.png", this);
    private BufferedImage deminerImage = WorldMap.getBufferedImage("src/images/deminer.png", this);
    private BufferedImage deminerRestingImage = WorldMap.getBufferedImage("src/images/demineResting.png", this);
    private BufferedImage type1MineImage = WorldMap.getBufferedImage("src/images/type1_mine.png", this);
    private BufferedImage clearedType1MineImage = WorldMap.getBufferedImage("src/images/type1_mine_cleared.png", this);
    private BufferedImage type2MineImage = WorldMap.getBufferedImage("src/images/type2_mine.png", this);
    private BufferedImage clearedType2MineImage = WorldMap.getBufferedImage("src/images/type2_mine_cleared.png", this);
    private BufferedImage personInFieldImage = WorldMap.getBufferedImage("src/images/campesinos.png", this);
    private BufferedImage sweeperImage = WorldMap.getBufferedImage("src/images/sweeper.png", this);
    private BufferedImage sweeperRestingImage = WorldMap.getBufferedImage("src/images/sweeperResting.png", this);
    private BufferedImage tracerImage = WorldMap.getBufferedImage("src/images/tracer.png", this);
    private BufferedImage tracerRestingImage = WorldMap.getBufferedImage("src/images/tracerResting.png", this);
    private BufferedImage watchmecImage = WorldMap.getBufferedImage("src/images/watchmec.png", this);
    private BufferedImage watchmecClearingImage = WorldMap.getBufferedImage("src/images/watchmecOccupied3.png", this);
    private BufferedImage watchmecRestingImage = WorldMap.getBufferedImage("src/images/watchmecResting.png", this);
    
    
    private List<Rectangle> imagesGridRec;
    private List<TexturePaint> imagesPaintTP;

    private List<Rectangle> imagesBaseRec;
    private List<TexturePaint> imagesBaseTP;
    
    private List<Rectangle> imagesLandMineRec;
    private List<TexturePaint> imagesLandMineTP;

    private List<Rectangle> imagesClearedMinesRec;
    private List<TexturePaint> imagesClearedMinesTP;

    private List<Rectangle> personsInFieldRec;
    private List<TexturePaint> personsInFieldTP;

    private List<Rectangle> imagesDeminersRec;
    private List<TexturePaint> imagesDeminersTP;

    private List<Rectangle> imagesSweepersRec;
    private List<TexturePaint> imagesSweepersTP;

    private List<Rectangle> imagesTracersRec;
    private List<TexturePaint> imagesTracersTP;

    private List<Rectangle> imagesWatchmecsRec;
    private List<TexturePaint> imagesWatchmecsTP;

    private TexturePaint backgroundTexturePicture;
    private Rectangle backgroundRectangle;

    public WorldMap() {
        createNewMap();
    }

    public void createNewMap() {
        initializeBoard();
        initializeScenario();
        
        initializeShapes();
        initializeEntities();
        createObjectSlots();
        setBaseSlots();
        
        createtype1MinesShapes();
        createtype2MinesShapes();
        createPersonsInFieldShapes();
        createGridShapes();
        createBackgroundShape();
    }

    private void initializeScenario(){
        numOfMinesType1 = SmadhConstants.TYPE1_LANDMINES_QTY;
        numOfMinesType2 = SmadhConstants.TYPE2_LANDMINES_QTY;
        maxPersonsInFieldQty = SmadhConstants.MAX_PERSONS_IN_FIELD_QTY;
    }
    
    private void initializeBoard(){
        height = SmadhConstants.BOARD_HEIGTH;
        width = SmadhConstants.BOARD_WIDTH;
        squareSize = SmadhConstants.SQUARE_SIZE;
    }
    
    private void initializeShapes() {
        imagesGridRec = new ArrayList<>();
        imagesPaintTP = new ArrayList<>();
        imagesBaseRec = new ArrayList<>();
        imagesBaseTP = new ArrayList<>();
        imagesLandMineRec = new ArrayList<>();
        imagesLandMineTP = new ArrayList<>();
        imagesClearedMinesRec = new ArrayList<>();
        imagesClearedMinesTP = new ArrayList<>();
        personsInFieldRec = new ArrayList<>();
        personsInFieldTP = new ArrayList<>();
        imagesDeminersRec = new ArrayList<>();
        imagesDeminersTP = new ArrayList<>();
        imagesSweepersRec = new ArrayList<>();
        imagesSweepersTP = new ArrayList<>();
        imagesTracersRec = new ArrayList<>();
        imagesTracersTP = new ArrayList<>();
        imagesWatchmecsRec = new ArrayList<>();
        imagesWatchmecsTP = new ArrayList<>();
    }

    private void initializeEntities() {
        //boardSlots = new LinkedHashMap<>();
        boardSlots = new ArrayList<>();
        landMineEntities = new ArrayList<>();
        personsInFieldEntities = new ArrayList<>();
        deminersGroup = new ArrayList<>();
        sweepersGroup = new ArrayList<>();
        tracersGroup = new ArrayList<>();
        watchmecsGroup = new ArrayList<>();
    }

    private void createBackgroundShape() {
        backgroundRectangle = new Rectangle(0, 0, width * squareSize, height * squareSize);
        backgroundTexturePicture = new TexturePaint(backgroundImage, backgroundRectangle);
    }

    private void createObjectSlots(){
        for(int x=0;x<width;x++){
            for(int y=0;y<height;y++){
                Slot slot = new Slot(x,y);
                slot.setAllocated(true);
                slot.setAllocationType(SmadhConstants.ALLOCATION_FIELD);
                slot.setInhabited(false);
                slot.setInhabitedType(SmadhConstants.INHABITED_NONE);
                boardSlots.add(slot);
            }
        }
    }
    
    private void setBaseSlots(){
        
        for(int x=0;x<5;x++){
            for(int y=6;y<14;y++){
                
                //Slot slot = boardSlots.get(x+";"+y);
                Slot slot = getSlotByCoordinates(x,y);
                int index = boardSlots.indexOf(slot);
                slot.setAllocated(true);
                slot.setAllocationType(SmadhConstants.ALLOCATION_BASE);
                slot.setInhabited(false);
                slot.setInhabitedType(SmadhConstants.INHABITED_NONE);
                boardSlots.add(index, slot);
                Rectangle rec = new Rectangle(slot.getX() * squareSize, slot.getY() * squareSize, squareSize, squareSize);
                imagesBaseRec.add(rec);
                imagesBaseTP.add(new TexturePaint(basegroundImage, rec));
            }
        }
    }
    
    private Slot getSlotByCoordinates(int x, int y){
        System.out.println("buscando: "+x +";"+y);
        return boardSlots.stream().filter(s -> s.getX()==x && s.getY()==y).findFirst().orElse(null);
    }
    
    private void createtype1MinesShapes() {
        for (int i = 0; i < numOfMinesType1; i++) {
            Slot slot = lookForAnAvailableSlot(i,SmadhConstants.INHABITED_MINET1,1);
            if(!Objects.isNull(slot)){
                landMineEntities.add(new MineEntity(1,SmadhConstants.TYPE1_LANDMINES_PERSISTANCE,slot.getX(), slot.getY(), SmadhConstants.INHABITED_MINET1 +"_"+ i));
                Rectangle rec = new Rectangle(slot.getX() * squareSize, slot.getY() * squareSize, squareSize, squareSize);
                imagesLandMineRec.add(rec);
                imagesLandMineTP.add(new TexturePaint(type1MineImage, rec));
            }
        }
    }

    private void createtype2MinesShapes() {
        for (int i = 0; i < numOfMinesType2; i++) {
            Slot slot = lookForAnAvailableSlot(i,SmadhConstants.INHABITED_MINET2,2);
            if(!Objects.isNull(slot)){
                landMineEntities.add(new MineEntity(2,SmadhConstants.TYPE2_LANDMINES_PERSISTANCE,slot.getX(), slot.getY(), SmadhConstants.INHABITED_MINET2 +"_"+ i));
                Rectangle rec = new Rectangle(slot.getX() * squareSize, slot.getY() * squareSize, squareSize, squareSize);
                imagesLandMineRec.add(rec);
                imagesLandMineTP.add(new TexturePaint(type2MineImage, rec));
            }
        }
    }

    private void createPersonsInFieldShapes() {
        for (int i = 0; i < maxPersonsInFieldQty; i++) {
            Slot slot = lookForAnAvailableSlot(i,SmadhConstants.INHABITED_PERSON,1);
            if(!Objects.isNull(slot)){
                personsInFieldEntities.add(new PersonEntity(1,SmadhConstants.PERSON_IN_FIELD_PERSISTANCE,slot.getX(), slot.getY(), SmadhConstants.INHABITED_PERSON +"_"+ i));
                Rectangle rec = new Rectangle(slot.getX() * squareSize, slot.getY() * squareSize, squareSize, squareSize);
                personsInFieldRec.add(rec);
                personsInFieldTP.add(new TexturePaint(personInFieldImage, rec));
            }
        }
    }

    private Slot lookForAnAvailableSlot(int entityId,String inhabitedType,int entityType){
        boolean isLookingForFreeSlots=true;
        Slot oldSlot;
        Slot newSlot=null;
        while(isLookingForFreeSlots){
            Random r = new Random();
            int x = r.nextInt(width);
            int y = r.nextInt(height);
            //oldSlot = boardSlots.get(x+";"+y);
            oldSlot = getSlotByCoordinates(x,y);
            int index = boardSlots.indexOf(oldSlot);
            if(!oldSlot.isInhabited()&&oldSlot.getAllocationType().equals(SmadhConstants.ALLOCATION_FIELD)){
                newSlot = oldSlot;
                newSlot.setEntityAlias(inhabitedType +"_"+ entityId);
                newSlot.setEntityType(entityType);
                newSlot.setInhabited(true);
                newSlot.setInhabitedType(inhabitedType);
                boardSlots.add(index, oldSlot);
                //boardSlots.put(x+";"+y, newSlot);
                isLookingForFreeSlots=false;
            }
        }
        return newSlot;
    }
    
    private void createGridShapes() {
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                Rectangle rec = new Rectangle(j * squareSize, i * squareSize, squareSize, squareSize);
                imagesGridRec.add(rec);
                imagesPaintTP.add(new TexturePaint(gridblockImage, rec));
            }
        }
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2d = (Graphics2D) g;
        g2d.setPaint(backgroundTexturePicture);
        g2d.fill(backgroundRectangle);
        g2d.setPaint(Color.black);
        g2d.draw(backgroundRectangle);

        for (int i = 0; i < height * width; i++) {
            g2d.setPaint(imagesPaintTP.get(i));
            g2d.fill(imagesGridRec.get(i));
            g2d.setPaint(Color.black);
            g2d.draw(imagesGridRec.get(i));
        }
        
        for (int i = 0; i < imagesBaseTP.size(); i++) {
            g2d.setPaint(imagesBaseTP.get(i));
            g2d.fill(imagesBaseRec.get(i));
            g2d.setPaint(Color.black);
            g2d.draw(imagesBaseRec.get(i));
        }
        for (int i = 0; i < landMineEntities.size(); i++) {
            g2d.setPaint(imagesLandMineTP.get(i));
            g2d.fill(imagesLandMineRec.get(i));
            //g2d.setPaint(Color.gray);
            g2d.draw(imagesLandMineRec.get(i));
        }
        
        for (int i = 0; i < imagesClearedMinesTP.size(); i++) {
            g2d.setPaint(imagesClearedMinesTP.get(i));
            g2d.fill(imagesClearedMinesRec.get(i));
            //g2d.setPaint(Color.gray);
            g2d.draw(imagesClearedMinesRec.get(i));
        }
        
        for (int i = 0; i < personsInFieldEntities.size(); i++) {
            g2d.setPaint(personsInFieldTP.get(i));
            g2d.fill(personsInFieldRec.get(i));
            //g2d.setPaint(Color.gray);
            g2d.draw(personsInFieldRec.get(i));
        }
        for (int i = 0; i < deminersGroup.size(); i++) {
            g2d.setPaint(imagesDeminersTP.get(i));
            g2d.fill(imagesDeminersRec.get(i));
            //g2d.setPaint(Color.gray);
            g2d.draw(imagesDeminersRec.get(i));
        }
        for (int i = 0; i < sweepersGroup.size(); i++) {
            g2d.setPaint(imagesSweepersTP.get(i));
            g2d.fill(imagesSweepersRec.get(i));
            //g2d.setPaint(Color.gray);
            g2d.draw(imagesSweepersRec.get(i));
        }
        for (int i = 0; i < tracersGroup.size(); i++) {
            g2d.setPaint(imagesTracersTP.get(i));
            g2d.fill(imagesTracersRec.get(i));
            //g2d.setPaint(Color.gray);
            g2d.draw(imagesTracersRec.get(i));
        }
        for (int i = 0; i < watchmecsGroup.size(); i++) {
            g2d.setPaint(imagesWatchmecsTP.get(i));
            g2d.fill(imagesWatchmecsRec.get(i));
            //g2d.setPaint(Color.gray);
            g2d.draw(imagesWatchmecsRec.get(i));
        }        
    }

    public static BufferedImage getBufferedImage(String imageFile, Component c) {
        Image image = c.getToolkit().getImage(imageFile);
        waitForImage(image, c);
        BufferedImage bufferedImage = new BufferedImage(image.getWidth(c), image.getHeight(c), BufferedImage.TYPE_INT_ARGB);
        Graphics2D g2d = bufferedImage.createGraphics();
        g2d.drawImage(image, 0, 0, c);
        return (bufferedImage);
    }

    public static BufferedImage getBufferedImageWithAlpha(String imageFile, Component c, float alpha) {
        Image image = c.getToolkit().getImage(imageFile);
        waitForImage(image, c);
        BufferedImage bufferedImage = new BufferedImage(image.getWidth(c), image.getHeight(c), BufferedImage.TYPE_INT_ARGB);
        Graphics2D g2d = bufferedImage.createGraphics();
        g2d.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, alpha));
        g2d.drawImage(image, 0, 0, c);
        return (bufferedImage);
    }

    public static boolean waitForImage(Image image, Component c) {
        MediaTracker tracker = new MediaTracker(c);
        tracker.addImage(image, 0);
        try {
            tracker.waitForAll();
        } catch (InterruptedException ie) {
        }
        return (!tracker.isErrorAny());
    }

    public static JFrame openInJFrame(Container content, int width, int height) {
        return (openInJFrame(content, width, height, content.getClass().getName(), Color.white));
    }

    public static JFrame openInJFrame(Container content, int width, int height, String title, Color bgColor) {
        JFrame frame = new JFrame("Simulador SMADH");
        frame.setResizable(false);
        frame.setBackground(bgColor);
        content.setBackground(bgColor);
        frame.setSize(width, height);
        frame.setContentPane(content);
        frame.addWindowListener(new ExitListener());
        frame.setVisible(true);
        return (frame);
    }

    public void addRoleDeminer(String alias, DeminerState ds) {
        deminersGroup.add(new SmadhEntity(ds.getRoleName(),ds.getFuel(),
                          ds.getActionForce(),ds.getFuelConsumptionPerPeriod(),
                          ds.getX(), ds.getY(), alias));
        //Slot slot = new Slot(ds.getX(), ds.getX());
        Slot slot = getSlotByCoordinates(ds.getX(), ds.getX());
        int index = boardSlots.indexOf(slot);
        slot.setInhabited(true);
        slot.setInhabitedType(SmadhConstants.INHABITED_AGENT_DEMINER);
        slot.setEntityAlias(alias);
        boardSlots.add(index, slot);
        Rectangle rec = createRectangle(ds.getX(),ds.getY());
        imagesDeminersRec.add(rec);
        imagesDeminersTP.add(new TexturePaint(deminerImage, rec));
    }
    
    
    public Slot tryToPresetSlotDeminer(String alias, int x, int y) {
        Slot slot = getSlotByCoordinates(x, y);
        if(slot.isInhabited()){
            return null;
        }else{
            slot.setInhabited(true);
            slot.setInhabitedType(SmadhConstants.INHABITED_AGENT_DEMINER);
            slot.setEntityAlias(alias);
            return slot;
        }
    }
    
    public Slot tryToPresetSlotForWatchMec(String alias, int x, int y) {
        Slot slot = getSlotByCoordinates(x, y);
        if(slot.isInhabited()){
            return null;
        }else{
            slot.setInhabited(true);
            slot.setInhabitedType(SmadhConstants.INHABITED_AGENT_OCCUPIED_WATCHMEC);
            slot.setEntityAlias(alias);
            return slot;
        }
    }
    
    public void moveDeminer(String alias, int x, int y) {
        int botIndex = find(this.deminersGroup, alias);
        if (botIndex != -1) {
            updateSlotOccupiedByDeminer(botIndex,alias, x, y);
            Rectangle rec = createRectangle(x,y);
            imagesDeminersRec.set(botIndex, rec);
            imagesDeminersTP.set(botIndex, new TexturePaint(deminerImage, rec));
            repaint();
        }
    }
    
    public void updateSlotOccupiedByDeminer(int botIndex,String alias, int newX, int newY){
        int x = deminersGroup.get(botIndex).getXpos();
        int y = deminersGroup.get(botIndex).getYpos();
        //Slot slot = boardSlots.get(x+";"+y);
        
        Slot slot = getSlotByCoordinates(x, y);
        int index = boardSlots.indexOf(slot);
        slot.setInhabited(false);
        slot.setInhabitedType(SmadhConstants.INHABITED_NONE);
        slot.setEntityAlias(null);
        boardSlots.add(index,slot);
        //slot = boardSlots.get(newX+";"+newY);
        slot = getSlotByCoordinates(newX, newY);
        index = boardSlots.indexOf(slot);
        slot.setInhabited(true);
        slot.setInhabitedType(SmadhConstants.INHABITED_AGENT_DEMINER);
        slot.setEntityAlias(alias);
        boardSlots.add(index,slot);
        deminersGroup.get(botIndex).setXpos(newX);
        deminersGroup.get(botIndex).setYpos(newY); 
    }
    
    public void addRoleSweeper(String alias, SweeperState ds) {
        sweepersGroup.add(new SmadhEntity(ds.getRoleName(),ds.getFuel(),
                          ds.getActionForce(),ds.getFuelConsumptionPerPeriod(),
                          ds.getX(), ds.getY(), alias));
        Rectangle rec = createRectangle(ds.getX(),ds.getY());
        imagesSweepersRec.add(rec);
        imagesSweepersTP.add(new TexturePaint(sweeperImage, rec));
    }
    
    public void moveSweeper(String alias, int x, int y) {
        int botIndex = find(this.sweepersGroup, alias);
        if (botIndex != -1) {
            updateSlotOccupiedBySweeper(botIndex,alias, x, y);
            Rectangle rec = createRectangle(x,y);
            imagesSweepersRec.set(botIndex, rec);
            imagesSweepersTP.set(botIndex, new TexturePaint(sweeperImage, rec));
            repaint();
        }
    }
    
    public void updateSlotOccupiedBySweeper(int botIndex,String alias, int newX, int newY){
        int x = sweepersGroup.get(botIndex).getXpos();
        int y = sweepersGroup.get(botIndex).getYpos();
        //Slot slot = boardSlots.get(x+";"+y);
        Slot slot = getSlotByCoordinates(x, y);
        int index = boardSlots.indexOf(slot);
        
        slot.setInhabited(false);
        slot.setInhabitedType(SmadhConstants.INHABITED_NONE);
        slot.setEntityAlias(null);
        boardSlots.add(index,slot);
        //slot = boardSlots.get(newX+";"+newY);
        slot = getSlotByCoordinates(newX, newY);
        index = boardSlots.indexOf(slot);
        slot.setInhabited(true);
        slot.setInhabitedType(SmadhConstants.INHABITED_AGENT_SWEEPER);
        slot.setEntityAlias(alias);
        boardSlots.add(index,slot);
        sweepersGroup.get(botIndex).setXpos(newX);
        sweepersGroup.get(botIndex).setYpos(newY); 
    }
    
    
    public void addRoleTracer(String alias, TracerState ts) {
        tracersGroup.add(new SmadhEntity(ts.getRoleName(),ts.getFuel(),
                          ts.getActionForce(),ts.getFuelConsumptionPerPeriod(),
                          ts.getX(), ts.getY(), alias));
        Rectangle rec = createRectangle(ts.getX(),ts.getY());
        imagesTracersRec.add(rec);
        imagesTracersTP.add(new TexturePaint(tracerImage, rec));
    }
    
    public void addRoleWatchmec(String alias, WatchMecState ws) {
        watchmecsGroup.add(new SmadhEntity(ws.getRoleName(),ws.getFuel(),
                          ws.getActionForce(),ws.getFuelConsumptionPerPeriod(),
                          ws.getX(), ws.getY(), alias));
         Rectangle rec = createRectangle(ws.getX(),ws.getY());
        imagesWatchmecsRec.add(rec);
        imagesWatchmecsTP.add(new TexturePaint(watchmecImage, rec));
    }
    
    private Rectangle createRectangle(int x, int y){
        return new Rectangle(x * squareSize, 
                            y * squareSize, squareSize, squareSize);
    }
    
    
    public Slot demineByDeminer(String alias,int entityType,int x, int y) {
        //Slot mineSlot = boardSlots.get(x+";"+y);
        Slot mineSlot = getSlotByCoordinates(x, y);
        int index = boardSlots.indexOf(mineSlot);
        if(mineSlot.isInhabited() && 
          (mineSlot.getInhabitedType().equals(SmadhConstants.INHABITED_MINET1) || 
           mineSlot.getInhabitedType().equals(SmadhConstants.INHABITED_MINET2))){
           int mineIndex = findMine(landMineEntities,x,y,entityType); 
           int minePersistance = landMineEntities.get(mineIndex).getPersistence();
           minePersistance = minePersistance - SmadhConstants.BOTS_DEMINERS_ACTION_FORCE;
           landMineEntities.get(mineIndex).setPersistence(minePersistance);
           if(minePersistance<=0){
               mineSlot.setClearedByAlias(alias);
               mineSlot.setInhabited(false);
               mineSlot.setInhabitedType(SmadhConstants.INHABITED_NONE);
               boardSlots.add(index,mineSlot);
               landMineEntities.remove(mineIndex);
               imagesLandMineRec.remove(mineIndex);
               imagesLandMineTP.remove(mineIndex);
               Rectangle rec = createRectangle(x,y);
               imagesClearedMinesRec.add(rec);
               if(entityType==1){
                   imagesClearedMinesTP.add(new TexturePaint(clearedType1MineImage, rec));
               }else{
                   imagesClearedMinesTP.add(new TexturePaint(clearedType2MineImage, rec));
               }
            repaint();   
           }            
        }
        return mineSlot;
    }

    public Slot demineBySweeper(String alias,int entityType,int x, int y) {
        //Slot mineSlot = boardSlots.get(x+";"+y);
        Slot mineSlot = getSlotByCoordinates(x, y);
        int index = boardSlots.indexOf(mineSlot);
        if(mineSlot.isInhabited() && 
          (mineSlot.getInhabitedType().equals(SmadhConstants.INHABITED_MINET1) || 
           mineSlot.getInhabitedType().equals(SmadhConstants.INHABITED_MINET2))){
           int mineIndex = findMine(landMineEntities,x,y,entityType); 
           int minePersistance = landMineEntities.get(mineIndex).getPersistence();
           minePersistance = minePersistance - SmadhConstants.BOTS_SWEEPERS_ACTION_FORCE;
           landMineEntities.get(mineIndex).setPersistence(minePersistance);
           if(minePersistance<=0){
               mineSlot.setClearedByAlias(alias);
               mineSlot.setInhabited(false);
               mineSlot.setInhabitedType(SmadhConstants.INHABITED_NONE);
               boardSlots.add(index,mineSlot);
               landMineEntities.remove(mineIndex);
               imagesLandMineRec.remove(mineIndex);
               imagesLandMineTP.remove(mineIndex);
               Rectangle rec = createRectangle(x,y);
               imagesClearedMinesRec.add(rec);
               if(entityType==1){
                   imagesClearedMinesTP.add(new TexturePaint(clearedType1MineImage, rec));
               }else{
                   imagesClearedMinesTP.add(new TexturePaint(clearedType2MineImage, rec));
               }
            repaint();   
           }            
        }
        return mineSlot;
    }
    
    public Slot clearByWatchMec(String alias,int x, int y) {
        Slot personSlot = getSlotByCoordinates(x, y);
        int index = boardSlots.indexOf(personSlot);
        if(personSlot.isInhabited() && 
           personSlot.getInhabitedType().equals(SmadhConstants.INHABITED_PERSON)){
           int personIndex = findPerson(personsInFieldEntities,x,y,1); 
           int personPersistance = personsInFieldEntities.get(personIndex).getPersistence();
           personPersistance = personPersistance - SmadhConstants.BOTS_WATCHMECS_ACTION_FORCE;
           personsInFieldEntities.get(personIndex).setPersistence(personPersistance);
           if(personPersistance<=0){
               personSlot.setClearedByAlias(alias);
               personSlot.setInhabited(false);
               personSlot.setInhabitedType(SmadhConstants.INHABITED_NONE);
               boardSlots.add(index,personSlot);
               personsInFieldEntities.remove(personIndex);
               personsInFieldRec.remove(personIndex);
               personsInFieldTP.remove(personIndex);
            repaint();   
           }            
        }
        return personSlot;
    }
    
    public Slot startClearingByWatchMec(String alias,int x, int y) {
        Slot personSlot = getSlotByCoordinates(x, y);
        int index = boardSlots.indexOf(personSlot);
        //if(personSlot.getInhabitedType().equals(SmadhConstants.INHABITED_PERSON)){
           int personIndex = findPerson(personsInFieldEntities,x,y,1); 
           int personPersistance = personsInFieldEntities.get(personIndex).getPersistence();
           personPersistance = personPersistance - SmadhConstants.BOTS_WATCHMECS_ACTION_FORCE;
           personsInFieldEntities.get(personIndex).setPersistence(personPersistance);
           if(personPersistance<=0){
               
               personSlot.setClearedByAlias(alias);
               personSlot.setInhabited(false);
               personSlot.setInhabitedType(SmadhConstants.INHABITED_AGENT_OCCUPIED_WATCHMEC);
               boardSlots.add(index,personSlot);
               personsInFieldEntities.remove(personIndex);
               personsInFieldRec.remove(personIndex);
               personsInFieldTP.remove(personIndex);
               int botIndex = find(this.watchmecsGroup, alias);
               if (botIndex != -1) {
                    Rectangle rec = createRectangle(x,y);
                    imagesWatchmecsRec.set(botIndex, rec);
                    imagesWatchmecsTP.set(botIndex, new TexturePaint(watchmecClearingImage, rec));
                }   
            repaint();   
           }            
        //}
        return personSlot;
    }
    
    
    public void disableEntity(String alias) {//ojo hay que tener en cuanta que son dos agentes limpiando
        int botIndex = -1;
        int mineIndex = -1;
        int personIndex = -1;
        if(alias.startsWith("D")){
            botIndex = find(this.deminersGroup, alias);
            mineIndex = findMine(this.landMineEntities, deminersGroup.get(botIndex).getXpos(), deminersGroup.get(botIndex).getYpos(),1);
        }else if(alias.startsWith("S")){
            botIndex = find(this.sweepersGroup, alias);
            mineIndex = findMine(this.landMineEntities, sweepersGroup.get(botIndex).getXpos(), sweepersGroup.get(botIndex).getYpos(),2);
        }else if(alias.startsWith("W")){
            botIndex = find(this.watchmecsGroup, alias);
            personIndex = findPerson(this.personsInFieldEntities , watchmecsGroup.get(botIndex).getXpos(), watchmecsGroup.get(botIndex).getYpos(),1);
        }

        if (personIndex != -1) {
            personsInFieldEntities.remove(personIndex);
            personsInFieldRec.remove(personIndex);
            personsInFieldTP.remove(personIndex);
        }
        if (mineIndex != -1) {
            landMineEntities.remove(mineIndex);
            imagesLandMineRec.remove(mineIndex);
            imagesLandMineTP.remove(mineIndex);
        }
        
        repaint();
    }
    
    public void markMine(String alias) {
        int botIndex = -1;
        int mineIndex = -1;
        if(alias.startsWith("T")){
            botIndex = find(this.tracersGroup, alias);
            mineIndex = markMine(this.landMineEntities, tracersGroup.get(botIndex).getXpos(), tracersGroup.get(botIndex).getYpos(),false);
        }
        if (mineIndex != -1) {
            this.landMineEntities.get(mineIndex).setMarked(true);
        }
        repaint();
    }
    
    public void putTracerInRest(String alias) {
        int botIndex = find(this.tracersGroup, alias);
        if (botIndex != -1) {
            int x =tracersGroup.get(botIndex).getXpos();
            int y =tracersGroup.get(botIndex).getYpos();
            Rectangle rec = createRectangle(x,y);
            imagesTracersRec.set(botIndex, rec);
            imagesTracersTP.set(botIndex, new TexturePaint(tracerRestingImage, rec));
            repaint();
        }
    }
    
    public void putDeminerInRest(String alias) {
        int botIndex = find(this.deminersGroup, alias);
        if (botIndex != -1) {
            int x =deminersGroup.get(botIndex).getXpos();
            int y =deminersGroup.get(botIndex).getYpos();
            Rectangle rec = createRectangle(x,y);
            imagesDeminersRec.set(botIndex, rec);
            imagesDeminersTP.set(botIndex, new TexturePaint(deminerRestingImage, rec));
            repaint();
        }
    }
    
    public void putSweeperInRest(String alias) {
        int botIndex = find(this.sweepersGroup, alias);
        if (botIndex != -1) {
            int x =sweepersGroup.get(botIndex).getXpos();
            int y =sweepersGroup.get(botIndex).getYpos();
            Rectangle rec = createRectangle(x,y);
            imagesSweepersRec.set(botIndex, rec);
            imagesSweepersTP.set(botIndex, new TexturePaint(sweeperRestingImage, rec));
            repaint();
        }
    }
    
    public void putWatchMecInRest(String alias) {
        int botIndex = find(this.watchmecsGroup, alias);
        if (botIndex != -1) {
            int x =watchmecsGroup.get(botIndex).getXpos();
            int y =watchmecsGroup.get(botIndex).getYpos();
            Rectangle rec = createRectangle(x,y);
            imagesWatchmecsRec.set(botIndex, rec);
            imagesWatchmecsTP.set(botIndex, new TexturePaint(watchmecRestingImage, rec));
            repaint();
        }
    }
    
    public void cleanAllMines() {
        landMineEntities.clear();
        imagesLandMineRec.clear();
        imagesLandMineTP.clear();
        repaint();
    }

    public void moveTracer(String alias, int x, int y) {
        int botIndex = find(this.tracersGroup, alias);
        tracersGroup.get(botIndex).setXpos(x);
        tracersGroup.get(botIndex).setYpos(y);
        if (botIndex != -1) {
            Rectangle rec = createRectangle(x,y);
            imagesTracersRec.set(botIndex, rec);
            imagesTracersTP.set(botIndex, new TexturePaint(tracerImage, rec));
            repaint();
        }
    }
    
    public void moveWatchmec(String alias, int x, int y) {
        int botIndex = find(this.watchmecsGroup, alias);
        watchmecsGroup.get(botIndex).setXpos(x);
        watchmecsGroup.get(botIndex).setYpos(y); 
        if (botIndex != -1) {
            //updateSlotOccupiedByWatchMec(botIndex,alias, x, y);
            Rectangle rec =  createRectangle(x,y);
            imagesWatchmecsRec.set(botIndex, rec);
            imagesWatchmecsTP.set(botIndex, new TexturePaint(watchmecImage, rec));
            repaint();
        }
    }
    
    public void moveWatchmecWithPerson(String alias, int x, int y) {
        int botIndex = find(this.watchmecsGroup, alias);
        if (botIndex != -1) {
            updateSlotOccupiedByWatchMec(botIndex,alias, x, y);
            Rectangle rec =  createRectangle(x,y);
            imagesWatchmecsRec.set(botIndex, rec);
            imagesWatchmecsTP.set(botIndex, new TexturePaint(watchmecClearingImage, rec));
            repaint();
        }
    }
    
    public void clearPersonInSafeArea(String alias, int x, int y){

        Slot slot = getSlotByCoordinates(x, y);
        int index = boardSlots.indexOf(slot);
        slot.setInhabited(false);
        slot.setInhabitedType(SmadhConstants.INHABITED_NONE);
        slot.setEntityAlias(null);
        boardSlots.add(index,slot);
        slot = getSlotByCoordinates(x, y);
        index = boardSlots.indexOf(slot);
    }
    
    
    
    public void updateSlotOccupiedByWatchMec(int botIndex,String alias, int newX, int newY){
        int x = watchmecsGroup.get(botIndex).getXpos();
        int y = watchmecsGroup.get(botIndex).getYpos();
        //Slot slot = boardSlots.get(x+";"+y);
        
        Slot slot = getSlotByCoordinates(x, y);
        int index = boardSlots.indexOf(slot);
        slot.setInhabited(false);
        slot.setInhabitedType(SmadhConstants.INHABITED_NONE);
        slot.setEntityAlias(null);
        boardSlots.add(index,slot);
        //slot = boardSlots.get(newX+";"+newY);
        
        slot = getSlotByCoordinates(newX, newY);
        index = boardSlots.indexOf(slot);
        
        slot.setInhabited(true);
        slot.setInhabitedType(SmadhConstants.INHABITED_AGENT_OCCUPIED_WATCHMEC);
        slot.setEntityAlias(alias);
        
        boardSlots.add(index,slot);
        watchmecsGroup.get(botIndex).setXpos(newX);
        watchmecsGroup.get(botIndex).setYpos(newY); 
    }
    
    
    public MineEntity senseMinesInWorld(int agentXPosition, int agentYPosition, double agentSenseSize){
        List<MineEntity> tentativeMines = landMineEntities
                                          .stream()
                                          .filter(t-> getDistanceBetweenMineAndAgentPosition(agentXPosition,agentYPosition,t)<=agentSenseSize)
                                          .filter(t-> !t.isMarked())
                                          .collect(Collectors.toList());
        if(tentativeMines.isEmpty()){
            return new MineEntity(null);
        }else{
            return tentativeMines.get(0);
        }
    }
    
    public PersonEntity sensePersonsInWorld(int agentXPosition, int agentYPosition, double agentSenseSize){
        List<PersonEntity> tentativePersons = personsInFieldEntities
                                          .stream()
                                          .filter(t-> getDistanceBetweenPersonAndAgentPosition(agentXPosition,agentYPosition,t)<=agentSenseSize)
                                          .filter(t-> !t.isMarked())
                                          .collect(Collectors.toList());
        if( tentativePersons.isEmpty()){
            return new PersonEntity(null);
        }else{
            return tentativePersons.get(0);
        }
    }
    
    public List<Slot> senseAllInWorldByWatchMec(int agentXPosition, int agentYPosition){
        List<Slot> sensedSlots =new ArrayList<>();      
        buildValidSlotList(sensedSlots,agentXPosition-1,agentYPosition-1);
        buildValidSlotList(sensedSlots,agentXPosition,agentYPosition-1);
        buildValidSlotList(sensedSlots,agentXPosition+1,agentYPosition-1);
        buildValidSlotList(sensedSlots,agentXPosition-1,agentYPosition);
        buildValidSlotList(sensedSlots,agentXPosition+1,agentYPosition);
        buildValidSlotList(sensedSlots,agentXPosition-1,agentYPosition+1);
        buildValidSlotList(sensedSlots,agentXPosition,agentYPosition+1);
        buildValidSlotList(sensedSlots,agentXPosition+1,agentYPosition+1);
        return sensedSlots;
    }
    
    
    public List<Slot> senseAllInWorldByDeminer(int agentXPosition, int agentYPosition){
        List<Slot> sensedSlots =new ArrayList<>();      
        buildValidSlotList(sensedSlots,agentXPosition-1,agentYPosition-1);
        buildValidSlotList(sensedSlots,agentXPosition,agentYPosition-1);
        buildValidSlotList(sensedSlots,agentXPosition+1,agentYPosition-1);
        buildValidSlotList(sensedSlots,agentXPosition-1,agentYPosition);
        buildValidSlotList(sensedSlots,agentXPosition+1,agentYPosition);
        buildValidSlotList(sensedSlots,agentXPosition-1,agentYPosition+1);
        buildValidSlotList(sensedSlots,agentXPosition,agentYPosition+1);
        buildValidSlotList(sensedSlots,agentXPosition+1,agentYPosition+1);
        return sensedSlots;
    }
    
    public List<Slot> senseAllInWorldBySweeper(int agentXPosition, int agentYPosition){
        List<Slot> sensedSlots =new ArrayList<>();      
        buildValidSlotList(sensedSlots,agentXPosition-1,agentYPosition-1);
        buildValidSlotList(sensedSlots,agentXPosition,agentYPosition-1);
        buildValidSlotList(sensedSlots,agentXPosition+1,agentYPosition-1);
        buildValidSlotList(sensedSlots,agentXPosition-1,agentYPosition);
        buildValidSlotList(sensedSlots,agentXPosition+1,agentYPosition);
        buildValidSlotList(sensedSlots,agentXPosition-1,agentYPosition+1);
        buildValidSlotList(sensedSlots,agentXPosition,agentYPosition+1);
        buildValidSlotList(sensedSlots,agentXPosition+1,agentYPosition+1);
        return sensedSlots;
    }
    
    public void buildValidSlotList(List<Slot> sensedSlots,int x, int y){
        Slot slot;
        slot = getSlotByCoordinates(x, y);
        if(!Objects.isNull(slot)){
            //System.out.println("Creación valid slot x:"+x+" x':"+slot.getX()+" y:"+y+" y':"+slot.getY());
            sensedSlots.add(slot);
        }  
    }
    
    private double getDistanceBetweenMineAndAgentPosition(int agentXPosition,int agentYPosition,MineEntity mineEntity){
        return Calculations.calculateDistanceBetweenPoints(agentXPosition,agentYPosition,
                                              mineEntity.getXpos(),
                                              mineEntity.getYpos());
    }
    
    private double getDistanceBetweenPersonAndAgentPosition(int agentXPosition,int agentYPosition,PersonEntity personEntity){
        return Calculations.calculateDistanceBetweenPoints(agentXPosition,agentYPosition,
                                              personEntity.getXpos(),
                                              personEntity.getYpos());
    }
        
    private int find(List<SmadhEntity> bots, String alias) {
        for (int i = 0; i < bots.size(); i++) {
            if (bots.get(i).getAlias().equals(alias)) {
                return i;
            }
        }
        return -1;
    }

    private int findMine(List<MineEntity> mine, int xpos, int ypos,int type) {
        for (int i = 0; i < mine.size(); i++) {
            if (xpos == mine.get(i).getXpos() && ypos == mine.get(i).getYpos() && type == mine.get(i).getType()) {
                return i;
            }
        }
        return -1;
    }
    
    private int markMine(List<MineEntity> mine, int xpos, int ypos,boolean mark) {
        for (int i = 0; i < mine.size(); i++) {
            if (xpos == mine.get(i).getXpos() && ypos == mine.get(i).getYpos() && mark==mine.get(i).isMarked()) {
                return i;
            }
        }
        return -1;
    }
    
    private int findPerson(List<PersonEntity> person, int xpos, int ypos,int type) {
        for (int i = 0; i < person.size(); i++) {
            if (xpos == person.get(i).getXpos() && ypos == person.get(i).getYpos() && type == person.get(i).getType()) {
                return i;
            }
        }
        return -1;
    }

    public static class ExitListener extends WindowAdapter {
        @Override
        public void windowClosing(WindowEvent event) {
            System.exit(0);
        }
    }

    public List<SmadhEntity> getDeminerGroup() {
        return deminersGroup;
    }

    public void setDeminerGroup(List<SmadhEntity> deminersGroup) {
        this.deminersGroup = deminersGroup;
    }

    public List<MineEntity> getLandMineEntities() {
        return landMineEntities;
    }

    public void setLandMineEntities(List<MineEntity> type1MineEntities) {
        this.landMineEntities = type1MineEntities;
    }
    
    public List<PersonEntity> getPersonsInFieldEntities() {
        return personsInFieldEntities;
    }
    
    public List<SmadhEntity> getSweepersGroup() {
        return sweepersGroup;
    }

    public void setSweepersGroup(List<SmadhEntity> sweepersGroup) {
        this.sweepersGroup = sweepersGroup;
    }

    public List<SmadhEntity> getTracersGroup() {
        return tracersGroup;
    }

    public void setTracersGroup(List<SmadhEntity> tracersGroup) {
        this.tracersGroup = tracersGroup;
    }

    public List<SmadhEntity> getWatchmecsGroup() {
        return watchmecsGroup;
    }

    public void setWatchmecsGroup(List<SmadhEntity> watchmecsGroup) {
        this.watchmecsGroup = watchmecsGroup;
    }

}
