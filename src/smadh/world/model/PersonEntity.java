package smadh.world.model;

/**
 *
 * @author raxon
 */
public class PersonEntity {
    
    private int type;
    private boolean marked;
    private int persistence;
    private int xpos;
    private int ypos;
    private String alias;

    public int getXpos() {
        return xpos;
    }

    public void setXpos(int xpos) {
        this.xpos = xpos;
    }

    public int getYpos() {
        return ypos;
    }

    public void setYpos(int ypos) {
        this.ypos = ypos;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getPersistence() {
        return persistence;
    }

    public void setPersistence(int persistence) {
        this.persistence = persistence;
    }
    
    public boolean isMarked() {
        return marked;
    }

    public void setMarked(boolean marked) {
        this.marked = marked;
    }
    
    public PersonEntity(int type, int persistence, int xpos, int ypos, String alias) {
        this.type = type;
        this.persistence = persistence;
        this.xpos = xpos;
        this.ypos = ypos;
        this.alias = alias;
    }
    
    public PersonEntity(int xpos, int ypos, String alias) {
        this.xpos = xpos;
        this.ypos = ypos;
        this.alias = alias;
    }
    
    public PersonEntity(String alias) {
        this.type = 0;
        this.marked=false;
        this.persistence = 0;
        this.xpos = -1;
        this.ypos = -1;
        this.alias = alias;
    }

}
