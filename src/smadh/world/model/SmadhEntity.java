package smadh.world.model;

/**
 *
 * @author raxon
 */
public class SmadhEntity {
    private String agentRole;
    private int fuel;
    private int actionForce;
    private int fuelConsumptionPerPeriod;
    private int xpos;
    private int ypos;
    private String alias;

    public String getAgentRole() {
        return agentRole;
    }

    public void setAgentRole(String agentRole) {
        this.agentRole = agentRole;
    }

    public int getFuel() {
        return fuel;
    }

    public void setFuel(int fuel) {
        this.fuel = fuel;
    }

    public int getActionForce() {
        return actionForce;
    }

    public void setActionForce(int actionForce) {
        this.actionForce = actionForce;
    }

    public int getFuelConsumptionPerPeriod() {
        return fuelConsumptionPerPeriod;
    }

    public void setFuelConsumptionPerPeriod(int fuelConsumptionPerPeriod) {
        this.fuelConsumptionPerPeriod = fuelConsumptionPerPeriod;
    }

    public int getXpos() {
        return xpos;
    }

    public void setXpos(int xpos) {
        this.xpos = xpos;
    }

    public int getYpos() {
        return ypos;
    }

    public void setYpos(int ypos) {
        this.ypos = ypos;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public SmadhEntity(String agentRole, int fuel, int actionForce, int fuelConsumptionPerPeriod, int xpos, int ypos,String alias) {
        this.agentRole = agentRole;
        this.fuel = fuel;
        this.actionForce = actionForce;
        this.fuelConsumptionPerPeriod = fuelConsumptionPerPeriod;
        this.xpos = xpos;
        this.ypos = ypos;
        this.alias = alias;
    }
    
    public SmadhEntity(String alias, int xpos, int ypos) {
        this.xpos = xpos;
        this.ypos = ypos;
        this.alias = alias;
    }

}
