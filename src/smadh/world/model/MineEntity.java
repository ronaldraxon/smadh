package smadh.world.model;

import java.util.List;

/**
 *
 * @author raxon
 */
    public class MineEntity {
    private boolean marked;
    private boolean cleared;
    private int type;
    private int persistence;
    private int xpos;
    private int ypos;
    private String alias;
    private String asigneeAlias;

    public int getXpos() {
        return xpos;
    }

    public void setXpos(int xpos) {
        this.xpos = xpos;
    }

    public int getYpos() {
        return ypos;
    }

    public void setYpos(int ypos) {
        this.ypos = ypos;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getPersistence() {
        return persistence;
    }

    public void setPersistence(int persistence) {
        this.persistence = persistence;
    }
    
    public boolean isMarked() {
        return marked;
    }

    public void setMarked(boolean marked) {
        this.marked = marked;
    }
    
    public MineEntity(int type, int persistence, int xpos, int ypos, String alias) {
        this.marked = false;
        this.type = type;
        this.persistence = persistence;
        this.xpos = xpos;
        this.ypos = ypos;
        this.alias = alias;
    }
    
    public MineEntity(String alias) {
        this.marked = false;
        this.type = 0;
        this.persistence = 0;
        this.xpos = -1;
        this.ypos = -1;
        this.alias = alias;
    }
    
     public MineEntity(int type,int x,int y) {
        this.cleared =false;
        this.asigneeAlias ="";
        this.marked = true;
        this.type = type;
        this.persistence = 0;
        this.xpos = x;
        this.ypos = y;
    }
     
    
    public String getAsigneeAlias() {
        return asigneeAlias;
    }

    public void setAsigneesAlias(String asigneeAlias) {
        this.asigneeAlias = asigneeAlias;
    }
    

    
    public boolean isCleared() {
        return cleared;
    }

    public void setCleared(boolean cleared) {
        this.cleared = cleared;
    }
}
