 package smadh.sweeper;



import BESA.Kernell.Agent.AgentBESA;
import BESA.Kernell.Agent.KernellAgentExceptionBESA;
import BESA.Kernell.Agent.StateBESA;
import BESA.Kernell.Agent.StructBESA;
import BESA.Log.ReportBESA;
import smadh.sweeper.state.SweeperState;

/**
 *
 * @author Raxon
 */
public class SweeperAgent extends AgentBESA {

    
    public SweeperAgent(String alias, StateBESA state, 
                        StructBESA structAgent, double passwd) 
                        throws KernellAgentExceptionBESA {
        super(alias, state, structAgent, passwd);
    }
    
    @Override
    public void setupAgent() {
        ReportBESA.info("SETUP AGENT -> " + getAlias());
        SweeperState cs = (SweeperState)this.getState();
    }

    @Override
    public void shutdownAgent() {
        ReportBESA.info("SHUTDOWN AGENT -> " + getAlias());
    }
    
}
