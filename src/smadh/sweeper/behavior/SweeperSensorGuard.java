/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package smadh.sweeper.behavior;

import BESA.ExceptionBESA;
import BESA.Kernell.Agent.Event.DataBESA;
import BESA.Kernell.Agent.Event.EventBESA;
import BESA.Kernell.Agent.GuardBESA;
import BESA.Kernell.Agent.StateBESA;
import BESA.Kernell.System.Directory.AgHandlerBESA;
import BESA.Log.ReportBESA;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import javafx.util.Pair;
import smadh.config.SmadhConstants;

import smadh.control.behavior.ReportSweeperGuard;
import smadh.data.ActionData;

import smadh.data.SweeperActionData;

import smadh.sweeper.state.SweeperState;
import smadh.utils.Calculations;
import smadh.world.behavior.sweeperInteractions.SenseQuerySweeperGuard;

import smadh.world.behavior.sweeperInteractions.UpdateSweeperGuard;
import smadh.world.model.Slot;

/**
 *
 * @author Raxon
 */
public class SweeperSensorGuard extends GuardBESA{

    @Override
    public boolean funcEvalBool(StateBESA objEvalBool) {
        return true;
    }
    
    @Override
    public void funcExecGuard(EventBESA ebesa) {
        SweeperActionData data = (SweeperActionData) ebesa.getData();
        SweeperState state = (SweeperState) this.getAgent().getState();
        System.out.println("accion entrante :"+data.getAction());
        System.out.println("estado actual :"+state.getStatus());
        switch (data.getAction()) {
            case "enable":
                changeToReadyStatus(state);
            break;    
            case "search":
                if(isStatusDifferentThanRest(state)){
                    if (isEnergyUnder1Point(state)|| isCharging(state)){
                        reChargeDrone(state);
                    }else if(isJobFinished(state)){
                        returnToBase(state);
                    }else{
                        useDetector(data, state);
                    }
                }
            break;
            case "evaluate":
               
                if(isStatusDifferentThanRest(state)){
                    if (isEnergyUnder1Point(state)|| isCharging(state)){
                        reChargeDrone(state);
                    }else if(isJobFinished(state)){
                        returnToBase(state);
                    }else{
                        evaluateAndProceed(data,state);
                    }
                }
            break;
            case "demine":
             
                if(isStatusDifferentThanRest(state)){
                    if (isEnergyUnder1Point(state)|| isCharging(state)){
                        reChargeDrone(state);
                    }else if(isJobFinished(state)){
                        returnToBase(state);
                    }else{
                        startDemining(data,state);
                    }
                }
            break; 
            case "cleared":
                if(isStatusDifferentThanRest(state)){
                    if (isEnergyUnder1Point(state)|| isCharging(state)){
                        reChargeDrone(state);
                    }else if(isJobFinished(state)){
                        returnToBase(state);
                    }else{
                        beReadyForNextSearch(data,state);
                    }
                }
            break;
        }
    }
    
    private boolean isStatusDifferentThanRest(SweeperState state){
        return !state.getStatus().equals(SmadhConstants.STATUS_REST);
    }
    
    private boolean isJobFinished(SweeperState state){
        return state.getDroneMap().size()>SmadhConstants.BOTS_SWEEPERS_MAX_EXPLORATION_SCORE;
    }
    
    private boolean isEnergyUnder1Point(SweeperState state){
        return state.getFuel()<1;
    }
    
    private boolean isCharging(SweeperState state){
        return state.getStatus().equals(SmadhConstants.STATUS_CHARGING);
    }
    
    private void reChargeDrone(SweeperState state){
        DataBESA dataAction=new ActionData(this.getAgent().getAlias(), "");
        if(!state.getStatus().equals(SmadhConstants.STATUS_CHARGING)){
            state.setStatus(SmadhConstants.STATUS_CHARGING);
            dataAction = new ActionData(this.getAgent().getAlias(), "charging");
        }
        state.setFuel(state.getFuel()+SmadhConstants.BOTS_REFUEL_UNITS_PER_PT);
        if(state.getFuel()>=SmadhConstants.BOTS_SWEEPERS_FUEL_MAX_CAPACITY){
            state.setStatus(SmadhConstants.STATUS_READY);
            dataAction = new ActionData(this.getAgent().getAlias(), "operating");
        }
        reportAction(dataAction);
    }
    
    private void useDetector(SweeperActionData data,SweeperState state){
        if(state.getStatus().equals(SmadhConstants.STATUS_READY)){
            changeToDetectingStatus(state);
            consumeEnergyPerPeriod(state);
            senseWorld(data,state);
        }
    }
    
    private void evaluateAndProceed(SweeperActionData data, SweeperState state){
        doWhateverIsPossible(data,state);
        insertPointIntoDronesMap(state);
        changeToReadyStatus(state);
    }
    
    private void doWhateverIsPossible(SweeperActionData data,SweeperState state){
        changeToMovingStatus(state);
        lookForDeminingTarget(data,state);
        if(state.getStatus().equals(SmadhConstants.STATUS_TARGET_AQUIRED)){
            startDemining(data,state);
            
        }else{
            lookForFreeSlotToMove(data,state);
            moveDroneAndReport(state);
            
        }
        insertPointIntoDronesMap(state);
    }
    
    private void changeToRestStatus(SweeperState state){
        state.setStatus(SmadhConstants.STATUS_REST);
    }
    
    private void changeToDetectingStatus(SweeperState state){
        state.setStatus(SmadhConstants.STATUS_DETECTING);
    }
    
    private void changeToReadyStatus(SweeperState state){
        state.setStatus(SmadhConstants.STATUS_READY);
    }
    
    private void changeToMovingStatus(SweeperState state){
        state.setStatus(SmadhConstants.STATUS_MOVING);
    }
    
    private void changeToDeminingStatus(SweeperState state){
        state.setStatus(SmadhConstants.STATUS_DEMINING);
    }
    
    private void changeToReturnToBaseStatus(SweeperState state){
        state.setStatus(SmadhConstants.STATUS_RETURNING_TO_BASE);
    }
    
    private void lookForDeminingTarget(SweeperActionData data,SweeperState state){
        List<Slot> slots = data.getSlots();
        for(Slot slot : slots){
            if(slot.isInhabited() && 
               (slot.getInhabitedType().equals(SmadhConstants.INHABITED_MINET1) ||
                slot.getInhabitedType().equals(SmadhConstants.INHABITED_MINET2))){
                state.setTargetx(slot.getX());
                state.setTargety(slot.getY());
                state.setTargetAlias(slot.getEntityAlias());
                state.setTargetEntityType(slot.getEntityType());
                state.setStatus(SmadhConstants.STATUS_TARGET_AQUIRED);
            }
        }
    }
    
    private void lookForFreeSlotToMove(SweeperActionData data,SweeperState state){
        List<Slot> slots = data.getSlots();
        slots = slots
                .stream()
                .filter(t-> t.isInhabited()==false)
                .collect(Collectors.toList());
        if(slots.isEmpty()){
            state.setTargetx(state.getX());
            state.setTargety(state.getY());
        }else{
            Random rand = new Random();
            System.out.println("tamañoSlots: " + slots.size());
            int slotIndex = rand.nextInt(slots.size());
            System.out.println("indice: " + slotIndex);
            state.setTargetx(slots.get(slotIndex).getX());
            state.setTargety(slots.get(slotIndex).getY());
        }
    }
    
    private void consumeEnergyPerPeriod(SweeperState state){
        state.setFuel(state.getFuel()-SmadhConstants.BOTS_SWEEPERS_FUEL_CONSUMPTION_PER_PT);
    }
    
    private void moveDroneAndReport(SweeperState state){
        state.setX(state.getTargetx());
        state.setY(state.getTargety());
        DataBESA dataAction;
        dataAction = new ActionData(this.getAgent().getAlias(), "move", state.getX(), state.getY());
        reportAction(dataAction);
        System.out.println("moviendose a: " + state.getX()+";"+ state.getY());
        consumeEnergyPerPeriod(state);
    }
    
    private void startDemining(SweeperActionData data, SweeperState state){
        changeToDeminingStatus(state); 
        consumeEnergyPerPeriod(state);
        DataBESA dataAction;
        dataAction = new ActionData(this.getAgent().getAlias(), "demine",
                                    state.getTargetx(),state.getTargety(),
                                    state.getTargetEntityType(),state.getActionForce());
        reportAction(dataAction);
    }
    
    private void beReadyForNextSearch(SweeperActionData data, SweeperState state){
        changeToReadyStatus(state);        
        DataBESA dataAction;
        dataAction = new ActionData(data.getAlias(), "cleared",data.getX(),data.getY(),data.getEntityType());
        reportAction(dataAction);
    }
    
    private void insertPointIntoDronesMap(SweeperState state){
        state.setPointToDroneMap();
    }
        
    private Pair<Integer,Integer> getOneStepCloserToOrigin(SweeperState state){
        int newx = state.getX();
        int newy = state.getY(); 
        int destinationx = state.getInitialx();
        int destinationy = state.getInitialy();
        if(destinationx - newx > 0){
            newx = newx + 1;
        }else if (destinationx - newx < 0){
            newx = newx - 1;
        }
        if(destinationy - newy > 0){
            newy = newy + 1;
        }else if(destinationy - newy < 0){
            newy = newy - 1;
        }
        return new Pair(newx,newy);
    }
    
    private void senseWorld(SweeperActionData data, SweeperState state){
        DataBESA dataAction;
        dataAction = new SweeperActionData(data.getAlias(),state.getX(),state.getY(),"sense","All");
        EventBESA senseQueryToWorld = new EventBESA(SenseQuerySweeperGuard.class.getName(),dataAction);
        AgHandlerBESA ah;
        try {
            ah = getAgent().getAdmLocal().getHandlerByAlias("WORLD");
            ah.sendEvent(senseQueryToWorld);
        } catch (ExceptionBESA e) {
          ReportBESA.error(e);
        }
    }
    
    private void reportAction(DataBESA dataAction){
        EventBESA graphicUpdateEvent = new EventBESA(UpdateSweeperGuard.class.getName(), dataAction);
        EventBESA controlReportEvent = new EventBESA(ReportSweeperGuard.class.getName(), dataAction);
        AgHandlerBESA ah;
        try {
            ah = getAgent().getAdmLocal().getHandlerByAlias("WORLD");
            ah.sendEvent(graphicUpdateEvent);
            ah = getAgent().getAdmLocal().getHandlerByAlias("CONTROL");
            ah.sendEvent(controlReportEvent);
        } catch (ExceptionBESA e) {
            ReportBESA.error(e);
        }
    }
    
    private void returnToBase(SweeperState state){
        changeToReturnToBaseStatus(state);
        Pair<Integer,Integer> newStep = getOneStepCloserToOrigin(state);
        DataBESA dataAction;
        double distance=Calculations.calculateDistanceBetweenPoints(state.getInitialx(), state.getInitialy(), state.getX(), state.getY());
        if(distance == 0){
            changeToRestStatus(state);
            dataAction = new ActionData(this.getAgent().getAlias(), "rest");
        }else{
            dataAction = new ActionData(this.getAgent().getAlias(), "move", newStep.getKey(), newStep.getValue());
            state.setX(newStep.getKey());
            state.setY(newStep.getValue());
        }
        reportAction(dataAction);
    }
}
