/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package smadh.control;

/**
 *
 * @author raxon
 */
public class ExplorationPoint {
    int x;
    int y;
    boolean explored;

    public boolean isExplored() {
        return explored;
    }

    public void setExplored(boolean explored) {
        this.explored = explored;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public ExplorationPoint(int x, int y){
        this.x = x;
        this.y = y;
    }
}
