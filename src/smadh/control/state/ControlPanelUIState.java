package smadh.control.state;

import BESA.Kernell.Agent.StateBESA;
import BESA.Kernell.System.AdmBESA;
import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.stream.Collectors;
import javafx.util.Pair;
import smadh.config.SmadhConstants;
import smadh.control.ControlPanelUI;
import smadh.control.ExplorationRoute;
import smadh.deminer.DeminerAgent;
import smadh.sweeper.SweeperAgent;
import smadh.tracer.TracerAgent;
import smadh.utils.Calculations;
import smadh.watchmec.WatchMecAgent;
import smadh.world.model.MineEntity;
import smadh.world.model.PersonEntity;
import smadh.world.model.Slot;

/**
 *
 * @author raxon
 */
public class ControlPanelUIState extends StateBESA {
    
    String alias;
    String controlPanelStatus;
    Boolean isPanelVisible = true;
    ControlPanelUI controlPanelUI;
    List<TracerAgent> tracersPool= new ArrayList<>();
    List<DeminerAgent> deminersPool= new ArrayList<>();
    List<SweeperAgent> sweepersPool= new ArrayList<>();
    List<WatchMecAgent> watchMecsPool= new ArrayList<>();
    List<MineEntity> landMineEntities;
    List<PersonEntity> personsInFieldEntities;
    
    List<ExplorationRoute> explorationRoutes;
    List<ExplorationRoute> explorationRoutesForWatchMecs;
    
    private List<String> botsAlias;

    public ControlPanelUIState(AdmBESA admLocal) {
        controlPanelUI = new ControlPanelUI(admLocal);
        controlPanelUI.setVisible(isPanelVisible);
        landMineEntities = new ArrayList<>();
        personsInFieldEntities = new ArrayList<>();
        createExplorationRoutes();
        createExplorationRoutesForWatchMecs();
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getControlPanelStatus() {
        return controlPanelStatus;
    }

    public void setControlPanelStatus(String controlPanelStatus) {
        this.controlPanelStatus = controlPanelStatus;
    }

    public ControlPanelUI getControlPanelUI() {
        return controlPanelUI;
    }
    
    public List<String> getBotsAlias() {
        return botsAlias;
    }

    public void setBotsAlias(List<String> botsAlias) {
        this.botsAlias = botsAlias;
    }
    
    public void increaseMarkedMines(int mineType){
        controlPanelUI.increaseMarkedMines(mineType);
    }
    
    public void increaseMarkedMines(int x, int y, int mineType){
        long mineInPos =landMineEntities.stream()
                        .filter(mine->mine.getXpos()==x && mine.getYpos()==y).count();
        if(mineInPos==0){
            landMineEntities.add(new MineEntity(mineType, x,y));
            controlPanelUI.increaseMarkedMines(mineType);
        }
    }
    
    public void increaseClearedPersons(String alias,int x,int y){
        controlPanelUI.increaseClearedPersons(alias,x,y);
    }
    
    public void reportDemining(int mineType,int x, int y){
        controlPanelUI.reportDemining(mineType,x,y);
    }
    
    public void reportMovementAndVerifyCrash(String alias,int x,int y){
        controlPanelUI.reportMovementAndVerifyCrash(alias,x,y);
    }
    
    public void reportClearing(){
        controlPanelUI.reportClearing();
    }
    
    public void increaseDisabledMines(String alias, int mineType,int x,int y){
        controlPanelUI.reportMineDisabling(alias,mineType,x,y);
        try{
            MineEntity mine =landMineEntities.stream().filter(lm ->lm.getType()==mineType && lm.getXpos()==x && lm.getYpos()==y).findFirst().get();
            int index = landMineEntities.indexOf(mine);
            mine.setCleared(true);
            mine.setMarked(true);
            mine.setAsigneesAlias(alias);
            landMineEntities.add(index, mine);
        }catch(NoSuchElementException e){}
        
    }
       
    
    public void putTracerInRest(String alias){
        //controlPanelUI.putTracerInRest(alias);
    }
    
    public void putTracerInRest(int energy,String alias){
        //controlPanelUI.putTracerInRest(alias);
    }
    
    public void putDeminerInRest(String alias){
        //controlPanelUI.putDeminerInRest(alias);
    }
    
    public void putSweeperInRest(String alias){
        //controlPanelUI.putSweeperInRest(alias);
    }
    
    public void putWatchMecInRest(String alias){
        //controlPanelUI.putWatchMecInRest(alias);
    }
        
    public void setUnitsInBase(Integer unitsInBase) {
        controlPanelUI.setUnitsInBase(unitsInBase);
    }

    public void setUnitsInOperation(Integer unitsInOperation) {
        controlPanelUI.setUnitsInOperation(unitsInOperation);
    }
    
    public void increaseTracerUnitsChargingAndDecreseOperationUnits(String alias){
        controlPanelUI.increaseTracerUnitsChargingAndDecreseOperationUnits(alias);
    }
    
    public void increaseDeminerUnitsChargingAndDecreseOperationUnits(String alias){
        controlPanelUI.increaseDeminerUnitsChargingAndDecreseOperationUnits(alias);
    }
    
    public void increaseSweeperUnitsChargingAndDecreseOperationUnits(String alias){
        controlPanelUI.increaseSweeperUnitsChargingAndDecreseOperationUnits(alias);
    }
    
    public void increaseWatchMecUnitsChargingAndDecreseOperationUnits(String alias){
        controlPanelUI.increaseWatchMecUnitsChargingAndDecreseOperationUnits(alias);
    }

    public void increaseTracerUnitsOperatingAndDecreseUnitsCharging(String alias){
        controlPanelUI.increaseTracerUnitsOperatingAndDecreseUnitsCharging(alias);
    }
    
    public void increaseDeminerUnitsOperatingAndDecreseUnitsCharging(String alias){
        controlPanelUI.increaseDeminerUnitsOperatingAndDecreseUnitsCharging(alias);
    }
    
    public void increaseSweeperUnitsOperatingAndDecreseUnitsCharging(String alias){
        controlPanelUI.increaseSweeperUnitsOperatingAndDecreseUnitsCharging(alias);
    }
    
    public void increaseWatchMecUnitsOperatingAndDecreseUnitsCharging(String alias){
        controlPanelUI.increaseWatchMecUnitsOperatingAndDecreseUnitsCharging(alias);
    }
    
    
    public void increaseTracerUnitsOperatingAndDecreseUnitsInRest(String alias){
        controlPanelUI.increaseTracerUnitsOperatingAndDecreseUnitsInRest(alias);
    }
    
    public void increaseDeminerUnitsOperatingAndDecreseUnitsInRest(String alias){
        controlPanelUI.increaseDeminerUnitsOperatingAndDecreseUnitsInRest(alias);
    }
    
    public void increaseSweeperUnitsOperatingAndDecreseUnitsInRest(String alias){
        controlPanelUI.increaseSweeperUnitsOperatingAndDecreseUnitsInRest(alias);
    }
    
    public void increaseWatchMecUnitsOperatingAndDecreseUnitsInRest(String alias){
        controlPanelUI.increaseWatchMecUnitsOperatingAndDecreseUnitsInRest(alias);
    }
    
    public void setTracersQty(Integer tracersQty) {
        controlPanelUI.setTracersQty(tracersQty);
    }

    public void setDeminersQty(Integer deminersQty) {
        controlPanelUI.setDeminersQty(deminersQty);
    }

    public void setSweepersQty(Integer sweepersQty) {
        controlPanelUI.setSweepersQty(sweepersQty);
    }

    public void setWatchMecsQty(Integer watchMecsQty) {
        controlPanelUI.setWatchMecsQty(watchMecsQty);
    }
    
    public List<TracerAgent> getTracersPool() {
        return tracersPool;
    }

    public void setTracersPool(List<TracerAgent> tracersPool) {
        this.tracersPool = tracersPool;
    }

    public List<DeminerAgent> getDeminersPool() {
        return deminersPool;
    }

    public void setDeminersPool(List<DeminerAgent> deminersPool) {
        this.deminersPool = deminersPool;
    }

    public List<SweeperAgent> getSweepersPool() {
        return sweepersPool;
    }

    public void setSweepersPool(List<SweeperAgent> sweepersPool) {
        this.sweepersPool = sweepersPool;
    }

    public List<WatchMecAgent> getWatchMecsPool() {
        return watchMecsPool;
    }

    public void setWatchMecsPool(List<WatchMecAgent> watchMecsPool) {
        this.watchMecsPool = watchMecsPool;
    }
    
    public void reportAsCompletedAnExplorationRoute(String alias){
        ExplorationRoute er = explorationRoutes.stream()
                              .filter(eroute-> eroute.isAssigned() && 
                                               eroute.getAssignedToAlias().equals(alias))
                              .findFirst()
                              .orElse(null);
        if(Objects.nonNull(er)){
            int index = explorationRoutes.indexOf(er);
            er.setAssigned(false);
            er.setCompleted(true);
            explorationRoutes.add(index, er);
            controlPanelUI.reportExplorationRouteCompleted(alias,er.getRouteNumber());
        }
    }
    
    
    synchronized public MineEntity getAnAvailableDeminingJob(String alias,int x, int y){
        List<MineEntity> mineEntities = new ArrayList<>();
        if(!sweepersPool.isEmpty() && !deminersPool.isEmpty()){
            if(alias.contains("S")){
                try{
                 mineEntities = landMineEntities.stream().filter(lm ->!lm.isCleared() && 
                                                                 //lm.getAsigneeAlias().equals("") && 
                                                                 lm.getType()==2)
                                                            .collect(Collectors.toList());
                }catch(ConcurrentModificationException e){}
            }else{
                try{
                 mineEntities = landMineEntities.stream().filter(lm ->!lm.isCleared() && 
                                                                 //lm.getAsigneeAlias().equals("") && 
                                                                 lm.getType()==1)
                                                            .collect(Collectors.toList());
                }catch(ConcurrentModificationException e){}
            }
        }else if(sweepersPool.isEmpty() || deminersPool.isEmpty()){
                        if(alias.contains("S")){
                try{
                 mineEntities = landMineEntities.stream().filter(lm ->!lm.isCleared() && 
                                                                 //lm.getAsigneeAlias().equals("") && 
                                                                 lm.getType()==2)
                                                            .collect(Collectors.toList());
                }catch(ConcurrentModificationException e){}
            }else{
                try{
                 mineEntities = landMineEntities.stream().filter(lm ->!lm.isCleared() && 
                                                                 //lm.getAsigneeAlias().equals("") && 
                                                                 lm.getType()==1)
                                                            .collect(Collectors.toList());
                }catch(ConcurrentModificationException e){}
            }
        }else{
            try{
                 mineEntities = landMineEntities.stream().filter(lm ->!lm.isCleared() && 
                                                                 lm.getAsigneeAlias().equals(""))
                                                            .collect(Collectors.toList());
            }catch(ConcurrentModificationException e){}
        }

        MineEntity closestMine=null;
        double initialDistance = 400;
        double distance;
        for(MineEntity me :mineEntities){
            distance=Calculations.calculateDistanceBetweenPoints(me.getXpos(), me.getYpos(), x, y);
            if(distance<initialDistance){
                closestMine = me;
                initialDistance=distance;
            }
        }
        if(Objects.nonNull(closestMine)){
            int index = landMineEntities.indexOf(closestMine);
            landMineEntities.get(index).setAsigneesAlias(alias);
        }
        return closestMine;
    }
    
    public ExplorationRoute getAFreeExplorationRoute(String alias){
        ExplorationRoute er = explorationRoutes.stream()
                              .filter(eroute-> !eroute.isAssigned() && 
                                               !eroute.isCompleted())
                              .findFirst()
                              .orElse(null);
        int index;
        if(Objects.isNull(er)){
            return er;
        }else{
            index = explorationRoutes.indexOf(er);
            er.setAssigned(true);
            er.setAssignedToAlias(alias);
            explorationRoutes.add(index, er);
            return er;
        }
    }
    
        public ExplorationRoute getAFreeExplorationRouteForWatchMec(String alias){
        ExplorationRoute er = explorationRoutesForWatchMecs.stream()
                              .filter(eroute-> !eroute.isAssigned() && 
                                               !eroute.isCompleted())
                              .findFirst()
                              .orElse(null);
        int index;
        if(Objects.isNull(er)){
            return er;
        }else{
            index = explorationRoutesForWatchMecs.indexOf(er);
            er.setAssigned(true);
            er.setAssignedToAlias(alias);
            explorationRoutesForWatchMecs.add(index, er);
            return er;
        }
    }
    
    
    public void createExplorationRoutes(){
        explorationRoutes = new ArrayList<>();
        List<Pair<Integer,Integer>> route1 = buildRoute1();
        List<Pair<Integer,Integer>> route2 = buildRoute2();
        List<Pair<Integer,Integer>> route3 = buildRoute3();
        List<Pair<Integer,Integer>> route4 = buildRoute4();
        explorationRoutes.add(new ExplorationRoute(route1,1));
        explorationRoutes.add(new ExplorationRoute(route2,2));
        explorationRoutes.add(new ExplorationRoute(route3,3));
        explorationRoutes.add(new ExplorationRoute(route4,4));
    }
    
    public void createExplorationRoutesForWatchMecs(){
        explorationRoutesForWatchMecs = new ArrayList<>();
        List<Pair<Integer,Integer>> route1 = buildRoute1();
        List<Pair<Integer,Integer>> route2 = buildRoute3();
        List<Pair<Integer,Integer>> route3 = buildRoute2();
        List<Pair<Integer,Integer>> route4 = buildRoute4();
        explorationRoutesForWatchMecs.add(new ExplorationRoute(route1,1));
        explorationRoutesForWatchMecs.add(new ExplorationRoute(route2,2));
        explorationRoutesForWatchMecs.add(new ExplorationRoute(route3,3));
        explorationRoutesForWatchMecs.add(new ExplorationRoute(route4,4));
    }
    

    
    public List<Pair<Integer,Integer>> buildRoute1(){
        List<Pair<Integer,Integer>> route1;
        route1 = new ArrayList<>();
        route1.add(new Pair(1,7));
        route1.add(new Pair(1,6));
        route1.add(new Pair(1,5));
        route1.add(new Pair(1,4));
        route1.add(new Pair(1,3));
        route1.add(new Pair(1,2));

        route1.add(new Pair(2,2));
        route1.add(new Pair(3,2));
        route1.add(new Pair(4,2));
        route1.add(new Pair(5,2));
        route1.add(new Pair(6,2));
        route1.add(new Pair(7,2));
        route1.add(new Pair(8,2));
        route1.add(new Pair(9,2));
        route1.add(new Pair(10,2));
        route1.add(new Pair(11,2));
        route1.add(new Pair(12,2));
        route1.add(new Pair(13,2));
        route1.add(new Pair(14,2));
        route1.add(new Pair(15,2));
        route1.add(new Pair(16,2));
        route1.add(new Pair(17,2));
        route1.add(new Pair(18,2));
        route1.add(new Pair(19,2));
        route1.add(new Pair(20,2));
        route1.add(new Pair(21,2));
        route1.add(new Pair(22,2));
        route1.add(new Pair(23,2));
        route1.add(new Pair(24,2));
        route1.add(new Pair(25,2));
        route1.add(new Pair(26,2));
        route1.add(new Pair(27,2));
        route1.add(new Pair(28,2));
        route1.add(new Pair(29,2));
        route1.add(new Pair(30,2));
        route1.add(new Pair(31,2));
        route1.add(new Pair(32,2));
        route1.add(new Pair(33,2));
        route1.add(new Pair(34,2));
        //route1.add(new Pair(35,2));

        //route1.add(new Pair(35,3));
        route1.add(new Pair(34,3));

        route1.add(new Pair(34,4));
        route1.add(new Pair(33,4));
        route1.add(new Pair(32,4));
        route1.add(new Pair(31,4));
        route1.add(new Pair(30,4));
        route1.add(new Pair(29,4));
        route1.add(new Pair(28,4));
        route1.add(new Pair(27,4));
        route1.add(new Pair(26,4));
        route1.add(new Pair(25,4));
        route1.add(new Pair(24,4));
        route1.add(new Pair(23,4));
        route1.add(new Pair(22,4));
        route1.add(new Pair(21,4));
        route1.add(new Pair(20,4));
        route1.add(new Pair(19,4));
        route1.add(new Pair(18,4));
        route1.add(new Pair(17,4));
        route1.add(new Pair(16,4));
        route1.add(new Pair(15,4));
        route1.add(new Pair(14,4));
        route1.add(new Pair(13,4));
        route1.add(new Pair(12,4));
        route1.add(new Pair(11,4));
        route1.add(new Pair(10,4));
        route1.add(new Pair(9,4));
        route1.add(new Pair(8,4));
        route1.add(new Pair(7,4));
        route1.add(new Pair(6,4));
        route1.add(new Pair(5,4));
        route1.add(new Pair(4,4));
        route1.add(new Pair(3,4));
        route1.add(new Pair(3,5));
        return route1;
    }
    
    public List<Pair<Integer,Integer>> buildRoute2(){
        List<Pair<Integer,Integer>> route2;
        route2 = new ArrayList<>();
        
        route2.add(new Pair(2,7));
        route2.add(new Pair(3,7));
        route2.add(new Pair(4,7));
        route2.add(new Pair(5,7));
        route2.add(new Pair(6,7));
        route2.add(new Pair(7,7));
        route2.add(new Pair(8,7));
        route2.add(new Pair(9,7));
        route2.add(new Pair(10,7));
        route2.add(new Pair(11,7));
        route2.add(new Pair(12,7));
        route2.add(new Pair(13,7));
        route2.add(new Pair(14,7));
        route2.add(new Pair(15,7));
        route2.add(new Pair(16,7));
        route2.add(new Pair(17,7));
        route2.add(new Pair(18,7));
        route2.add(new Pair(19,7));
        route2.add(new Pair(20,7));
        route2.add(new Pair(21,7));
        route2.add(new Pair(22,7));
        route2.add(new Pair(23,7));
        route2.add(new Pair(24,7));
        route2.add(new Pair(25,7));
        route2.add(new Pair(26,7));
        route2.add(new Pair(27,7));
        route2.add(new Pair(28,7));
        route2.add(new Pair(29,7));
        route2.add(new Pair(30,7));
        route2.add(new Pair(31,7));
        route2.add(new Pair(32,7));
        route2.add(new Pair(33,7));
        route2.add(new Pair(34,7));
        //route2.add(new Pair(35,7));
        
        route2.add(new Pair(34,8));
        //route2.add(new Pair(35,9));


        route2.add(new Pair(34,9));
        route2.add(new Pair(33,9));
        route2.add(new Pair(32,9));
        route2.add(new Pair(31,9));
        route2.add(new Pair(30,9));
        route2.add(new Pair(29,9));
        route2.add(new Pair(28,9));
        route2.add(new Pair(27,9));
        route2.add(new Pair(26,9));
        route2.add(new Pair(25,9));
        route2.add(new Pair(24,9));
        route2.add(new Pair(23,9));
        route2.add(new Pair(22,9));
        route2.add(new Pair(21,9));
        route2.add(new Pair(20,9));
        route2.add(new Pair(19,9));
        route2.add(new Pair(18,9));
        route2.add(new Pair(17,9));
        route2.add(new Pair(16,9));
        route2.add(new Pair(15,9));
        route2.add(new Pair(14,9));
        route2.add(new Pair(13,9));
        route2.add(new Pair(12,9));
        route2.add(new Pair(11,9));
        route2.add(new Pair(10,9));
        route2.add(new Pair(9,9));
        route2.add(new Pair(8,9));
        route2.add(new Pair(7,9));
        route2.add(new Pair(6,9));
        route2.add(new Pair(5,9));
        return route2;
    }
    
    public List<Pair<Integer,Integer>> buildRoute3(){
        List<Pair<Integer,Integer>> route3;
        route3 = new ArrayList<>();
        
        route3.add(new Pair(3,9));
        route3.add(new Pair(3,10));
        route3.add(new Pair(3,11));
        route3.add(new Pair(3,12));
        route3.add(new Pair(3,13));
        route3.add(new Pair(3,14));
        route3.add(new Pair(3,15));
        route3.add(new Pair(3,16));

        route3.add(new Pair(4,16));
        route3.add(new Pair(5,16));
        route3.add(new Pair(6,16));
        route3.add(new Pair(7,16));
        route3.add(new Pair(8,16));
        route3.add(new Pair(9,16));
        route3.add(new Pair(10,16));
        route3.add(new Pair(11,16));
        route3.add(new Pair(12,16));
        route3.add(new Pair(13,16));
        route3.add(new Pair(14,16));
        route3.add(new Pair(15,16));
        route3.add(new Pair(16,16));
        route3.add(new Pair(17,16));
        route3.add(new Pair(18,16));
        route3.add(new Pair(19,16));
        route3.add(new Pair(20,16));
        route3.add(new Pair(21,16));
        route3.add(new Pair(22,16));
        route3.add(new Pair(23,16));
        route3.add(new Pair(24,16));
        route3.add(new Pair(25,16));
        route3.add(new Pair(26,16));
        route3.add(new Pair(27,16));
        route3.add(new Pair(28,16));
        route3.add(new Pair(29,16));
        route3.add(new Pair(30,16));
        route3.add(new Pair(31,16));
        route3.add(new Pair(32,16));
        route3.add(new Pair(33,16));
        route3.add(new Pair(34,16));
        //route3.add(new Pair(35,16));
        route3.add(new Pair(34,17));
        //route3.add(new Pair(35,18));
        
        route3.add(new Pair(34,18));
        route3.add(new Pair(33,18));
        route3.add(new Pair(32,18));
        route3.add(new Pair(31,18));
        route3.add(new Pair(30,18));
        route3.add(new Pair(29,18));
        route3.add(new Pair(28,18));
        route3.add(new Pair(27,18));
        route3.add(new Pair(26,18));
        route3.add(new Pair(25,18));
        route3.add(new Pair(24,18));
        route3.add(new Pair(23,18));
        route3.add(new Pair(22,18));
        route3.add(new Pair(21,18));
        route3.add(new Pair(20,18));
        route3.add(new Pair(19,18));
        route3.add(new Pair(18,18));
        route3.add(new Pair(17,18));
        route3.add(new Pair(16,18));
        route3.add(new Pair(15,18));
        route3.add(new Pair(14,18));
        route3.add(new Pair(13,18));
        route3.add(new Pair(12,18));
        route3.add(new Pair(11,18));
        route3.add(new Pair(10,18));
        route3.add(new Pair(9,18));
        route3.add(new Pair(8,18));
        route3.add(new Pair(7,18));
        route3.add(new Pair(6,18));
        route3.add(new Pair(5,18));
        route3.add(new Pair(4,18));
        route3.add(new Pair(3,18));
        route3.add(new Pair(2,18));
        route3.add(new Pair(1,18));
        
        route3.add(new Pair(1,17));
        route3.add(new Pair(1,16));
        route3.add(new Pair(1,15));
        route3.add(new Pair(1,14));
        
        return route3;
    }
    
    public List<Pair<Integer,Integer>> buildRoute4(){
        List<Pair<Integer,Integer>> route4;
        route4 = new ArrayList<>();
        
        route4.add(new Pair(4,9));
        route4.add(new Pair(4,10));
        route4.add(new Pair(4,11));

        route4.add(new Pair(5,11));
        route4.add(new Pair(6,11));
        route4.add(new Pair(7,11));
        route4.add(new Pair(8,11));
        route4.add(new Pair(9,11));
        route4.add(new Pair(10,11));
        route4.add(new Pair(11,11));
        route4.add(new Pair(12,11));
        route4.add(new Pair(13,11));
        route4.add(new Pair(14,11));
        route4.add(new Pair(15,11));
        route4.add(new Pair(16,11));
        route4.add(new Pair(17,11));
        route4.add(new Pair(18,11));
        route4.add(new Pair(19,11));
        route4.add(new Pair(20,11));
        route4.add(new Pair(21,11));
        route4.add(new Pair(22,11));
        route4.add(new Pair(23,11));
        route4.add(new Pair(24,11));
        route4.add(new Pair(25,11));
        route4.add(new Pair(26,11));
        route4.add(new Pair(27,11));
        route4.add(new Pair(28,11));
        route4.add(new Pair(29,11));
        route4.add(new Pair(30,11));
        route4.add(new Pair(31,11));
        route4.add(new Pair(32,11));
        route4.add(new Pair(33,11));
        route4.add(new Pair(34,11));
        //route4.add(new Pair(35,11));
   

        route4.add(new Pair(34,12));
        //route4.add(new Pair(35,13));
        
        route4.add(new Pair(34,13));
        route4.add(new Pair(33,13));
        route4.add(new Pair(32,13));
        route4.add(new Pair(31,13));
        route4.add(new Pair(30,13));
        route4.add(new Pair(29,13));
        route4.add(new Pair(28,13));
        route4.add(new Pair(27,13));
        route4.add(new Pair(26,13));
        route4.add(new Pair(25,13));
        route4.add(new Pair(24,13));
        route4.add(new Pair(23,13));
        route4.add(new Pair(22,13));
        route4.add(new Pair(21,13));
        route4.add(new Pair(20,13));
        route4.add(new Pair(19,13));
        route4.add(new Pair(18,13));
        route4.add(new Pair(17,13));
        route4.add(new Pair(16,13));
        route4.add(new Pair(15,13));
        route4.add(new Pair(14,13));
        route4.add(new Pair(13,13));
        route4.add(new Pair(12,13));
        route4.add(new Pair(11,13));
        route4.add(new Pair(10,13));
        route4.add(new Pair(9,13));
        route4.add(new Pair(8,13));
        route4.add(new Pair(7,13));
        route4.add(new Pair(6,13));
        route4.add(new Pair(5,13));
        
        return route4;
    }
}
