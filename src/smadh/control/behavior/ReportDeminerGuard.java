/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package smadh.control.behavior;


import BESA.Kernell.Agent.Event.EventBESA;

import BESA.Kernell.Agent.GuardBESA;
import smadh.control.state.ControlPanelUIState;
import smadh.data.ActionData;


/**
 *
 * @author raxon
 */
public class ReportDeminerGuard extends GuardBESA {

    @Override
    public void funcExecGuard(EventBESA ebesa) {
        ActionData data = (ActionData) ebesa.getData();
        ControlPanelUIState state = (ControlPanelUIState)this.getAgent().getState();
        switch (data.getAction()) {
            case "operating":
                state.increaseDeminerUnitsOperatingAndDecreseUnitsCharging(data.getAlias());
                break;
            case "charging":
                state.increaseDeminerUnitsChargingAndDecreseOperationUnits(data.getAlias());
                break;
            case "rest":
                state.putDeminerInRest(data.getAlias());
                break;
            case "demine":
                state.reportDemining(data.getEntityType(),data.getX(),data.getY());
                break;
            case "move":
                state.reportMovementAndVerifyCrash(data.getAlias(),data.getX(), data.getY());
                break;
            case "cleared":
                state.increaseDisabledMines(data.getAlias(), data.getEntityType(), data.getX(), data.getY());
                break;
        }
        
    }
    
}
