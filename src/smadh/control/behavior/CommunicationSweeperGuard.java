/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package smadh.control.behavior;

import BESA.ExceptionBESA;
import BESA.Kernell.Agent.Event.DataBESA;
import BESA.Kernell.Agent.Event.EventBESA;
import BESA.Kernell.Agent.GuardBESA;
import BESA.Kernell.System.Directory.AgHandlerBESA;
import BESA.Log.ReportBESA;
import java.util.Objects;
import smadh.control.state.ControlPanelUIState;
import smadh.data.ActionData;
import smadh.data.DeminerActionData;
import smadh.data.SweeperActionData;
import smadh.sweeper.behavior.SweeperCooperativeActionGuard;
import smadh.world.model.MineEntity;


/**
 *
 * @author raxon
 */
public class CommunicationSweeperGuard extends GuardBESA {

    @Override
    public void funcExecGuard(EventBESA ebesa) {
        ActionData data = (ActionData) ebesa.getData();
        ControlPanelUIState state = (ControlPanelUIState)this.getAgent().getState();
        switch (data.getAction()) {
            case "requestForDemineJob":
                 lookForAvailableDeminingJob(state,data);
                break;
        }
        
    }
    
    private void lookForAvailableDeminingJob(ControlPanelUIState state,ActionData data){
        MineEntity mineEntity = state.getAnAvailableDeminingJob(data.getAlias(),data.getX(),data.getY());
                DataBESA dataAction;
                if(Objects.isNull(mineEntity)){
                    dataAction = new SweeperActionData("waitAndAskLater");
                }else{
                    dataAction = new SweeperActionData(mineEntity.getXpos(),mineEntity.getYpos(),"assingDemineJob",mineEntity.getType(),mineEntity.isMarked());
                }
                EventBESA queryAnswerToTracer = new EventBESA(SweeperCooperativeActionGuard.class.getName(),dataAction);
                AgHandlerBESA ah;
                try {
                    ah = getAgent().getAdmLocal().getHandlerByAlias(data.getAlias());
                    ah.sendEvent(queryAnswerToTracer);
                } catch (ExceptionBESA e) {
                    ReportBESA.error(e);
                }
    }
    

}
