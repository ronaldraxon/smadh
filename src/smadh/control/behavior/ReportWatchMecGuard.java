/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package smadh.control.behavior;


import BESA.Kernell.Agent.Event.EventBESA;

import BESA.Kernell.Agent.GuardBESA;
import smadh.control.state.ControlPanelUIState;
import smadh.data.ActionData;


/**
 *
 * @author raxon
 */
public class ReportWatchMecGuard extends GuardBESA {

  
    @Override
    public void funcExecGuard(EventBESA ebesa) {
        ActionData data = (ActionData) ebesa.getData();
        ControlPanelUIState state = (ControlPanelUIState)this.getAgent().getState();
        switch (data.getAction()) {
            case "operating":
                state.increaseWatchMecUnitsOperatingAndDecreseUnitsCharging(data.getAlias());
                break;
            case "charging":
                state.increaseWatchMecUnitsChargingAndDecreseOperationUnits(data.getAlias());
                break;
            case "rest":
                state.putWatchMecInRest(data.getAlias());
                break;
            case "clear":    
                state.reportClearing();
                break;
            case "cleared":
                state.increaseClearedPersons(data.getAlias(),data.getX(),data.getY());
                break;
                
        }
        
    }
    
}
