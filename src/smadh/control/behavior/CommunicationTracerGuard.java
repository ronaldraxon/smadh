package smadh.control.behavior;


import BESA.ExceptionBESA;
import BESA.Kernell.Agent.Event.DataBESA;
import BESA.Kernell.Agent.Event.EventBESA;

import BESA.Kernell.Agent.GuardBESA;
import BESA.Kernell.System.Directory.AgHandlerBESA;
import BESA.Log.ReportBESA;
import java.util.Objects;
import smadh.control.ExplorationRoute;
import smadh.control.state.ControlPanelUIState;
import smadh.data.ActionData;
import smadh.data.TracerActionData;
import smadh.tracer.behavior.TracerCooperativeActionGuard;


/**
 *
 * @author raxon
 */
public class CommunicationTracerGuard extends GuardBESA {

    @Override
    public void funcExecGuard(EventBESA ebesa) {
        ActionData data = (ActionData) ebesa.getData();
        ControlPanelUIState state = (ControlPanelUIState)this.getAgent().getState();
        switch (data.getAction()) {
            case "requestRoute":
                lookForFreeRoutesAndAnswer(state,data);
                break;
            case "reportCompletedRoute":
                reportACompletedRoute(state,data);
                break;
        }
        
    }
    
    private void reportACompletedRoute(ControlPanelUIState state,ActionData data){
        state.reportAsCompletedAnExplorationRoute(data.getAlias());
    }
    
    private void lookForFreeRoutesAndAnswer(ControlPanelUIState state,ActionData data){
                ExplorationRoute explorationRoute = state.getAFreeExplorationRoute(data.getAlias());
                DataBESA dataAction;
                if(Objects.isNull(explorationRoute)){
                    dataAction = new TracerActionData("returnToBase");
                }else{
                    dataAction = new TracerActionData("assignRoute",explorationRoute);
                }
                EventBESA queryAnswerToTracer = new EventBESA(TracerCooperativeActionGuard.class.getName(),dataAction);
                AgHandlerBESA ah;
                try {
                    ah = getAgent().getAdmLocal().getHandlerByAlias(data.getAlias());
                    ah.sendEvent(queryAnswerToTracer);
                } catch (ExceptionBESA e) {
                    ReportBESA.error(e);
                }
    }
}
