package smadh.control.behavior;


import BESA.Kernell.Agent.Event.EventBESA;

import BESA.Kernell.Agent.GuardBESA;
import smadh.config.SmadhConstants;
import smadh.control.state.ControlPanelUIState;
import smadh.data.ActionData;


/**
 *
 * @author raxon
 */
public class ReportTracerGuard extends GuardBESA {

    @Override
    public void funcExecGuard(EventBESA ebesa) {
        ActionData data = (ActionData) ebesa.getData();
        ControlPanelUIState state = (ControlPanelUIState)this.getAgent().getState();

            switch (data.getAction()) {
            case "operating":
                state.increaseTracerUnitsOperatingAndDecreseUnitsCharging(data.getAlias());
                break;
            case "charging":
                state.increaseTracerUnitsChargingAndDecreseOperationUnits(data.getAlias());
                break;
            case "rest":
                state.putTracerInRest(data.getAlias());
                break;
            case "mark":
                state.increaseMarkedMines(data.getX(),data.getY(),data.getEntityType());
                break;
            }
        
        
        
    }
    
}
