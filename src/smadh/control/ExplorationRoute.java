/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package smadh.control;

import java.util.ArrayList;
import java.util.List;
import javafx.util.Pair;

/**
 *
 * @author raxon
 */
public class ExplorationRoute {
    String assignedToAlias;
    int routeNumber;
    boolean assigned;
    boolean completed;
    List<ExplorationPoint> explorationPoints;
    
    public boolean isAssigned() {
        return assigned;
    }

    public void setAssigned(boolean assigned) {
        this.assigned = assigned;
    }
    
    public boolean isCompleted() {
        return completed;
    }

    public void setCompleted(boolean completed) {
        this.completed = completed;
    }
    public String getAssignedToAlias() {
        return assignedToAlias;
    }

    public void setAssignedToAlias(String assignedToAlias) {
        this.assignedToAlias = assignedToAlias;
    }

    public List<ExplorationPoint> getExplorationpPoints() {
        return explorationPoints;
    }

    public void setExplorationPoints(List<ExplorationPoint> explorationRoute) {
        this.explorationPoints = explorationRoute;
    }
    
    public int getRouteNumber() {
        return routeNumber;
    }
    
    public ExplorationRoute(List<Pair<Integer, Integer>> points, int routeNumber) {
        this.routeNumber = routeNumber;
        explorationPoints = new ArrayList<>();
        completed =false;
        assigned =false;
        points.forEach(pp -> explorationPoints.add(new ExplorationPoint(pp.getKey(),pp.getValue())) );
    }
    
}
