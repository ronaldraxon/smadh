package smadh.control;

import BESA.ExceptionBESA;

import BESA.Kernell.Agent.Event.EventBESA;
import BESA.Kernell.Agent.PeriodicGuardBESA;

import BESA.Kernell.System.AdmBESA;
import BESA.Kernell.System.Directory.AgHandlerBESA;

import BESA.Util.PeriodicDataBESA;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import javax.swing.JFrame;
import smadh.config.SmadhConstants;
import smadh.world.behavior.RunWorldGuard;
import smadh.world.model.MineEntity;
import smadh.world.model.PersonEntity;
import smadh.world.model.SmadhEntity;

/**
 *
 * @author raxon
 *//**
 *
 * @author raxon
 */
public class ControlPanelUI extends JFrame {
    
    private javax.swing.JButton jButtonDetenerSimulacion;
    private javax.swing.JButton jButtonIniciarSimulacion;
    private javax.swing.JLabel jLabelEnergiaConsumidaHeader;
    private javax.swing.JLabel jLabelEnergiaConsumidaValue;
    private javax.swing.JLabel jLabelEstadoOperacionHeader;
    private javax.swing.JLabel jLabelEstadoOperacionValue;
    private javax.swing.JLabel jLabelMinasDesactivadasHeader;
    private javax.swing.JLabel jLabelMinasEncontradasHeader;
    private javax.swing.JLabel jLabelMinasT1DesactivadasValue;
    private javax.swing.JLabel jLabelMinasT1EncontradasValue;
    private javax.swing.JLabel jLabelMinasT1Header;
    private javax.swing.JLabel jLabelMinasT2DesactivadasValue;
    private javax.swing.JLabel jLabelMinasT2EncontradasValue;
    private javax.swing.JLabel jLabelMinasT2Header;
    private javax.swing.JLabel jLabelPersonasEncontradasHeader;
    private javax.swing.JLabel jLabelPersonasEncontradasValue;
    private javax.swing.JLabel jLabelPersonasEvacuadasHeader;
    private javax.swing.JLabel jLabelPersonasEvacuadasValue;
    private javax.swing.JLabel jLabelTiempoTrascurridoHeader;
    private javax.swing.JLabel jLabelTiempoTrascurridoValue;
    private javax.swing.JLabel jLabelUnidadesBarredoresHeader;
    private javax.swing.JLabel jLabelUnidadesBarredoressValue;
    private javax.swing.JLabel jLabelUnidadesDesminadoresHeader;
    private javax.swing.JLabel jLabelUnidadesDesminadoresValue;
    private javax.swing.JLabel jLabelUnidadesDetectoresHeader;
    private javax.swing.JLabel jLabelUnidadesDetectoresValue;
    private javax.swing.JLabel jLabelUnidadesEnBaseHeader;
    private javax.swing.JLabel jLabelUnidadesEnBaseValue;
    private javax.swing.JLabel jLabelUnidadesEnOperacionHeader;
    private javax.swing.JLabel jLabelUnidadesEnOperacionValue;
    private javax.swing.JLabel jLabelUnidadesEnRepostajeHeader;
    private javax.swing.JLabel jLabelUnidadesEnRepostajeValue;
    private javax.swing.JLabel jLabelUnidadesVigilantesHeader;
    private javax.swing.JLabel jLabelUnidadesVigilantesValue;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTextArea jTextHistorialDeEventos;
    
    private StopWatch watch;
    
    private Integer markedT1Mines;
    private Integer markedT2Mines;
    private Integer disabledT1Mines;
    private Integer disabledT2Mines;
    private Integer clearedPersons;
    private Integer clearedPersonsInTime;
    private Integer consumedEnergy;


    private Integer unitsInBase;
    private Integer unitsInOperation;
    private Integer unitsCharging;
    private List<MineEntity> mineEntities;
    private List<PersonEntity> personEntities;
    private List<SmadhEntity> smadhEntities;
    private Integer collisions;
    private Integer recharges;
    private Integer tracersQty;
    private Integer deminersQty;
    private Integer sweepersQty;
    private Integer watchMecsQty;
            
    
    
    private StringBuilder historial;
    
    
    public static int GAME_PERIODIC_TIME = 100;//1000;
    public static int GAME_PERIODIC_DELAY_TIME = 10;//100;
    public static final int PASS =74261;
    
    AdmBESA admLocal;
    
    public ControlPanelUI(AdmBESA admLocal) {
        this.admLocal = admLocal;
        initComponents();
        watch = new StopWatch();
        markedT1Mines =0;
        markedT2Mines =0;
        disabledT1Mines =0;
        disabledT2Mines =0;
        clearedPersons = 0;
        clearedPersonsInTime = 0;
        unitsCharging = 0;
        consumedEnergy =0;
        recharges = 0;
        collisions = 0;
        personEntities = new ArrayList<>();
        mineEntities = new ArrayList<>();
        smadhEntities = new ArrayList<>();
        historial = new StringBuilder();
    }
    
    private void initComponents(){
        jButtonIniciarSimulacion = new javax.swing.JButton();
        jButtonDetenerSimulacion = new javax.swing.JButton();
        jPanel9 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTextHistorialDeEventos = new javax.swing.JTextArea();
        jPanel3 = new javax.swing.JPanel();
        jLabelUnidadesEnOperacionHeader = new javax.swing.JLabel();
        jLabelUnidadesEnOperacionValue = new javax.swing.JLabel();
        jLabelUnidadesEnRepostajeHeader = new javax.swing.JLabel();
        jLabelUnidadesEnRepostajeValue = new javax.swing.JLabel();
        jLabelUnidadesDesminadoresHeader = new javax.swing.JLabel();
        jLabelUnidadesBarredoresHeader = new javax.swing.JLabel();
        jLabelUnidadesDetectoresHeader = new javax.swing.JLabel();
        jLabelUnidadesVigilantesHeader = new javax.swing.JLabel();
        jLabelUnidadesDesminadoresValue = new javax.swing.JLabel();
        jLabelUnidadesBarredoressValue = new javax.swing.JLabel();
        jLabelUnidadesDetectoresValue = new javax.swing.JLabel();
        jLabelUnidadesVigilantesValue = new javax.swing.JLabel();
        jLabelUnidadesEnBaseHeader = new javax.swing.JLabel();
        jLabelUnidadesEnBaseValue = new javax.swing.JLabel();
        jPanel5 = new javax.swing.JPanel();
        jLabelMinasT2DesactivadasValue = new javax.swing.JLabel();
        jLabelMinasT1DesactivadasValue = new javax.swing.JLabel();
        jLabelMinasDesactivadasHeader = new javax.swing.JLabel();
        jLabelMinasEncontradasHeader = new javax.swing.JLabel();
        jLabelMinasT1EncontradasValue = new javax.swing.JLabel();
        jLabelMinasT2EncontradasValue = new javax.swing.JLabel();
        jLabelMinasT2Header = new javax.swing.JLabel();
        jLabelMinasT1Header = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();
        jLabelPersonasEncontradasHeader = new javax.swing.JLabel();
        jLabelPersonasEvacuadasHeader = new javax.swing.JLabel();
        jLabelPersonasEncontradasValue = new javax.swing.JLabel();
        jLabelPersonasEvacuadasValue = new javax.swing.JLabel();
        jPanel6 = new javax.swing.JPanel();
        jLabelTiempoTrascurridoValue = new javax.swing.JLabel();
        jLabelTiempoTrascurridoHeader = new javax.swing.JLabel();
        jPanel7 = new javax.swing.JPanel();
        jLabelEstadoOperacionHeader = new javax.swing.JLabel();
        jLabelEstadoOperacionValue = new javax.swing.JLabel();
        jPanel8 = new javax.swing.JPanel();
        jLabelEnergiaConsumidaHeader = new javax.swing.JLabel();
        jLabelEnergiaConsumidaValue = new javax.swing.JLabel();
        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Panel de Control SMADH");
        setResizable(false);

        jButtonIniciarSimulacion.setText("Iniciar");
        jButtonIniciarSimulacion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                try {
                    jButtonIniciarSimulacionActionPerformed(evt);
                } catch (ExceptionBESA ex) {
                    Logger.getLogger(ControlPanelUI.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });

        jButtonDetenerSimulacion.setText("Detener");
        jButtonDetenerSimulacion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                try {
                    jButtonDetenerSimulacionActionPerformed(evt);
                } catch (ExceptionBESA ex) {
                    Logger.getLogger(ControlPanelUI.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });

        jPanel9.setBorder(javax.swing.BorderFactory.createTitledBorder("Historial de eventos"));

        jTextHistorialDeEventos.setColumns(20);
        jTextHistorialDeEventos.setRows(5);
        jScrollPane2.setViewportView(jTextHistorialDeEventos);

        javax.swing.GroupLayout jPanel9Layout = new javax.swing.GroupLayout(jPanel9);
        jPanel9.setLayout(jPanel9Layout);
        jPanel9Layout.setHorizontalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel9Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane2)
                .addContainerGap())
        );
        jPanel9Layout.setVerticalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel9Layout.createSequentialGroup()
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 128, Short.MAX_VALUE)
                .addContainerGap())
        );

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder("Unidades Operativas"));

        jLabelUnidadesEnOperacionHeader.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabelUnidadesEnOperacionHeader.setText("Unidades en operación:");

        jLabelUnidadesEnOperacionValue.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabelUnidadesEnOperacionValue.setForeground(new java.awt.Color(0, 0, 0));
        jLabelUnidadesEnOperacionValue.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabelUnidadesEnOperacionValue.setText("0");

        jLabelUnidadesEnRepostajeHeader.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabelUnidadesEnRepostajeHeader.setText("Unidades en repostaje:");

        jLabelUnidadesEnRepostajeValue.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabelUnidadesEnRepostajeValue.setForeground(new java.awt.Color(0, 0, 0));
        jLabelUnidadesEnRepostajeValue.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabelUnidadesEnRepostajeValue.setText("0");

        jLabelUnidadesDesminadoresHeader.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabelUnidadesDesminadoresHeader.setText("Cantidad de desminadores:");

        jLabelUnidadesBarredoresHeader.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabelUnidadesBarredoresHeader.setText("Cantidad de barredores:      ");

        jLabelUnidadesDetectoresHeader.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabelUnidadesDetectoresHeader.setText("Cantidad de detectores:      ");

        jLabelUnidadesVigilantesHeader.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabelUnidadesVigilantesHeader.setText("Cantidad de vigilantes:        ");

        jLabelUnidadesDesminadoresValue.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabelUnidadesDesminadoresValue.setForeground(new java.awt.Color(0, 0, 0));
        jLabelUnidadesDesminadoresValue.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabelUnidadesDesminadoresValue.setText("0");

        jLabelUnidadesBarredoressValue.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabelUnidadesBarredoressValue.setForeground(new java.awt.Color(0, 0, 0));
        jLabelUnidadesBarredoressValue.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabelUnidadesBarredoressValue.setText("0");

        jLabelUnidadesDetectoresValue.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabelUnidadesDetectoresValue.setForeground(new java.awt.Color(0, 0, 0));
        jLabelUnidadesDetectoresValue.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabelUnidadesDetectoresValue.setText("0");

        jLabelUnidadesVigilantesValue.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabelUnidadesVigilantesValue.setForeground(new java.awt.Color(0, 0, 0));
        jLabelUnidadesVigilantesValue.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabelUnidadesVigilantesValue.setText("0");
        
        jLabelUnidadesEnBaseHeader.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabelUnidadesEnBaseHeader.setText("Unidades en reposo:");

        jLabelUnidadesEnBaseValue.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabelUnidadesEnBaseValue.setForeground(new java.awt.Color(0, 0, 0));
        jLabelUnidadesEnBaseValue.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabelUnidadesEnBaseValue.setText("0");

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(jLabelUnidadesEnBaseHeader, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabelUnidadesDesminadoresHeader, javax.swing.GroupLayout.DEFAULT_SIZE, 165, Short.MAX_VALUE)
                    .addComponent(jLabelUnidadesDetectoresHeader, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabelUnidadesBarredoresHeader, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabelUnidadesVigilantesHeader, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabelUnidadesEnOperacionHeader, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabelUnidadesEnRepostajeHeader, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(7, 7, 7)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabelUnidadesBarredoressValue, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 115, Short.MAX_VALUE)
                    .addComponent(jLabelUnidadesDesminadoresValue, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabelUnidadesDetectoresValue, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabelUnidadesVigilantesValue, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabelUnidadesEnOperacionValue, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabelUnidadesEnRepostajeValue, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabelUnidadesEnBaseValue, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addGap(14, 14, 14)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelUnidadesEnBaseHeader)
                    .addComponent(jLabelUnidadesEnBaseValue))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelUnidadesEnOperacionHeader, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabelUnidadesEnOperacionValue))
                .addGap(5, 5, 5)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelUnidadesEnRepostajeHeader)
                    .addComponent(jLabelUnidadesEnRepostajeValue, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelUnidadesDetectoresHeader, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabelUnidadesDetectoresValue))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelUnidadesDesminadoresHeader)
                    .addComponent(jLabelUnidadesDesminadoresValue))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabelUnidadesBarredoressValue)
                    .addComponent(jLabelUnidadesBarredoresHeader, javax.swing.GroupLayout.Alignment.TRAILING))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelUnidadesVigilantesValue)
                    .addComponent(jLabelUnidadesVigilantesHeader, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        jPanel5.setBorder(javax.swing.BorderFactory.createTitledBorder("Artefactos Explosivos"));
        jLabelMinasT2DesactivadasValue.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        jLabelMinasT2DesactivadasValue.setForeground(new java.awt.Color(0, 204, 102));
        jLabelMinasT2DesactivadasValue.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabelMinasT2DesactivadasValue.setText("0");
        jLabelMinasT1DesactivadasValue.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        jLabelMinasT1DesactivadasValue.setForeground(new java.awt.Color(0, 204, 102));
        jLabelMinasT1DesactivadasValue.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabelMinasT1DesactivadasValue.setText("0");
        jLabelMinasDesactivadasHeader.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabelMinasDesactivadasHeader.setText("Desactivadas");
        jLabelMinasEncontradasHeader.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabelMinasEncontradasHeader.setText("Encontradas");
        jLabelMinasT1EncontradasValue.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        jLabelMinasT1EncontradasValue.setForeground(new java.awt.Color(255, 0, 0));
        jLabelMinasT1EncontradasValue.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabelMinasT1EncontradasValue.setText("0");
        jLabelMinasT2EncontradasValue.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        jLabelMinasT2EncontradasValue.setForeground(new java.awt.Color(255, 0, 0));
        jLabelMinasT2EncontradasValue.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabelMinasT2EncontradasValue.setText("0");
        jLabelMinasT2Header.setText("Minas t2");
        jLabelMinasT1Header.setText("Minas t1");
        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addGap(78, 78, 78)
                .addComponent(jLabelMinasEncontradasHeader, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabelMinasDesactivadasHeader, javax.swing.GroupLayout.PREFERRED_SIZE, 104, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                .addContainerGap(11, Short.MAX_VALUE)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addComponent(jLabelMinasT2Header)
                        .addGap(18, 18, 18)
                        .addComponent(jLabelMinasT2EncontradasValue, javax.swing.GroupLayout.PREFERRED_SIZE, 87, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabelMinasT2DesactivadasValue, javax.swing.GroupLayout.PREFERRED_SIZE, 98, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addComponent(jLabelMinasT1Header)
                        .addGap(18, 18, 18)
                        .addComponent(jLabelMinasT1EncontradasValue, javax.swing.GroupLayout.PREFERRED_SIZE, 87, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabelMinasT1DesactivadasValue, javax.swing.GroupLayout.PREFERRED_SIZE, 98, javax.swing.GroupLayout.PREFERRED_SIZE))))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelMinasDesactivadasHeader)
                    .addComponent(jLabelMinasEncontradasHeader))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabelMinasT1DesactivadasValue)
                            .addComponent(jLabelMinasT1EncontradasValue))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabelMinasT2DesactivadasValue)
                            .addComponent(jLabelMinasT2EncontradasValue)))
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addComponent(jLabelMinasT1Header, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabelMinasT2Header, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(7, 7, 7)))
                .addContainerGap())
        );

        jPanel4.setBorder(javax.swing.BorderFactory.createTitledBorder("Personas"));
        jLabelPersonasEncontradasHeader.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabelPersonasEncontradasHeader.setText("Encontradas");
        jLabelPersonasEvacuadasHeader.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabelPersonasEvacuadasHeader.setText("Evacuadas");
        jLabelPersonasEncontradasValue.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        jLabelPersonasEncontradasValue.setForeground(new java.awt.Color(255, 0, 0));
        jLabelPersonasEncontradasValue.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabelPersonasEncontradasValue.setText("0");
        jLabelPersonasEvacuadasValue.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        jLabelPersonasEvacuadasValue.setForeground(new java.awt.Color(0, 204, 102));
        jLabelPersonasEvacuadasValue.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabelPersonasEvacuadasValue.setText("0");
        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabelPersonasEncontradasHeader, javax.swing.GroupLayout.PREFERRED_SIZE, 87, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabelPersonasEncontradasValue, javax.swing.GroupLayout.PREFERRED_SIZE, 97, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(28, 28, 28)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabelPersonasEvacuadasValue, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabelPersonasEvacuadasHeader, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelPersonasEvacuadasHeader)
                    .addComponent(jLabelPersonasEncontradasHeader))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelPersonasEncontradasValue, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabelPersonasEvacuadasValue, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        );
        jLabelTiempoTrascurridoValue.setFont(new java.awt.Font("Tahoma", 0, 36)); // NOI18N
        jLabelTiempoTrascurridoValue.setForeground(new java.awt.Color(255, 102, 0));
        jLabelTiempoTrascurridoValue.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabelTiempoTrascurridoValue.setText("00:00:00");
        jLabelTiempoTrascurridoHeader.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabelTiempoTrascurridoHeader.setText("Tiempo trascurrido:");

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addComponent(jLabelTiempoTrascurridoValue, javax.swing.GroupLayout.PREFERRED_SIZE, 177, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(jLabelTiempoTrascurridoHeader, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabelTiempoTrascurridoHeader, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabelTiempoTrascurridoValue)
                .addGap(32, 32, 32))
        );

        jLabelEstadoOperacionHeader.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabelEstadoOperacionHeader.setText("Estado de Operación:");

        jLabelEstadoOperacionValue.setFont(new java.awt.Font("Tahoma", 0, 36)); // NOI18N
        jLabelEstadoOperacionValue.setForeground(new java.awt.Color(0, 204, 102));
        jLabelEstadoOperacionValue.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabelEstadoOperacionValue.setText("En Espera");

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addComponent(jLabelEstadoOperacionHeader, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabelEstadoOperacionValue, javax.swing.GroupLayout.PREFERRED_SIZE, 231, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabelEstadoOperacionHeader)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabelEstadoOperacionValue))
        );

        jLabelEnergiaConsumidaHeader.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabelEnergiaConsumidaHeader.setText("Energía Consumida:");

        jLabelEnergiaConsumidaValue.setFont(new java.awt.Font("Tahoma", 0, 36)); // NOI18N
        jLabelEnergiaConsumidaValue.setForeground(new java.awt.Color(0, 153, 153));
        jLabelEnergiaConsumidaValue.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabelEnergiaConsumidaValue.setText("0");

        javax.swing.GroupLayout jPanel8Layout = new javax.swing.GroupLayout(jPanel8);
        jPanel8.setLayout(jPanel8Layout);
        jPanel8Layout.setHorizontalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel8Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabelEnergiaConsumidaValue, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabelEnergiaConsumidaHeader, javax.swing.GroupLayout.DEFAULT_SIZE, 122, Short.MAX_VALUE))
                .addGap(22, 22, 22))
        );
        jPanel8Layout.setVerticalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabelEnergiaConsumidaHeader)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabelEnergiaConsumidaValue)
                .addContainerGap())
        );

        jLabelEnergiaConsumidaHeader.getAccessibleContext().setAccessibleName("JLabelenergiaConsumidaHeader");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jButtonIniciarSimulacion, javax.swing.GroupLayout.PREFERRED_SIZE, 408, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(12, 12, 12)
                        .addComponent(jButtonDetenerSimulacion, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(12, 12, 12))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jPanel8, javax.swing.GroupLayout.PREFERRED_SIZE, 134, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(21, 21, 21))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jPanel9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addContainerGap())))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, 76, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(14, 14, 14)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButtonIniciarSimulacion, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButtonDetenerSimulacion, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        jPanel3.getAccessibleContext().setAccessibleName("");

        getAccessibleContext().setAccessibleDescription("");
        getAccessibleContext().setAccessibleParent(this);

        pack();
    }
                                                     
    private void jButtonIniciarSimulacionActionPerformed(java.awt.event.ActionEvent evt)throws ExceptionBESA {                                                         

        PeriodicDataBESA data  = new PeriodicDataBESA(SmadhConstants.PERIODIC_TIME, SmadhConstants.PERIODIC_DELAY_TIME, 
                                                      PeriodicGuardBESA.START_PERIODIC_CALL);
        
        EventBESA startPeriodicEv = new EventBESA(RunWorldGuard.class.getName(), data);
        
        AgHandlerBESA ah = this.admLocal.getHandlerByAlias("WORLD");
        ah.sendEvent(startPeriodicEv); 
        watch = new StopWatch();
        watch.start(jLabelTiempoTrascurridoValue);
        unitsInOperation = unitsInBase;    
        unitsInBase = unitsInBase - unitsInOperation;
        //jLabelUnidadesEnBaseValue.setText(unitsInBase.toString());
        jLabelUnidadesEnBaseValue.setText("0");
        Integer unidades = SmadhConstants.MAX_DEMINERS_QTY + SmadhConstants.MAX_SWEEPERS_QTY + SmadhConstants.MAX_WATCHMECS_QTY + SmadhConstants.MAX_TRACERS_QTY;
        jLabelUnidadesEnOperacionValue.setText(String.valueOf(unidades));
        jLabelEstadoOperacionValue.setForeground(new java.awt.Color(250, 0, 0));
        jLabelEstadoOperacionValue.setText("En ejecución");
        historial.append(LocalDateTime.now()).append(":_Inicio de Operación").append("\n");
        jTextHistorialDeEventos.setText(historial.toString());
        jButtonIniciarSimulacion.setEnabled(false);
    }                                                        

    private void jButtonDetenerSimulacionActionPerformed(java.awt.event.ActionEvent evt) throws ExceptionBESA {                                                         
        PeriodicDataBESA data  = new PeriodicDataBESA(SmadhConstants.PERIODIC_TIME, SmadhConstants.PERIODIC_DELAY_TIME, 
                                                      PeriodicGuardBESA.SUSPEND_PERIODIC_CALL);
        EventBESA startPeriodicEv = new EventBESA(RunWorldGuard.class.getName(), data);
        AgHandlerBESA ah = this.admLocal.getHandlerByAlias("WORLD");
        ah.sendEvent(startPeriodicEv);
        watch.stop();
        //jLabelUnidadesEnBaseValue.setText(unitsInBase.toString());
        Integer unidades = SmadhConstants.MAX_DEMINERS_QTY + SmadhConstants.MAX_SWEEPERS_QTY + SmadhConstants.MAX_WATCHMECS_QTY + SmadhConstants.MAX_TRACERS_QTY;
        jLabelUnidadesEnBaseValue.setText(String.valueOf(unidades));
        jLabelUnidadesEnOperacionValue.setText(String.valueOf("0"));
        
        jButtonIniciarSimulacion.setEnabled(true);
        historial.append(LocalDateTime.now()).append(":_Se ha detenido la operación").append("\n");
        historial.append(":_Tiempo de ejecución: ").append(jLabelTiempoTrascurridoValue.getText()).append("\n");
        historial.append(":_Energía empleada: ").append(consumedEnergy.toString()).append("\n");
        historial.append(":_Cantidad de repostajes: ").append(recharges.toString()).append("\n");
        historial.append(":_Cantidad de colisiones: ").append(collisions.toString()).append("\n");
        jTextHistorialDeEventos.setText(historial.toString());
        jLabelEstadoOperacionValue.setForeground(new java.awt.Color(0, 204, 102));
        jLabelEstadoOperacionValue.setText("En Espera");
        
    }   
    
    public void increaseMarkedMines(int mineType){
        if(mineType!=-1){
          if(mineType==1){
              markedT1Mines++;
              jLabelMinasT1EncontradasValue.setText(markedT1Mines.toString());
              historial.append(LocalDateTime.now()).append(":_Mina tipo 1 encontrada").append("\n");
              jTextHistorialDeEventos.setText(historial.toString());
          }  
          if(mineType==2){
              markedT2Mines++;
              jLabelMinasT2EncontradasValue.setText(markedT2Mines.toString());
              historial.append(LocalDateTime.now()).append(":_Mina tipo 2 encontrada").append("\n");
              jTextHistorialDeEventos.setText(historial.toString());
          } 
        }
    }
    
    public void reportDemining(int mineType,int x, int y){
        if(mineType!=-1){
        int mineIndex = findMine(mineEntities,x,y,mineType);
            if(mineIndex == -1){
                mineEntities.add(new MineEntity(mineType,x,y));
                if(mineType==1){
                    historial.append(LocalDateTime.now()).append(":_Se inicia despeje de  Mina tipo 1 en ").append(x).append(";").append(y).append("\n");
                    jTextHistorialDeEventos.setText(historial.toString());
                }  
                if(mineType==2){
                    historial.append(LocalDateTime.now()).append(":_Se inicia despeje de  Mina tipo 2 en ").append(x).append(";").append(y).append("\n");
                    jTextHistorialDeEventos.setText(historial.toString());
                } 
            }
        }
    }
    
    public void reportMovementAndVerifyCrash(String alias,int x, int y){
        //Colocar en historial a donde se deplaza?
        int currentEntityIndex = findSmadhEntityByAlias(smadhEntities,alias);
        if(currentEntityIndex == -1){
            smadhEntities.add(new SmadhEntity(alias,x,y));
        }else{
            smadhEntities.get(currentEntityIndex).setXpos(x);
            smadhEntities.get(currentEntityIndex).setYpos(y);
        }
        List<SmadhEntity> entidadesEnPosicion = getSmadhEntitiesByPosition(smadhEntities,x,y);
        if(entidadesEnPosicion.size()>1){
            collisions++;
            historial.append(LocalDateTime.now()).append(":_Se presentó una colisión").append(" en posición: ").append(x).append(";").append(y).append(" por: ").append(alias).append("\n");
            jTextHistorialDeEventos.setText(historial.toString());
            consumedEnergy = consumedEnergy + SmadhConstants.COLLISION_PENALTY;
            jLabelEnergiaConsumidaValue.setText(consumedEnergy.toString());
            //Se reporta crash
        }
    }
    
    private List<SmadhEntity> getSmadhEntitiesByPosition(List<SmadhEntity> entity, int xpos, int ypos) {
        return entity.stream()
                     .filter(e-> e.getXpos()==xpos && e.getYpos()==ypos)
                     .collect(Collectors.toList());
    }
    
    private int findSmadhEntityByAlias(List<SmadhEntity> entity,String alias) {
        for (int i = 0; i < entity.size(); i++) {
            if (alias.equals(entity.get(i).getAlias())) {
                return i;
            }
        }
        return -1;
    }
    
    private int findMine(List<MineEntity> mine, int xpos, int ypos,int type) {
        for (int i = 0; i < mine.size(); i++) {
            if (xpos == mine.get(i).getXpos() && ypos == mine.get(i).getYpos() && type == mine.get(i).getType()) {
                return i;
            }
        }
        return -1;
    }
    
    
    
    public void reportClearing(){
        historial.append(LocalDateTime.now()).append(":_Se inicia el despeje de una persona").append("\n");
        jTextHistorialDeEventos.setText(historial.toString());
    }
    
    
    public void reportMineDisabling(String alias,int mineType,int x,int y){
       if(!Objects.isNull(alias)){
           if(mineType!=-1){
          if(mineType==1){
              disabledT1Mines++;
              jLabelMinasT1DesactivadasValue.setText(disabledT1Mines.toString());
              historial.append(LocalDateTime.now()).append(":_Mina tipo 1 deshabilitada").append(" en posición: ").append(x).append(";").append(y).append(" por: ").append(alias).append("\n");
              jTextHistorialDeEventos.setText(historial.toString());
          }  
          if(mineType==2){
              disabledT2Mines++;
              jLabelMinasT2DesactivadasValue.setText(disabledT2Mines.toString());
              historial.append(LocalDateTime.now()).append(":_Mina tipo 2 deshabilitada").append(" en posición: ").append(x).append(";").append(y).append(" por: ").append(alias).append("\n");
              jTextHistorialDeEventos.setText(historial.toString());
          } 
        }
       }
    }
    
    public void increaseClearedPersons(String alias,int x,int y){
        if(!Objects.isNull(alias)){
            clearedPersons++;
            clearedPersonsInTime++;
            jLabelPersonasEncontradasValue.setText(clearedPersons.toString());
            jLabelPersonasEvacuadasValue.setText(clearedPersonsInTime.toString());
            historial.append(LocalDateTime.now()).append(":_Despeje de persona de zona de peligro").append("\n");
            jTextHistorialDeEventos.setText(historial.toString());
        }
        
    }
    
    public void putTracerInRest(String alias){
        historial.append(LocalDateTime.now()).append(":_Detector (Tracer): ").append(alias).append(", se ha puesto en modo reposo").append("\n");
        jTextHistorialDeEventos.setText(historial.toString());
        unitsInBase = unitsInBase + 1;
        unitsInOperation = unitsInOperation - 1;
        jLabelUnidadesEnBaseValue.setText(unitsInBase.toString());
        jLabelUnidadesEnOperacionValue.setText(unitsInOperation.toString());
    }
    
    public void putTracerInRest(int energy, String alias){
        historial.append(LocalDateTime.now()).append(":_Detector (Tracer): ").append(alias).append(", se ha puesto en modo reposo").append("\n");
        jTextHistorialDeEventos.setText(historial.toString());
        unitsInBase = unitsInBase + 1;
        unitsInOperation = unitsInOperation - 1;
        jLabelUnidadesEnBaseValue.setText(unitsInBase.toString());
        jLabelUnidadesEnOperacionValue.setText(unitsInOperation.toString());
        consumedEnergy = consumedEnergy +energy;
        jLabelEnergiaConsumidaValue.setText(consumedEnergy.toString());
    }
    
    public void putDeminerInRest(String alias){
        historial.append(LocalDateTime.now()).append(":_Desminador (Deminer): ").append(alias).append(", se ha puesto en modo reposo").append("\n");
        jTextHistorialDeEventos.setText(historial.toString());
        unitsInBase = unitsInBase + 1;
        unitsInOperation = unitsInOperation - 1;
        jLabelUnidadesEnBaseValue.setText(unitsInBase.toString());
        jLabelUnidadesEnOperacionValue.setText(unitsInOperation.toString());
    }
    
    public void putSweeperInRest(String alias){
        historial.append(LocalDateTime.now()).append(":_Barredor (Sweeper): ").append(alias).append(", se ha puesto en modo reposo").append("\n");
        jTextHistorialDeEventos.setText(historial.toString());
        unitsInBase = unitsInBase + 1;
        unitsInOperation = unitsInOperation - 1;
        jLabelUnidadesEnBaseValue.setText(unitsInBase.toString());
        jLabelUnidadesEnOperacionValue.setText(unitsInOperation.toString());
    }
    
    public void putWatchMecInRest(String alias){
        historial.append(LocalDateTime.now()).append(":_Vigilante (WatchMec): ").append(alias).append(", se ha puesto en modo reposo").append("\n");
        jTextHistorialDeEventos.setText(historial.toString());
        unitsInBase = unitsInBase + 1;
        unitsInOperation = unitsInOperation - 1;
        jLabelUnidadesEnBaseValue.setText(unitsInBase.toString());
        jLabelUnidadesEnOperacionValue.setText(unitsInOperation.toString());
    }
        
    public void increaseTracerUnitsChargingAndDecreseOperationUnits(String alias){
        recharges++; 
        unitsCharging = unitsCharging + 1;
        unitsInOperation = unitsInOperation - 1;
        jLabelUnidadesEnRepostajeValue.setText(unitsCharging.toString());
        jLabelUnidadesEnOperacionValue.setText(unitsInOperation.toString());
        historial.append(LocalDateTime.now()).append(":_Detector (Tracer): ").
                  append(alias).append(", se ha puesto en modo recarga").append("\n");
        jTextHistorialDeEventos.setText(historial.toString());
        consumedEnergy = consumedEnergy +SmadhConstants.BOTS_TRACERS_FUEL_MAX_CAPACITY;
        jLabelEnergiaConsumidaValue.setText(consumedEnergy.toString());
    }
    public void increaseDeminerUnitsChargingAndDecreseOperationUnits(String alias){
        recharges++;
        unitsCharging = unitsCharging + 1;
        unitsInOperation = unitsInOperation -1;
        jLabelUnidadesEnRepostajeValue.setText(unitsCharging.toString());
        jLabelUnidadesEnOperacionValue.setText(unitsInOperation.toString());
        historial.append(LocalDateTime.now()).append(":_Desminador (Deminer): ")
                 .append(alias).append(", se ha puesto en modo recarga").append("\n");
        jTextHistorialDeEventos.setText(historial.toString());
        consumedEnergy = consumedEnergy +SmadhConstants.BOTS_DEMINERS_FUEL_MAX_CAPACITY;
        jLabelEnergiaConsumidaValue.setText(consumedEnergy.toString());
    }
    public void increaseSweeperUnitsChargingAndDecreseOperationUnits(String alias){
        recharges++;
        unitsCharging = unitsCharging + 1;
        unitsInOperation = unitsInOperation -1;
        jLabelUnidadesEnRepostajeValue.setText(unitsCharging.toString());
        jLabelUnidadesEnOperacionValue.setText(unitsInOperation.toString());
        historial.append(LocalDateTime.now()).append(":_Barredor (Sweeper): ")
                 .append(alias).append(", se ha puesto en modo recarga").append("\n");
        jTextHistorialDeEventos.setText(historial.toString());
        consumedEnergy = consumedEnergy +SmadhConstants.BOTS_SWEEPERS_FUEL_MAX_CAPACITY;
        jLabelEnergiaConsumidaValue.setText(consumedEnergy.toString());
    }
    
    public void increaseWatchMecUnitsChargingAndDecreseOperationUnits(String alias){ 
        recharges++;
        unitsCharging = unitsCharging + 1;
        unitsInOperation = unitsInOperation -1;
        jLabelUnidadesEnRepostajeValue.setText(unitsCharging.toString());
        jLabelUnidadesEnOperacionValue.setText(unitsInOperation.toString());
        historial.append(LocalDateTime.now()).append(":_Vigilante (WatchMec): ")
                 .append(alias).append(", se ha puesto en modo recarga").append("\n");
        jTextHistorialDeEventos.setText(historial.toString());
        consumedEnergy = consumedEnergy +SmadhConstants.BOTS_WATCHMECS_FUEL_MAX_CAPACITY;
        jLabelEnergiaConsumidaValue.setText(consumedEnergy.toString());
        
    }

    public void increaseTracerUnitsOperatingAndDecreseUnitsCharging(String alias){
        unitsCharging = unitsCharging - 1;
        unitsInOperation = unitsInOperation + 1;
        jLabelUnidadesEnRepostajeValue.setText(unitsCharging.toString());
        jLabelUnidadesEnOperacionValue.setText(unitsInOperation.toString());
        historial.append(LocalDateTime.now()).append(":_Detector (Tracer): ")
                 .append(alias).append(", se ha puesto en modo operación").append("\n");
        jTextHistorialDeEventos.setText(historial.toString());
    }
    
    public void increaseDeminerUnitsOperatingAndDecreseUnitsCharging(String alias){
        unitsCharging = unitsCharging - 1;
        unitsInOperation = unitsInOperation + 1;
        jLabelUnidadesEnRepostajeValue.setText(unitsCharging.toString());
        jLabelUnidadesEnOperacionValue.setText(unitsInOperation.toString());
        historial.append(LocalDateTime.now()).append(":_Desminador (Deminer): ")
                 .append(alias).append(", se ha puesto en modo operación").append("\n");
        jTextHistorialDeEventos.setText(historial.toString());
    }
    
    public void increaseSweeperUnitsOperatingAndDecreseUnitsCharging(String alias){
        unitsCharging = unitsCharging - 1;
        unitsInOperation = unitsInOperation + 1;
        jLabelUnidadesEnRepostajeValue.setText(unitsCharging.toString());
        jLabelUnidadesEnOperacionValue.setText(unitsInOperation.toString());
        historial.append(LocalDateTime.now()).append(":_Barredor (Sweeper): ")
                 .append(alias).append(", se ha puesto en modo operación").append("\n");
        jTextHistorialDeEventos.setText(historial.toString());
    }
    
    public void increaseWatchMecUnitsOperatingAndDecreseUnitsCharging(String alias){
        unitsCharging = unitsCharging - 1;
        unitsInOperation = unitsInOperation + 1;
        jLabelUnidadesEnRepostajeValue.setText(unitsCharging.toString());
        jLabelUnidadesEnOperacionValue.setText(unitsInOperation.toString());
        historial.append(LocalDateTime.now()).append(":_Vigilante (WatchMec): ")
                 .append(alias).append(", se ha puesto en modo operación").append("\n");
        jTextHistorialDeEventos.setText(historial.toString());
    }
    
    
    public void increaseTracerUnitsOperatingAndDecreseUnitsInRest(String alias){
        unitsInBase = unitsInBase - 1;
        unitsInOperation = unitsInOperation + 1;
        jLabelUnidadesEnRepostajeValue.setText(unitsCharging.toString());
        jLabelUnidadesEnOperacionValue.setText(unitsInOperation.toString());
        historial.append(LocalDateTime.now()).append(":_Detector (Tracer): ")
                 .append(alias).append(", se ha puesto en modo operación").append("\n");
        jTextHistorialDeEventos.setText(historial.toString());
    }
    
    public void increaseDeminerUnitsOperatingAndDecreseUnitsInRest(String alias){
        unitsInBase = unitsInBase - 1;
        unitsInOperation = unitsInOperation + 1;
        jLabelUnidadesEnRepostajeValue.setText(unitsCharging.toString());
        jLabelUnidadesEnOperacionValue.setText(unitsInOperation.toString());
        historial.append(LocalDateTime.now()).append(":_Desminador (Deminer): ")
                 .append(alias).append(", se ha puesto en modo operación").append("\n");
        jTextHistorialDeEventos.setText(historial.toString());
    }
    
    public void increaseSweeperUnitsOperatingAndDecreseUnitsInRest(String alias){
        unitsInBase = unitsInBase - 1;
        unitsInOperation = unitsInOperation + 1;
        jLabelUnidadesEnRepostajeValue.setText(unitsCharging.toString());
        jLabelUnidadesEnOperacionValue.setText(unitsInOperation.toString());
        historial.append(LocalDateTime.now()).append(":_Barredor (Sweeper): ")
                 .append(alias).append(", se ha puesto en modo operación").append("\n");
        jTextHistorialDeEventos.setText(historial.toString());
    }
    
    public void increaseWatchMecUnitsOperatingAndDecreseUnitsInRest(String alias){
        unitsInBase = unitsInBase - 1;
        unitsInOperation = unitsInOperation + 1;
        jLabelUnidadesEnRepostajeValue.setText(unitsCharging.toString());
        jLabelUnidadesEnOperacionValue.setText(unitsInOperation.toString());
        historial.append(LocalDateTime.now()).append(":_Vigilante (WatchMec): ")
                 .append(alias).append(", se ha puesto en modo operación").append("\n");
        jTextHistorialDeEventos.setText(historial.toString());
    }
    
    public void reportExplorationRouteCompleted(String alias, int explorationRoute){
        
        historial.append(LocalDateTime.now()).append(":_Detector (Tracer): ")
                 .append(alias).append(", ha completado la ruta de exploración ")
                 .append(explorationRoute).append("\n");
        jTextHistorialDeEventos.setText(historial.toString());
    }
    
    public void setUnitsInBase(Integer unitsInBase) {
        this.unitsInBase = unitsInBase;
        jLabelUnidadesEnBaseValue.setText(unitsInBase.toString());
    }

    public void setUnitsInOperation(Integer unitsInOperation) {
        this.unitsInOperation = unitsInOperation;
        jLabelUnidadesEnOperacionValue.setText(unitsInOperation.toString());
    }

    public void setUnitsCharging(Integer unitsCharging) {
        this.unitsCharging = unitsCharging;
        jLabelUnidadesEnRepostajeValue.setText(unitsCharging.toString());
    }
    
    public void setTracersQty(Integer tracersQty) {
        this.tracersQty = tracersQty;
        jLabelUnidadesDetectoresValue.setText(tracersQty.toString());
    }

    public void setDeminersQty(Integer deminersQty) {
        this.deminersQty = deminersQty;
        jLabelUnidadesDesminadoresValue.setText(deminersQty.toString());
    }

    public void setSweepersQty(Integer sweepersQty) {
        this.sweepersQty = sweepersQty;
        jLabelUnidadesBarredoressValue.setText(sweepersQty.toString());
    }

    public void setWatchMecsQty(Integer watchMecsQty) {
        this.watchMecsQty = watchMecsQty;
        jLabelUnidadesVigilantesValue.setText(watchMecsQty.toString());
    }
    
            
        
        
        
    
}
