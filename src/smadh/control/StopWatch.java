package smadh.control;


import java.math.BigDecimal;
import java.util.Timer;
import java.util.TimerTask;
import javax.swing.JLabel;
/**
 *
 * @author raxon
 */
public class StopWatch extends TimerTask {
   static BigDecimal counter = BigDecimal.ZERO;
   JLabel label;
   Timer timer;
    public void start(JLabel jLabelTiempoTrascurridoValue) {
        synchronized (this) {
            timer = new Timer("MyTimer");
            timer.scheduleAtFixedRate(this, 30, 1000);
            label = jLabelTiempoTrascurridoValue;
        }
    }

    public void stop() {
        timer.cancel();
        timer.purge();
    }

    public void pause() {
        
    }

    
    @Override
    public void run() {
                counter = counter.add(BigDecimal.ONE);
                int[] val = splitToComponentTimes(counter.plus());
                label.setText(createChronoString(val));
    }

    private static String createChronoString(int[] val){
        String secs =  val[2]<10?"0"+val[2]:""+val[2];
        String min =  val[1]<10?"0"+val[1]:""+val[1];
        String hour =  val[0]<10?"0"+val[0]:""+val[0];
        return hour+":"+min+":"+secs;
    }
    
    private static int[] splitToComponentTimes(BigDecimal biggy){
        long longVal = biggy.longValue();
        int hours = (int) longVal / 3600;
        int remainder = (int) longVal - hours * 3600;
        int mins = remainder / 60;
        remainder = remainder - mins * 60;
        int secs = remainder;

        int[] ints = {hours , mins , secs};
        return ints;
    }
}
