package smadh.control;


import BESA.Kernell.Agent.AgentBESA;

import BESA.Kernell.Agent.KernellAgentExceptionBESA;
import BESA.Kernell.Agent.StateBESA;
import BESA.Kernell.Agent.StructBESA;


/**
 *
 * @author raxon
 */
public class ControlPanelAgent extends AgentBESA{

    public ControlPanelAgent(String alias, StateBESA state, StructBESA structAgent, double passwd) throws KernellAgentExceptionBESA {
        super(alias, state, structAgent, passwd);
    }

    @Override
    public void setupAgent() {
    }

    @Override
    public void shutdownAgent() {
    }
    
}
