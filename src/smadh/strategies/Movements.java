package smadh.strategies;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import smadh.config.SmadhConstants;
import smadh.world.model.Slot;

/**
 *
 * @author raxon
 */
public class Movements {
    public static Map<String,Integer> simpleRandomMovement(int currentX,int currentY){
        Map<String,Integer> newPosition = new HashMap<>();
        newPosition.put("x", currentX);
        newPosition.put("y", currentY);
        Random rx = new Random();
        Random ry = new Random();
        int dirX = rx.nextInt(3);
        int dirY = ry.nextInt(3);
        if(dirX==2 && currentX+1<SmadhConstants.BOARD_WIDTH){
            newPosition.put("x", currentX+1);
        }
        if(dirX==1 && currentX-1>=0){
            newPosition.put("x", currentX-1);
        }
        if(dirY==2 && currentY+1<SmadhConstants.BOARD_HEIGTH){
            newPosition.put("y", currentY+1);
        }
        if(dirY==1 && currentY-1>=0){
            newPosition.put("y", currentY-1);
        }
        System.out.println(newPosition);
        return newPosition;
    }
    
    public static Slot lookForDeminerSuitableSlot(List<Slot> slots, int currentX,int currentY){
        
        return null;
    }
    
}
