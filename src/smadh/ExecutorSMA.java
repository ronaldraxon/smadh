package smadh;

import BESA.ExceptionBESA;
import BESA.Kernell.Agent.StructBESA;
import BESA.Kernell.System.AdmBESA;
import java.util.ArrayList;
import java.util.List;
import smadh.config.SmadhConstants;
import smadh.deminer.DeminerAgent;
import smadh.deminer.behavior.DeminerSensorGuard;
import smadh.deminer.state.DeminerState;
import smadh.control.ControlPanelAgent;
import smadh.control.ControlPanelUIBack;
import smadh.control.behavior.CommunicationDeminerGuard;
import smadh.control.behavior.CommunicationSweeperGuard;
import smadh.control.behavior.CommunicationTracerGuard;
import smadh.control.behavior.CommunicationWatchMecGuard;
import smadh.control.behavior.ReportDeminerGuard;
import smadh.control.behavior.ReportSweeperGuard;
import smadh.control.behavior.ReportTracerGuard;
import smadh.control.behavior.ReportWatchMecGuard;
import smadh.control.state.ControlPanelUIState;
import smadh.deminer.behavior.DeminerCooperativeActionGuard;
import smadh.sweeper.SweeperAgent;
import smadh.sweeper.behavior.SweeperCooperativeActionGuard;
import smadh.sweeper.behavior.SweeperSensorGuard;
import smadh.sweeper.state.SweeperState;
import smadh.tracer.TracerAgent;
import smadh.tracer.behavior.TracerCooperativeActionGuard;
import smadh.tracer.behavior.TracerSensorGuard;
import smadh.tracer.state.TracerState;
import smadh.watchmec.WatchMecAgent;
import smadh.watchmec.behavior.WatchMecCooperativeActionGuard;
import smadh.watchmec.behavior.WatchMecSensorGuard;
import smadh.watchmec.state.WatchMecState;
import smadh.world.WorldAgent;
import smadh.world.behavior.RunWorldGuard;
import smadh.world.behavior.deminerInteractions.UpdateDeminerGuard;
import smadh.world.behavior.sweeperInteractions.UpdateSweeperGuard;
import smadh.world.behavior.tracerInteractions.UpdateTracerGuard;
import smadh.world.behavior.watchmecInteractions.UpdateWatchMecGuard;
import smadh.world.behavior.deminerInteractions.SenseQueryDeminerGuard;
import smadh.world.behavior.sweeperInteractions.SenseQuerySweeperGuard;
import smadh.world.behavior.tracerInteractions.SenseQueryTracerGuard;
import smadh.world.behavior.watchmecInteractions.SenseQueryWatchMecGuard;
import smadh.world.state.WorldState;

/**
 *
 * @author raxon
 */
public class ExecutorSMA {

    public static int GAME_PERIODIC_TIME = 1000;
    public static int GAME_PERIODIC_DELAY_TIME = 100;
    public static final int DIMENSION_MAPA =20;
    public static final int PASS =74261;
    private static AdmBESA admLocal;
    private static WorldAgent wa;
    private static ControlPanelAgent cpa;
    private static List<TracerAgent> tracersPool= new ArrayList<>();
    private static List<DeminerAgent> deminersPool= new ArrayList<>();
    private static List<SweeperAgent> sweepersPool= new ArrayList<>();
    private static List<WatchMecAgent> watchMecsPool= new ArrayList<>();
    
    public static StructBESA createWorldStruct(){
        StructBESA worldStructure = new StructBESA();
        return worldStructure;
    }
    
    public static void createAdminBesa(){
        admLocal = AdmBESA.getInstance();
    }
    
    private static void loadLookAndFeel(){
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException | 
                 InstantiationException | 
                 IllegalAccessException | 
                 javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger
                     .getLogger(ControlPanelUIBack.class.getName())
                     .log(java.util.logging.Level.SEVERE, null, ex);
        }
    }
    
    public static void createTracers(WorldState worldState) throws ExceptionBESA{
        for(int i=1;i<=SmadhConstants.MAX_TRACERS_QTY;i++){
            TracerState state = new TracerState(20, i, 8);
            StructBESA struct = new StructBESA();
            struct.addBehavior("TracerBehavior"+i);
            struct.bindGuard("TracerBehavior"+i, TracerSensorGuard.class);
            TracerAgent tracer = new TracerAgent("T"+i, state, struct, PASS);
            tracersPool.add(tracer);
            worldState.getMap().addRoleTracer("T"+i, state);
            worldState.getTracersAlias().add("T"+i);
        }
    }
    
    public static void createCooperativeTracers(WorldState worldState) throws ExceptionBESA{
        for(int i=1;i<=SmadhConstants.MAX_TRACERS_QTY;i++){
            TracerState state = new TracerState(20, i, 8);
            StructBESA struct = new StructBESA();
            struct.addBehavior("TracerBehavior"+i);
            struct.bindGuard("TracerBehavior"+i, TracerCooperativeActionGuard.class);
            TracerAgent tracer = new TracerAgent("T"+i, state, struct, PASS);
            tracersPool.add(tracer);
            worldState.getMap().addRoleTracer("T"+i, state);
            worldState.getTracersAlias().add("T"+i);
        }
    }
    
    
    public static void createDeminers(WorldState worldState) throws ExceptionBESA{
        for(int i=1;i<=SmadhConstants.MAX_DEMINERS_QTY;i++){
            DeminerState state = new DeminerState(20,i,9);
            StructBESA struct = new StructBESA();
            struct.addBehavior("DeminerBehavior");
            struct.bindGuard("DeminerBehavior", DeminerSensorGuard.class);
            DeminerAgent deminer = new DeminerAgent("D"+i, state, struct, PASS);
            deminersPool.add(deminer);
            worldState.getMap().addRoleDeminer("D"+i,state);
            worldState.getDeminersAlias().add("D"+i);
        }
    }
    
    public static void createCooperativeDeminers(WorldState worldState) throws ExceptionBESA{
        for(int i=1;i<=SmadhConstants.MAX_DEMINERS_QTY;i++){
            DeminerState state = new DeminerState(20,i,9);
            StructBESA struct = new StructBESA();
            struct.addBehavior("DeminerBehavior");
            struct.bindGuard("DeminerBehavior", DeminerCooperativeActionGuard.class);
            DeminerAgent deminer = new DeminerAgent("D"+i, state, struct, PASS);
            deminersPool.add(deminer);
            worldState.getMap().addRoleDeminer("D"+i,state);
            worldState.getDeminersAlias().add("D"+i);
        }
    }
    
    public static void createSweepers(WorldState worldState) throws ExceptionBESA{
        for(int i=1;i<=SmadhConstants.MAX_SWEEPERS_QTY;i++){
            SweeperState state = new SweeperState(20,i,10);
            StructBESA struct = new StructBESA();
            struct.addBehavior("SweeperBehavior");
            struct.bindGuard("SweeperBehavior", SweeperSensorGuard.class);
            SweeperAgent sweeper = new SweeperAgent("S"+i, state, struct, PASS);
            sweepersPool.add(sweeper);
            worldState.getMap().addRoleSweeper("S"+i, state);
            worldState.getSweepersAlias().add("S"+i);
        }        
    }
    
        public static void createCooperativeSweepers(WorldState worldState) throws ExceptionBESA{
        for(int i=1;i<=SmadhConstants.MAX_SWEEPERS_QTY;i++){
            SweeperState state = new SweeperState(20,i,10);
            StructBESA struct = new StructBESA();
            struct.addBehavior("SweeperBehavior");
            struct.bindGuard("SweeperBehavior", SweeperCooperativeActionGuard.class);
            SweeperAgent sweeper = new SweeperAgent("S"+i, state, struct, PASS);
            sweepersPool.add(sweeper);
            worldState.getMap().addRoleSweeper("S"+i, state);
            worldState.getSweepersAlias().add("S"+i);
        }        
    }
    
    
    public static void createWatchMecs(WorldState worldState) throws ExceptionBESA{
        for(int i=1;i<=SmadhConstants.MAX_WATCHMECS_QTY;i++){
            WatchMecState state = new WatchMecState(20,i,11);
            StructBESA struct = new StructBESA();
            struct.addBehavior("WatchMecBehavior");
            struct.bindGuard("WatchMecBehavior", WatchMecSensorGuard.class);
            WatchMecAgent watchMec = new WatchMecAgent("W"+i, state, struct, PASS);
            watchMecsPool.add(watchMec);
            worldState.getMap().addRoleWatchmec("W"+i,state);
            worldState.getWatchMecsAlias().add("W"+i);
        }       
    }
    
    public static void createCooperativeWatchMecs(WorldState worldState) throws ExceptionBESA{
        for(int i=1;i<=SmadhConstants.MAX_WATCHMECS_QTY;i++){
            WatchMecState state = new WatchMecState(20,i,11);
            StructBESA struct = new StructBESA();
            struct.addBehavior("WatchMecBehavior");
            struct.bindGuard("WatchMecBehavior", WatchMecCooperativeActionGuard.class);
            WatchMecAgent watchMec = new WatchMecAgent("W"+i, state, struct, PASS);
            watchMecsPool.add(watchMec);
            worldState.getMap().addRoleWatchmec("W"+i,state);
            worldState.getWatchMecsAlias().add("W"+i);
        }       
    }
    
    public static void createworld(WorldState worldState) throws ExceptionBESA{
        StructBESA worldStruct = new StructBESA();
        worldStruct.addBehavior("WorldBehavior");
        worldStruct.bindGuard("WorldBehavior", RunWorldGuard.class);
        worldStruct.addBehavior("ChangeBehavior");        
        worldStruct.bindGuard("ChangeBehavior", UpdateWatchMecGuard.class);
        worldStruct.bindGuard("ChangeBehavior", UpdateSweeperGuard.class);
        worldStruct.bindGuard("ChangeBehavior", UpdateTracerGuard.class);
        worldStruct.bindGuard("ChangeBehavior", UpdateDeminerGuard.class);
        worldStruct.bindGuard("ChangeBehavior", SenseQueryTracerGuard.class);
        worldStruct.bindGuard("ChangeBehavior", SenseQueryDeminerGuard.class);
        worldStruct.bindGuard("ChangeBehavior", SenseQuerySweeperGuard.class);
        worldStruct.bindGuard("ChangeBehavior", SenseQueryWatchMecGuard.class);
        wa = new WorldAgent("WORLD", worldState, worldStruct, PASS);
    }
    
    public static void createPanelUI() throws ExceptionBESA{
        ControlPanelUIState cps = new ControlPanelUIState(admLocal);
        cps.setTracersPool(tracersPool);
        cps.setDeminersPool(deminersPool);
        cps.setSweepersPool(sweepersPool);
        cps.setWatchMecsPool(watchMecsPool);
        cps.setTracersQty(SmadhConstants.MAX_TRACERS_QTY);
        cps.setDeminersQty(SmadhConstants.MAX_DEMINERS_QTY);
        cps.setSweepersQty(SmadhConstants.MAX_SWEEPERS_QTY);
        cps.setWatchMecsQty(SmadhConstants.MAX_WATCHMECS_QTY);
        cps.setUnitsInOperation(0);
        cps.setUnitsInBase(SmadhConstants.MAX_TRACERS_QTY +
                           SmadhConstants.MAX_DEMINERS_QTY +
                           SmadhConstants.MAX_SWEEPERS_QTY +
                           SmadhConstants.MAX_WATCHMECS_QTY);
        StructBESA cpStruct = new StructBESA();
        //cpStruct.addBehavior("BotsBehavior"); 
        //cpStruct.bindGuard("BotsBehavior", RunWorldGuard.class);     
        cpStruct.addBehavior("TracerReport");
        cpStruct.bindGuard("TracerReport", ReportTracerGuard.class);
        cpStruct.addBehavior("DeminerReport");
        cpStruct.bindGuard("DeminerReport", ReportDeminerGuard.class);
        cpStruct.addBehavior("SweeperReport");
        cpStruct.bindGuard("SweeperReport", ReportSweeperGuard.class);
        cpStruct.addBehavior("WatchMecReport");
        cpStruct.bindGuard("WatchMecReport", ReportWatchMecGuard.class);
        if(SmadhConstants.ENABLE_COOPERATION){
            cpStruct.bindGuard("TracerReport", CommunicationTracerGuard.class);
            cpStruct.bindGuard("DeminerReport", CommunicationDeminerGuard.class);
            cpStruct.bindGuard("SweeperReport", CommunicationSweeperGuard.class);
            cpStruct.bindGuard("WatchMecReport", CommunicationWatchMecGuard.class);
        }
        cpa = new ControlPanelAgent("CONTROL",cps,cpStruct,PASS);
    }
    
    public static void startWorld(){
        wa.start();
    }
    
    public static void startTracers(){
        tracersPool.stream().forEach((tracersPool1) -> {
            tracersPool1.start();
        });
    }
    
    public static void startDeminers(){
        deminersPool.stream().forEach((deminersPool1) -> {
            deminersPool1.start();
        });
    }
    
    public static void startSweepers(){
        sweepersPool.stream().forEach((sweepersPool1) -> {
            sweepersPool1.start();
        });
    }
    
    public static void startWatchMecs(){
        watchMecsPool.stream().forEach((watchMecsPool1) -> {
            watchMecsPool1.start();
        });
    }
    
    public static void startControlPanel(){
        cpa.start();
    }
    
    public static void main(String args[])throws ExceptionBESA {
        loadLookAndFeel();
        createAdminBesa();
        WorldState worldState = new WorldState();
        if(SmadhConstants.ENABLE_COOPERATION){
            createCooperativeTracers(worldState);
            createCooperativeDeminers(worldState);
            createCooperativeSweepers(worldState);
            createCooperativeWatchMecs(worldState);
            createworld(worldState);
            createPanelUI();
            startWorld();
            startTracers();
            startDeminers();
            startSweepers();
            startWatchMecs();
            startControlPanel();
        }else{
            createTracers(worldState);
            createDeminers(worldState);
            createSweepers(worldState);
            createWatchMecs(worldState);
            createworld(worldState);
            createPanelUI();
            startWorld();
            startTracers();
            startDeminers();
            startSweepers();
            startWatchMecs();
            startControlPanel();
        }
        
    }
    

}