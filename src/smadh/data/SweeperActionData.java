package smadh.data;

import BESA.Kernell.Agent.Event.DataBESA;
import java.util.List;
import smadh.world.model.Slot;

/**
 *
 * @author Raxon
 */
public class SweeperActionData extends DataBESA {
    
    String alias;
    String action;
    boolean isMarkedTarget =false;
    int entityType;
    int x;
    int y;
    List<Slot> slots;
    String senseType;

    public SweeperActionData(String action) {
        this.action = action;
    }
    
    public SweeperActionData(String alias, String action) {
        this.alias = alias;
        this.action = action;
    }

    public SweeperActionData(int x, int y,String action) {
        this.x = x;
        this.y = y;
        this.action=action;
    }
    
    public SweeperActionData(int x, int y,String action,String senseType) {
        this.x = x;
        this.y = y;
        this.action=action;
        this.senseType = senseType;
    }
    
    public SweeperActionData(String alias,int x, int y,String action,String senseType) {
        this.alias = alias;
        this.x = x;
        this.y = y;
        this.action=action;
        this.senseType = senseType;
    }
    
    
    public SweeperActionData(String alias, String action, int x, int y) {
        this.alias = alias;
        this.action = action;
        this.x = x;
        this.y = y;
    }
    
    public SweeperActionData(String alias, String action, int x, int y, String senseType) {
        this.alias = alias;
        this.action = action;
        this.x = x;
        this.y = y;
        this.senseType = senseType;
    }

      public SweeperActionData(String alias, String action, int entityType) {
        this.alias = alias;
        this.action = action;
        this.entityType = entityType;
    }

    public SweeperActionData(String alias, String action, int x, int y, int entityType) {
        this.alias = alias;
        this.action = action;
        this.x = x;
        this.y = y;
        this.entityType = entityType;
    }
    
    public SweeperActionData(List<Slot> slots, String action) {
        this.slots=slots;
        this.action = action;
    }
    
    public SweeperActionData(int x, int y, String action,int entityType,boolean isMarkedTarget) {
        this.action = action;
        this.x = x;
        this.y = y;
        this.entityType = entityType;
        this.isMarkedTarget=isMarkedTarget;
    }
    
    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }
    
    public int getEntityType() {
        return entityType;
    }

    public void setEntityType(int entityType) {
        this.entityType = entityType;
    }
    
    public String getSenseType() {
        return senseType;
    }

    public void setSenseType(String senseType) {
        this.senseType = senseType;
    }
    
    public boolean isMarkedTarget() {
        return isMarkedTarget;
    }

    public void setIsMarkedTarget(boolean isMarkedTarget) {
        this.isMarkedTarget = isMarkedTarget;
    }
    
    public List<Slot> getSlots() {
        return slots;
    }

    public void setSlots(List<Slot> slots) {
        this.slots = slots;
    }
}
