package smadh.data;

import BESA.Kernell.Agent.Event.DataBESA;
import java.util.ArrayList;
import java.util.List;
import smadh.world.model.PersonEntity;

/**
 *
 * @author Raxon
 */
public class SensorPersonData extends DataBESA{
    private List<PersonEntity> person;

    public SensorPersonData() {
        person = new ArrayList<>();
    }

    public SensorPersonData(List<PersonEntity> person) {
        this.person = person;
    }

    public List<PersonEntity> getPerson() {
        return person;
    }

    public void setPerson(List<PersonEntity> person) {
        this.person = person;
    }
    
}
