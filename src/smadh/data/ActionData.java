package smadh.data;

import BESA.Kernell.Agent.Event.DataBESA;

/**
 *
 * @author Raxon
 */
public class ActionData extends DataBESA {
    
    String alias;
    String action;
    int actionForce;
    int energy;
    int entityType;
    int x;
    int y;

    public ActionData(String alias, String action) {
        this.alias = alias;
        this.action = action;
    }

     public ActionData(String alias, int energy,String action) {
        this.alias = alias;
        this.action = action;
        this.energy = energy;
    }
    
    public ActionData(String alias, String action, int x, int y) {
        this.alias = alias;
        this.action = action;
        this.x = x;
        this.y = y;
    }

    public ActionData(String alias, String action, int x, int y,int entityType) {
        this.entityType = entityType;
        this.alias = alias;
        this.action = action;
        this.x = x;
        this.y = y;
    }
    
    public ActionData(String alias, String action, int entityType) {
        this.alias = alias;
        this.action = action;
        this.entityType = entityType;
    }

    public ActionData(String alias, String action, int x, int y, int entityType,int actionForce) {
        this.alias = alias;
        this.action = action;
        this.x = x;
        this.y = y;
        this.entityType = entityType;
        this.actionForce = actionForce;
    }
    
    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }
    
    public int getEntityType() {
        return entityType;
    }

    public void setEntityType(int entityType) {
        this.entityType = entityType;
    }
    
    public int getEnergy() {
        return energy;
    }
    
    public void setEnergy(int energy) {
        this.energy = energy;
    }
   
}
