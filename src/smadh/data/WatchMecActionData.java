package smadh.data;

import BESA.Kernell.Agent.Event.DataBESA;
import java.util.List;
import smadh.control.ExplorationRoute;
import smadh.world.model.Slot;

/**
 *
 * @author Raxon
 */
public class WatchMecActionData extends DataBESA {
    
    String alias;
    String action;
    ExplorationRoute explorationRoute;
    boolean isMarkedTarget =false;
    int entityType;
    int x;
    int y;
    List<Slot> slots;
    double senseSize;
    String senseType;
    boolean allowedToMove;
    boolean needsValidation;

    public WatchMecActionData(String action) {
        this.action = action;
    }
    
    public WatchMecActionData(List<Slot> slots, String action) {
        this.slots = slots;
        this.action = action;
    }
    
    public WatchMecActionData(String action, ExplorationRoute explorationRoute) {
        this.action = action;
        this.explorationRoute = explorationRoute;
    }
    
    public WatchMecActionData(String alias, String action) {
        this.alias = alias;
        this.action = action;
    }

    public WatchMecActionData(int x, int y,String action) {
        this.x = x;
        this.y = y;
        this.action=action;
    }
    
    public WatchMecActionData(String action,boolean allowedToMove) {
        this.action=action;
        this.allowedToMove = allowedToMove;
    }
    
    
    public WatchMecActionData(int x, int y,String action,boolean allowedToMove) {
        this.x = x;
        this.y = y;
        this.action=action;
        this.allowedToMove = allowedToMove;
    }
    
    public WatchMecActionData(String alias, String action, int x, int y) {
        this.alias = alias;
        this.action = action;
        this.x = x;
        this.y = y;
    }
    
    public WatchMecActionData(String alias, String action, int x, int y,double senseSize, String senseType) {
        this.alias = alias;
        this.action = action;
        this.x = x;
        this.y = y;
        this.senseSize=senseSize;
        this.senseType = senseType;
    }

      public WatchMecActionData(String alias, String action, int entityType) {
        this.alias = alias;
        this.action = action;
        this.entityType = entityType;
    }

    public WatchMecActionData(String alias, String action, int x, int y, int entityType) {
        this.alias = alias;
        this.action = action;
        this.x = x;
        this.y = y;
        this.entityType = entityType;
    }
    
    public WatchMecActionData(int x, int y, String action,int entityType,boolean isMarkedTarget) {
        this.action = action;
        this.x = x;
        this.y = y;
        this.entityType = entityType;
        this.isMarkedTarget=isMarkedTarget;
    }
    
    public ExplorationRoute getExplorationRoute() {
        return explorationRoute;
    }

    public void setExplorationRoute(ExplorationRoute explorationRoute) {
        this.explorationRoute = explorationRoute;
    }
    
    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }
    
    public int getEntityType() {
        return entityType;
    }

    public void setEntityType(int entityType) {
        this.entityType = entityType;
    }
    
    public double getSenseSize() {
        return senseSize;
    }

    public void setSenseSize(double senseSize) {
        this.senseSize = senseSize;
    }
    
    public String getSenseType() {
        return senseType;
    }

    public void setSenseType(String senseType) {
        this.senseType = senseType;
    }
    
    public boolean isMarkedTarget() {
        return isMarkedTarget;
    }

    public void setIsMarkedTarget(boolean isMarkedTarget) {
        this.isMarkedTarget = isMarkedTarget;
    }
    
    public List<Slot> getSlots() {
        return slots;
    }

    public void setSlots(List<Slot> slots) {
        this.slots = slots;
    }
    
    public boolean isAllowedToMove() {
        return allowedToMove;
    }

    public void setAllowedToMove(boolean allowedToMove) {
        this.allowedToMove = allowedToMove;
    }
}
