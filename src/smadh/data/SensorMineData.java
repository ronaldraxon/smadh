package smadh.data;

import BESA.Kernell.Agent.Event.DataBESA;
import java.util.ArrayList;
import java.util.List;
import smadh.world.model.MineEntity;

/**
 *
 * @author Raxon
 */
public class SensorMineData extends DataBESA{
    private List<MineEntity> mine;

    public SensorMineData() {
        mine = new ArrayList<>();
    }

    public SensorMineData(List<MineEntity> mine) {
        this.mine = mine;
    }

    public List<MineEntity> getMine() {
        return mine;
    }

    public void setMine(List<MineEntity> mine) {
        this.mine = mine;
    }
    
}
