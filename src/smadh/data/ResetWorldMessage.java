package smadh.data;

import BESA.Kernell.Agent.Event.DataBESA;
/**
 *
 * @author raxon
 */
public class ResetWorldMessage extends DataBESA {
    String alias;
    String action;
    int size;
    int numTipe1Mines;

    public ResetWorldMessage(int size, int numDust,String action) {
        this.action = action;
        this.size = size;
        this.numTipe1Mines = numDust;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public int getNumDust() {
        return numTipe1Mines;
    }

    public void setNumDust(int numDust) {
        this.numTipe1Mines = numDust;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

}
