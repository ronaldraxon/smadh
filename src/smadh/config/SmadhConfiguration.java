
package smadh.config;

import java.util.Properties;

/**
 *
 * @author raxon
 */
public final class SmadhConfiguration {

private Properties properties = null;
private static SmadhConfiguration instance = null;

/** Private constructor */
private SmadhConfiguration (){
    this.properties = new Properties();
    try{
        properties.load(Thread.currentThread().getContextClassLoader().getResourceAsStream(SmadhConstants.PROPFILENAME));
    }catch(Exception ex){
        ex.printStackTrace();
    }
}   

/** Creates the instance is synchronized to avoid multithreads problems */
private synchronized static void createInstance () {
    if (instance == null) { 
        instance = new SmadhConfiguration ();
    }
}

/** Get the properties instance. Uses singleton pattern */
public static SmadhConfiguration getInstance(){
    // Uses singleton pattern to guarantee the creation of only one instance
    if(instance == null) {
        createInstance();
    }
    return instance;
}

/** Get a property of the property file */
public String getStringProperty(String key){
    String result = null;
    if(key !=null && !key.trim().isEmpty()){
        result = this.properties.getProperty(key);
    }
    return result;
}

public int getIntegerProperty(String key){
    int result = 0;
    if(key !=null && !key.trim().isEmpty()){
        result = Integer.parseInt(this.properties.getProperty(key));
    }
    return result;
}

public boolean getBooleanProperty(String key){
    boolean result = false;
    if(key !=null && !key.trim().isEmpty()){
        result = Boolean.parseBoolean(this.properties.getProperty(key));
    }
    return result;
}

/** Override the clone method to ensure the "unique instance" requeriment of this class */
public Object clone() throws CloneNotSupportedException {
    throw new CloneNotSupportedException();
}}
