
package smadh.config;

import smadh.config.SmadhConfiguration;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 *
 * @author raxon
 */
public class SmadhConstants {
    public static final String PROPFILENAME = "resources/config.properties";
    public static final int JAFRAME_HEIGTH = 628;
    public static final int JAFRAME_WIDTH = 1055;
    public static final int SQUARE_SIZE = 30;
    public static final int BOARD_HEIGTH = 20;
    public static final int BOARD_WIDTH = 35;
    public static final int PERIODIC_TIME = SmadhConfiguration.getInstance().getIntegerProperty("PERIODIC_TIME");
    public static final int PERIODIC_DELAY_TIME = SmadhConfiguration.getInstance().getIntegerProperty("PERIODIC_DELAY_TIME");
    public static final int MAP_DIMENSIONS = SmadhConfiguration.getInstance().getIntegerProperty("MAP_DIMENSIONS");
    public static final int MAX_EXCERCISE_ITERATIONS = SmadhConfiguration.getInstance().getIntegerProperty("MAX_EXCERCISE_ITERATIONS");
    public static final int BESA_PASS = SmadhConfiguration.getInstance().getIntegerProperty("BESA_PASS");
    public static final boolean ENABLE_COOPERATION = SmadhConfiguration.getInstance().getBooleanProperty("ENABLE_COOPERATION");
    
    
    public static final int COLLISION_PENALTY = SmadhConfiguration.getInstance().getIntegerProperty("COLLISION_PENALTY");
    public static final int BOTS_REFUEL_UNITS_PER_PT = SmadhConfiguration.getInstance().getIntegerProperty("BOTS_REFUEL_UNITS_PER_PT");
    public static final int BOTS_SWEEPERS_FUEL_MAX_CAPACITY = SmadhConfiguration.getInstance().getIntegerProperty("BOTS_SWEEPERS_FUEL_MAX_CAPACITY");
    public static final int BOTS_DEMINERS_FUEL_MAX_CAPACITY = SmadhConfiguration.getInstance().getIntegerProperty("BOTS_DEMINERS_FUEL_MAX_CAPACITY");
    public static final int BOTS_TRACERS_FUEL_MAX_CAPACITY = SmadhConfiguration.getInstance().getIntegerProperty("BOTS_TRACERS_FUEL_MAX_CAPACITY");
    public static final int BOTS_WATCHMECS_FUEL_MAX_CAPACITY = SmadhConfiguration.getInstance().getIntegerProperty("BOTS_WATCHMECS_FUEL_MAX_CAPACITY");
    public static final int BOTS_SWEEPERS_FUEL_CONSUMPTION_PER_PT = SmadhConfiguration.getInstance().getIntegerProperty("BOTS_SWEEPERS_FUEL_CONSUMPTION_PER_PT");
    public static final int BOTS_DEMINERS_FUEL_CONSUMPTION_PER_PT = SmadhConfiguration.getInstance().getIntegerProperty("BOTS_DEMINERS_FUEL_CONSUMPTION_PER_PT");
    public static final int BOTS_TRACERS_FUEL_CONSUMPTION_PER_PT = SmadhConfiguration.getInstance().getIntegerProperty("BOTS_TRACERS_FUEL_CONSUMPTION_PER_PT");
    public static final int BOTS_WATCHMECS_FUEL_CONSUMPTION_PER_PT = SmadhConfiguration.getInstance().getIntegerProperty("BOTS_WATCHMECS_FUEL_CONSUMPTION_PER_PT");
    
    public static final int BOTS_SWEEPERS_ACTION_FORCE = SmadhConfiguration.getInstance().getIntegerProperty("BOTS_SWEEPERS_ACTION_FORCE");
    public static final int BOTS_DEMINERS_ACTION_FORCE = SmadhConfiguration.getInstance().getIntegerProperty("BOTS_DEMINERS_ACTION_FORCE");
    public static final int BOTS_TRACERS_ACTION_FORCE = SmadhConfiguration.getInstance().getIntegerProperty("BOTS_TRACERS_ACTION_FORCE");
    public static final int BOTS_WATCHMECS_ACTION_FORCE = SmadhConfiguration.getInstance().getIntegerProperty("BOTS_WATCHMECS_ACTION_FORCE");
    
    public static final int BOTS_TRACERS_MAX_EXPLORATION_SCORE = SmadhConfiguration.getInstance().getIntegerProperty("BOTS_TRACERS_MAX_EXPLORATION_SCORE");
    public static final int BOTS_DEMINERS_MAX_EXPLORATION_SCORE = SmadhConfiguration.getInstance().getIntegerProperty("BOTS_DEMINERS_MAX_EXPLORATION_SCORE");
    public static final int BOTS_SWEEPERS_MAX_EXPLORATION_SCORE = SmadhConfiguration.getInstance().getIntegerProperty("BOTS_SWEEPERS_MAX_EXPLORATION_SCORE");
    public static final int BOTS_WATCHMECS_MAX_EXPLORATION_SCORE = SmadhConfiguration.getInstance().getIntegerProperty("BOTS_WATCHMECS_MAX_EXPLORATION_SCORE");
    
    public static final int MAX_SWEEPERS_QTY = SmadhConfiguration.getInstance().getIntegerProperty("MAX_SWEEPERS_QTY");
    public static final int MAX_DEMINERS_QTY = SmadhConfiguration.getInstance().getIntegerProperty("MAX_DEMINERS_QTY");
    public static final int MAX_TRACERS_QTY = SmadhConfiguration.getInstance().getIntegerProperty("MAX_TRACERS_QTY");
    public static final int MAX_WATCHMECS_QTY = SmadhConfiguration.getInstance().getIntegerProperty("MAX_WATCHMECS_QTY");
    
    public static final int MAX_PERSONS_IN_FIELD_QTY = SmadhConfiguration.getInstance().getIntegerProperty("MAX_PERSONS_IN_FIELD_QTY");
    public static final int PERSON_IN_FIELD_PERSISTANCE = SmadhConfiguration.getInstance().getIntegerProperty("PERSON_IN_FIELD_PERSISTANCE");
    public static final int TYPE1_LANDMINES_QTY = SmadhConfiguration.getInstance().getIntegerProperty("TYPE1_LANDMINES_QTY");     
    public static final int TYPE1_LANDMINES_PERSISTANCE = SmadhConfiguration.getInstance().getIntegerProperty("TYPE1_LANDMINES_PERSISTANCE");
    public static final int TYPE2_LANDMINES_QTY = SmadhConfiguration.getInstance().getIntegerProperty("TYPE2_LANDMINES_QTY");
    public static final int TYPE2_LANDMINES_PERSISTANCE = SmadhConfiguration.getInstance().getIntegerProperty("TYPE2_LANDMINES_PERSISTANCE");
    
    public static final int MAX_TIMES_WAITING_FOR_JOB = SmadhConfiguration.getInstance().getIntegerProperty("MAX_TIMES_WAITING_FOR_JOB"); 
    
    public static final double TRACER_DETECTION_RADIUS = 2.42;
    public static final double DEMINER_DETECTION_RADIUS = 1.42;
    public static final double WATCHMEC_DETECTION_RADIUS = 9;
    
    public static final String STATUS_READY = "READY";
    public static final String STATUS_REST = "REST";
    public static final String STATUS_REQUESTING_ROUTE = "REQUESTING ROUTE";
    public static final String STATUS_REQUESTING_DEMINE_JOB = "REQUEST DEMINE JOB";
    public static final String STATUS_ASSIGNED_DEMINE_JOB = "ASSIGNED DEMINE JOB";
    public static final String STATUS_RETURNING_TO_BASE = "RETURNING TO BASE";
    public static final String STATUS_TARGET_AQUIRED = "TARGET_AQUIRED";
    public static final String STATUS_CLEARING_PERSONS = "CLEARING PERSONS";
    public static final String STATUS_DEMINING = "DEMINING";
    public static final String STATUS_DETECTING = "DETECTING";
    public static final String STATUS_MOVING = "MOVING";
    public static final String STATUS_CHARGING = "CHARGING";
    
    
    public static final String ALLOCATION_FIELD = "FIELD";
    public static final String ALLOCATION_BASE = "BASE";
    
    public static final String INHABITED_AGENT_DEMINER = "DEMINER";
    public static final String INHABITED_AGENT_SWEEPER = "SWEEPER";
    public static final String INHABITED_AGENT_OCCUPIED_WATCHMEC = "OCCUPIED WATCHMEC";
    public static final String INHABITED_MINET1 = "MINET1";
    public static final String INHABITED_MINET2 = "MINET2";
    public static final String INHABITED_PERSON = "PERSON";
    public static final String INHABITED_NONE = "NONE";
    
    
    
}
