 package smadh.deminer;


import BESA.Kernell.Agent.AgentBESA;
import BESA.Kernell.Agent.KernellAgentExceptionBESA;
import BESA.Kernell.Agent.StateBESA;
import BESA.Kernell.Agent.StructBESA;
import BESA.Log.ReportBESA;
import smadh.deminer.state.DeminerState;


/**
 *
 * @author Raxon
 */
public class DeminerAgent extends AgentBESA {
  
    public DeminerAgent(String alias, StateBESA state, 
                        StructBESA structAgent, double passwd)
                        throws KernellAgentExceptionBESA {
        super(alias, state, structAgent, passwd);                                    
    }
    
    @Override
    public void setupAgent() {
        ReportBESA.info("SETUP AGENT -> " + getAlias());
        DeminerState cs = (DeminerState)this.getState();
        /*DataBESA data = new SubscribeData(this.getAlias(), this.x, this.y);
        EventBESA event = new EventBESA(SubscribeDeminerGuard.class.getName(), data);//En lugar de inscribirse al mundo debe inscribirse al lider
        AgHandlerBESA ah;
        
        /*try {
            ah = this.getAdmLocal().getHandlerByAlias("WORLD");//aquí se debe cambiar el destino
            ah.sendEvent(event);
        } catch (ExceptionBESA e) {
            ReportBESA.error(e);
        }*/
    }

    @Override
    public void shutdownAgent() {
        ReportBESA.info("SHUTDOWN AGENT -> " + getAlias());
    }
    
}
