/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package smadh.deminer.state;

import BESA.Kernell.Agent.StateBESA;
import java.util.HashMap;
import java.util.Map;
import javafx.util.Pair;
import smadh.config.SmadhConstants;

/**
 *
 * @author Raxon
 */
public class DeminerState extends  StateBESA{
    
    private Map<String,Pair<Integer,Integer>> droneMap;
    private String roleName;
    private String status;
    private int fuel;
    private int actionForce;
    private int fuelConsumptionPerPeriod;
    private int sizeMap;
    private int initialx;
    private int initialy;    
    private int x;
    private int y;
    private String targetAlias;
    private int targetEntityType;
    private int targetx;
    private int targety;
    private int assignedTargetx;
    private int assignedTargety;
    private int assignedTargetEntityType;
    private int previousX;
    private int previousY;
    private boolean targetAssigned;
    private int timesWaitingForJob;
    private boolean onRest;




    public DeminerState(int sizeMap,int x, int y) {
        this.sizeMap = sizeMap;
        this.roleName = "Deminer";
        this.initialx= x;
        this.initialy= y;   
        this.x = x;
        this.y = y;
        this.previousX = x;
        this.previousY = y;
        this.droneMap = new HashMap<>();
        setPointToDroneMap();
        this.status = SmadhConstants.STATUS_READY;
        this.fuel = SmadhConstants.BOTS_DEMINERS_FUEL_MAX_CAPACITY;
        this.actionForce = SmadhConstants.BOTS_DEMINERS_ACTION_FORCE;
        this.fuelConsumptionPerPeriod = SmadhConstants.BOTS_DEMINERS_FUEL_CONSUMPTION_PER_PT;  
    }
    
   public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
    
    public int getFuel() {
        return fuel;
    }

    public void setFuel(int fuel) {
        this.fuel = fuel;
    }

    public int getActionForce() {
        return actionForce;
    }

    public void setActionForce(int actionForce) {
        this.actionForce = actionForce;
    }

    public int getFuelConsumptionPerPeriod() {
        return fuelConsumptionPerPeriod;
    }

    public void setFuelConsumptionPerPeriod(int fuelConsumptionPerPeriod) {
        this.fuelConsumptionPerPeriod = fuelConsumptionPerPeriod;
    }
    
    public int getSizeMap() {
        return sizeMap;
    }

    public void setSizeMap(int sizeMap) {
        this.sizeMap = sizeMap;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }
    
    public int getInitialx() {
        return initialx;
    }

    public void setInitialx(int initialx) {
        this.initialx = initialx;
    }

    public int getInitialy() {
        return initialy;
    }

    public void setInitialy(int initialy) {
        this.initialy = initialy;
    }
    
    public int getPreviousX() {
        return previousX;
    }

    public void setPreviousX(int previousX) {
        this.previousX = previousX;
    }

    public int getPreviousY() {
        return previousY;
    }

    public void setPreviousY(int previousY) {
        this.previousY = previousY;
    }
    
    public Map<String, Pair<Integer, Integer>> getDroneMap() {
        return droneMap;
    }

    public void setPointToDroneMap() {
        Pair<Integer,Integer> pair =new Pair(this.x,this.y);
        this.droneMap.put(this.x+";"+this.y, pair);
    }
    
        public int getTargetx() {
        return targetx;
    }

    public void setTargetx(int targetx) {
        this.targetx = targetx;
    }

    public int getTargety() {
        return targety;
    }

    public void setTargety(int targety) {
        this.targety = targety;
    }
    
    public String getTargetAlias() {
        return targetAlias;
    }

    public void setTargetAlias(String targetAlias) {
        this.targetAlias = targetAlias;
    }
    
    public int getTargetEntityType() {
        return targetEntityType;
    }

    public void setTargetEntityType(int targetEntityType) {
        this.targetEntityType = targetEntityType;
    }
    
    public boolean isTargetAssigned() {
        return targetAssigned;
    }

    public void setTargetAssigned(boolean targetAssigned) {
        this.targetAssigned = targetAssigned;
    }
    
    public int getAssignedTargetx() {
        return assignedTargetx;
    }

    public void setAssignedTargetx(int assignedTargetx) {
        this.assignedTargetx = assignedTargetx;
    }

    public int getAssignedTargety() {
        return assignedTargety;
    }

    public void setAssignedTargety(int assignedTargety) {
        this.assignedTargety = assignedTargety;
    }
    
    public int getAssignedTargetEntityType() {
        return assignedTargetEntityType;
    }

    public void setAssignedTargetEntityType(int assignedTargetEntityType) {
        this.assignedTargetEntityType = assignedTargetEntityType;
    }
    
    public int getTimesWaitingForJob() {
        return timesWaitingForJob;
    }

    public void setTimesWaitingForJob(int timesWaitingForJob) {
        this.timesWaitingForJob = timesWaitingForJob;
    }

    public boolean isOnRest() {
        return onRest;
    }

    public void setOnRest(boolean onRest) {
        this.onRest = onRest;
    }
    
}
