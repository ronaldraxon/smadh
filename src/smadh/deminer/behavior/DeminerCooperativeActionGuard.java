package smadh.deminer.behavior;

import BESA.ExceptionBESA;
import BESA.Kernell.Agent.Event.DataBESA;
import BESA.Kernell.Agent.Event.EventBESA;
import BESA.Kernell.Agent.GuardBESA;
import BESA.Kernell.Agent.StateBESA;
import BESA.Kernell.System.Directory.AgHandlerBESA;
import BESA.Log.ReportBESA;
import java.util.List;
import java.util.Objects;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javafx.util.Pair;
import smadh.config.SmadhConstants;
import smadh.control.behavior.CommunicationDeminerGuard;
import smadh.control.behavior.ReportDeminerGuard;
import smadh.deminer.state.DeminerState;
import smadh.data.ActionData;
import smadh.data.DeminerActionData;
import smadh.utils.Calculations;
import smadh.world.behavior.deminerInteractions.SenseQueryDeminerGuard;
import smadh.world.behavior.deminerInteractions.UpdateDeminerGuard;
import smadh.world.model.Slot;

/**
 *
 * @author Raxon
 */
public class DeminerCooperativeActionGuard extends GuardBESA{

    @Override
    public boolean funcEvalBool(StateBESA objEvalBool) {
        return true;
    }
    
    @Override
    public void funcExecGuard(EventBESA ebesa) {
        DeminerActionData data = (DeminerActionData) ebesa.getData();
        DeminerState state = (DeminerState) this.getAgent().getState();
        switch (data.getAction()) {
            case "enable":
                changeToReadyStatus(state);
            break;    
            case "search":
                if(isStatusDifferentThanRest(state)){
                    if (isEnergyUnder1Point(state)|| isCharging(state)){
                        reChargeDrone(state);
                    }else if(isJobFinished(state)){
                        returnToBase(state);
                    }else{
                        doDemineJob(state);
                    }
                }
            break;
            case "waitAndAskLater":
            {
              try {
                 waitACoupleSecondsAndAskAgainForJob(state);
               } catch (InterruptedException ex) {
                 Logger.getLogger(DeminerCooperativeActionGuard.class.getName()).log(Level.SEVERE, null, ex);
            }
            }
            break;
            case "assingDemineJob":
                assignDemineJob(state,data);
            break;
            case "evaluate":
                if(isStatusDifferentThanRest(state)){
                    if (isEnergyUnder1Point(state)|| isCharging(state)){
                        reChargeDrone(state);
                    }else if(isJobFinished(state)){
                        returnToBase(state);
                    }else{
                        evaluateAndProceed(data,state);
                    }
                }
            break;
            case "moveAllowed":
                moveDroneAndReport(state);
            break;
            case "waitToMoveLater":
                {
                    try {
                        waitACoupleSecondsAndTryToPresetSlot(state);
                    } catch (InterruptedException ex) {
                        Logger.getLogger(DeminerCooperativeActionGuard.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            break;
            case "demine":
                if(isStatusDifferentThanRest(state)){
                    if (isEnergyUnder1Point(state)|| isCharging(state)){
                        reChargeDrone(state);
                    }else if(isJobFinished(state)){
                        returnToBase(state);
                    }else{
                        startDemining(state);
                    }
                }
            break; 
            case "cleared":
                if(isStatusDifferentThanRest(state)){
                    if (isEnergyUnder1Point(state)|| isCharging(state)){
                        reChargeDrone(state);
                    }else if(isJobFinished(state)){
                        returnToBase(state);
                    }else{
                        beReadyForNextSearch(data,state);
                    }
                }
            break;
        }
    }
    
    private boolean isStatusDifferentThanRest(DeminerState state){
        return !state.getStatus().equals(SmadhConstants.STATUS_REST);
    }
    
    private boolean isJobFinished(DeminerState state){
        return state.getDroneMap().size()>SmadhConstants.BOTS_DEMINERS_MAX_EXPLORATION_SCORE || 
               state.getTimesWaitingForJob()>SmadhConstants.MAX_TIMES_WAITING_FOR_JOB;
    }
    
    private boolean isEnergyUnder1Point(DeminerState state){
        return state.getFuel()<1;
    }
    
    private boolean isCharging(DeminerState state){
        return state.getStatus().equals(SmadhConstants.STATUS_CHARGING);
    }
    
    private void reChargeDrone(DeminerState state){
        DataBESA dataAction=new ActionData(this.getAgent().getAlias(), "");
        if(!state.getStatus().equals(SmadhConstants.STATUS_CHARGING)){
            state.setStatus(SmadhConstants.STATUS_CHARGING);
            dataAction = new ActionData(this.getAgent().getAlias(), "charging");
        }
        state.setFuel(state.getFuel()+SmadhConstants.BOTS_REFUEL_UNITS_PER_PT);
        if(state.getFuel()>=SmadhConstants.BOTS_DEMINERS_FUEL_MAX_CAPACITY){
            state.setStatus(SmadhConstants.STATUS_READY);
            dataAction = new ActionData(this.getAgent().getAlias(), "operating");
        }
        reportAction(dataAction);
    }
    
    private void waitACoupleSecondsAndAskAgainForJob(DeminerState state) throws InterruptedException{
        for(int i =1; i< 1000000000;i++){}
        state.setTimesWaitingForJob(state.getTimesWaitingForJob()+1);
        requestDemineJob(state);
    }
    
    private void waitACoupleSecondsAndTryToPresetSlot(DeminerState state) throws InterruptedException{
        for(int i =1; i< 1000000000;i++){}
        tryToPreSetSlotInWorldForMovementAndRequest(state);
    }
    
    
    
    private void assignDemineJob(DeminerState state,DeminerActionData data){
        state.setTimesWaitingForJob(0);
        state.setTargetAssigned(true);
        state.setAssignedTargetx(data.getX());
        state.setAssignedTargety(data.getY());
        state.setAssignedTargetEntityType(data.getEntityType());
        state.setOnRest(false);
        changeToReadyStatus(state);
    }
    
    private void doDemineJob(DeminerState state){
        if(state.isTargetAssigned()){
             useDetector(state);
        }else{
            if(state.getStatus().equals(SmadhConstants.STATUS_READY)){
                requestDemineJob(state);
            }
            
        }
    }
    
    private void requestDemineJob(DeminerState state){
        changeToRequestJob(state);        
        DataBESA dataAction;
        dataAction = new ActionData(getAgent().getAlias(), "requestForDemineJob",state.getX(),state.getY());
        requestInfoToControl(dataAction);
    }
    
    private void requestInfoToControl(DataBESA dataAction){
        EventBESA requestInfoEvent = new EventBESA(CommunicationDeminerGuard.class.getName(), dataAction);
        AgHandlerBESA ah;
        try {
            ah = getAgent().getAdmLocal().getHandlerByAlias("CONTROL");
            ah.sendEvent(requestInfoEvent);
        } catch (ExceptionBESA e) {
            ReportBESA.error(e);
        }
    }
    
    private void useDetector(DeminerState state){
        if(state.getStatus().equals(SmadhConstants.STATUS_READY)){
            changeToDetectingStatus(state);
            consumeEnergyPerPeriod(state);
            senseWorld(state);
        }
    }
    
    private void evaluateAndProceed(DeminerActionData data, DeminerState state){
        doWhateverIsPossible(data,state);
        insertPointIntoDronesMap(state);
        changeToReadyStatus(state);
    }
    
    private void doWhateverIsPossible(DeminerActionData data,DeminerState state){

            changeToMovingStatus(state);
            lookForDeminingTarget(data,state);
            if(state.getStatus().equals(SmadhConstants.STATUS_TARGET_AQUIRED)){
                startDemining(state);
            }else{
                lookForFreeSlotToMove(data,state);
            }
            insertPointIntoDronesMap(state);

    }
    
    private void changeToRestStatus(DeminerState state){
        state.setStatus(SmadhConstants.STATUS_REST);
    }
    
    private void changeToDetectingStatus(DeminerState state){
        state.setStatus(SmadhConstants.STATUS_DETECTING);
    }
    
    private void changeToReadyStatus(DeminerState state){
        state.setStatus(SmadhConstants.STATUS_READY);
    }
    
    private void changeToRequestJob(DeminerState state){
        state.setStatus(SmadhConstants.STATUS_REQUESTING_DEMINE_JOB);
    }
    
    private void changeToMovingStatus(DeminerState state){
        state.setStatus(SmadhConstants.STATUS_MOVING);
    }
    
    private void changeToDeminingStatus(DeminerState state){
        state.setStatus(SmadhConstants.STATUS_DEMINING);
    }
    
    private void changeToReturnToBaseStatus(DeminerState state){
        state.setStatus(SmadhConstants.STATUS_RETURNING_TO_BASE);
    }
    
    private void lookForDeminingTarget(DeminerActionData data,DeminerState state){
        List<Slot> slots = data.getSlots();
        for(Slot slot : slots){
            if(SmadhConstants.ENABLE_COOPERATION){
                if(slot.isInhabited() && 
                (slot.getInhabitedType().equals(SmadhConstants.INHABITED_MINET1))){
                state.setTargetx(slot.getX());
                state.setTargety(slot.getY());
                state.setTargetAlias(slot.getEntityAlias());
                state.setTargetEntityType(slot.getEntityType());
                state.setStatus(SmadhConstants.STATUS_TARGET_AQUIRED);
                }
            }else{
                if(slot.isInhabited() && 
                (slot.getInhabitedType().equals(SmadhConstants.INHABITED_MINET1) ||
                slot.getInhabitedType().equals(SmadhConstants.INHABITED_MINET2))){
                state.setTargetx(slot.getX());
                state.setTargety(slot.getY());
                state.setTargetAlias(slot.getEntityAlias());
                state.setTargetEntityType(slot.getEntityType());
                state.setStatus(SmadhConstants.STATUS_TARGET_AQUIRED);
                }
            }
            
        }
    }
    
    private void lookForFreeSlotToMove(DeminerActionData data,DeminerState state){
        List<Slot> slots = data.getSlots();
        slots = slots
                .stream()
                .filter(t-> t.isInhabited()==false)
                .collect(Collectors.toList());
        if(slots.isEmpty()){
            state.setTargetx(state.getX());
            state.setTargety(state.getY());
        }else{
            //look for the best slot to move o demine if is that the case(that one which is a step closer to final target)
            for(Slot slot :slots){
                if(slot.getX()==state.getAssignedTargetx() && slot.getY()==state.getAssignedTargety()){
                    state.setTargetx(slot.getX());
                    state.setTargety(slot.getY());
                    state.setTargetAlias(slot.getEntityAlias());
                    state.setTargetEntityType(slot.getEntityType());
                    state.setStatus(SmadhConstants.STATUS_TARGET_AQUIRED);
                }
            }
            if(state.getStatus().equals(SmadhConstants.STATUS_TARGET_AQUIRED)){
                startDemining(state);
            }else{
                Slot bestSlot=null;
                double initialDistance = 400;
                double distance;
                for(Slot sl :slots){
                        distance=Calculations.calculateDistanceBetweenPoints(state.getAssignedTargetx(), state.getAssignedTargety(),sl.getX(), sl.getY());//ojo aqui
                        if(distance<initialDistance){
                            bestSlot = sl;
                            initialDistance=distance;
                        }
                }
                if(Objects.nonNull(bestSlot)){
                    state.setTargetx(bestSlot.getX());
                    state.setTargety(bestSlot.getY());
                    tryToPreSetSlotInWorldForMovementAndRequest(state);
                }
            }
        }
    }
    
    private void tryToPreSetSlotInWorldForMovementAndRequest(DeminerState state){
        DataBESA dataAction;
        dataAction = new DeminerActionData(getAgent().getAlias(),"tryToPresetSlot",state.getTargetx(),state.getTargety());
        EventBESA senseQueryToWorld = new EventBESA(SenseQueryDeminerGuard.class.getName(),dataAction);
        AgHandlerBESA ah;
        try {
            ah = getAgent().getAdmLocal().getHandlerByAlias("WORLD");
            ah.sendEvent(senseQueryToWorld);
        } catch (ExceptionBESA e) {
          ReportBESA.error(e);
        }
    }
    
    private void consumeEnergyPerPeriod(DeminerState state){
        state.setFuel(state.getFuel()-SmadhConstants.BOTS_DEMINERS_FUEL_CONSUMPTION_PER_PT);
    }
    
    private void moveDroneAndReport(DeminerState state){
        state.setStatus(SmadhConstants.STATUS_MOVING);
        state.setX(state.getTargetx());
        state.setY(state.getTargety());
        DataBESA dataAction;
        dataAction = new ActionData(this.getAgent().getAlias(), "move", state.getX(), state.getY());
        reportAction(dataAction);
        consumeEnergyPerPeriod(state);
        System.out.println("moviendose a: " + state.getX()+";"+ state.getY());
        state.setStatus(SmadhConstants.STATUS_READY);
    }
    
    private void startDemining(DeminerState state){
        changeToDeminingStatus(state);        
        consumeEnergyPerPeriod(state);
        DataBESA dataAction;
        dataAction = new ActionData(this.getAgent().getAlias(), "demine",
                                    state.getTargetx(),state.getTargety(),
                                    state.getTargetEntityType(),state.getActionForce());
        reportAction(dataAction);
    }
    
    private void beReadyForNextSearch(DeminerActionData data, DeminerState state){
        changeToReadyStatus(state);        
        DataBESA dataAction;
        state.setTargetAssigned(false);
        dataAction = new ActionData(data.getAlias(), "cleared",data.getX(),data.getY(),data.getEntityType());
        reportAction(dataAction);
    }
    
    private void insertPointIntoDronesMap(DeminerState state){
        state.setPointToDroneMap();
    }
        
    private Pair<Integer,Integer> getOneStepCloserToOrigin(DeminerState state){
        int newx = state.getX();
        int newy = state.getY(); 
        int destinationx = state.getInitialx();
        int destinationy = state.getInitialy();
        if(destinationx - newx > 0){
            newx = newx + 1;
        }else if (destinationx - newx < 0){
            newx = newx - 1;
        }
        if(destinationy - newy > 0){
            newy = newy + 1;
        }else if(destinationy - newy < 0){
            newy = newy - 1;
        }
        return new Pair(newx,newy);
    }
    
    private void senseWorld(DeminerState state){
        DataBESA dataAction;
        dataAction = new DeminerActionData(getAgent().getAlias(),state.getX(),state.getY(),"sense","All");
        EventBESA senseQueryToWorld = new EventBESA(SenseQueryDeminerGuard.class.getName(),dataAction);
        AgHandlerBESA ah;
        try {
            ah = getAgent().getAdmLocal().getHandlerByAlias("WORLD");
            ah.sendEvent(senseQueryToWorld);
        } catch (ExceptionBESA e) {
          ReportBESA.error(e);
        }
    }
    
    private void reportAction(DataBESA dataAction){
        EventBESA graphicUpdateEvent = new EventBESA(UpdateDeminerGuard.class.getName(), dataAction);
        EventBESA controlReportEvent = new EventBESA(ReportDeminerGuard.class.getName(), dataAction);
        AgHandlerBESA ah;
        try {
            ah = getAgent().getAdmLocal().getHandlerByAlias("WORLD");
            ah.sendEvent(graphicUpdateEvent);
            ah = getAgent().getAdmLocal().getHandlerByAlias("CONTROL");
            ah.sendEvent(controlReportEvent);
        } catch (ExceptionBESA e) {
            ReportBESA.error(e);
        }
    }
    
    private void returnToBase(DeminerState state){
        changeToReturnToBaseStatus(state);
        DataBESA dataAction;
        Pair<Integer,Integer> newStep = getOneStepCloserToOrigin(state);
        double distance=Calculations.calculateDistanceBetweenPoints(state.getInitialx(), state.getInitialy(), state.getX(), state.getY());
        if(distance == 0){
                if(!state.isOnRest()){
                    state.setOnRest(true);
                    changeToRestStatus(state);
                    dataAction = new ActionData(this.getAgent().getAlias(), "rest");
                    reportAction(dataAction);
                }
                
        }else{
            dataAction = new ActionData(this.getAgent().getAlias(), "move", newStep.getKey(), newStep.getValue());
            state.setX(newStep.getKey());
            state.setY(newStep.getValue());
            reportAction(dataAction);
        }
        
    }
    

}
