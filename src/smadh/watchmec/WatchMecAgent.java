 package smadh.watchmec;


import BESA.Kernell.Agent.AgentBESA;
import BESA.Kernell.Agent.KernellAgentExceptionBESA;
import BESA.Kernell.Agent.StateBESA;
import BESA.Kernell.Agent.StructBESA;
import BESA.Log.ReportBESA;
import smadh.watchmec.state.WatchMecState;

/**
 *
 * @author Raxon
 */
public class WatchMecAgent extends AgentBESA {
    
    public WatchMecAgent(String alias, StateBESA state, 
                         StructBESA structAgent, double passwd) 
                         throws KernellAgentExceptionBESA {
        super(alias, state, structAgent, passwd);
    }
    
    @Override
    public void setupAgent() {
        ReportBESA.info("SETUP AGENT -> " + getAlias());
        WatchMecState cs = (WatchMecState)this.getState();
        /*DataBESA data = new SubscribeData(this.getAlias(), initialx, initialy);
        EventBESA event = new EventBESA(SubscribeWatchMecGuard.class.getName(), data);
        AgHandlerBESA ah;
        
        try {
            ah = this.getAdmLocal().getHandlerByAlias("WORLD");
            ah.sendEvent(event);
        } catch (ExceptionBESA e) {
            ReportBESA.error(e);
        }*/
    }

    @Override
    public void shutdownAgent() {
        ReportBESA.info("SHUTDOWN AGENT -> " + getAlias());
    }
}
