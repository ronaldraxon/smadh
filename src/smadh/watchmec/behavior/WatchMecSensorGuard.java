package smadh.watchmec.behavior;


import BESA.ExceptionBESA;
import BESA.Kernell.Agent.Event.DataBESA;
import BESA.Kernell.Agent.Event.EventBESA;
import BESA.Kernell.Agent.GuardBESA;
import BESA.Kernell.Agent.StateBESA;
import BESA.Kernell.System.Directory.AgHandlerBESA;
import BESA.Log.ReportBESA;
import java.util.Map;
import javafx.util.Pair;
import smadh.config.SmadhConstants;
import smadh.control.behavior.ReportWatchMecGuard;
import smadh.data.ActionData;
import smadh.data.WatchMecActionData;
import smadh.strategies.Movements;
import smadh.utils.Calculations;
import smadh.watchmec.state.WatchMecState;
import smadh.world.behavior.watchmecInteractions.SenseQueryWatchMecGuard;
import smadh.world.behavior.watchmecInteractions.UpdateWatchMecGuard;

/**
 *
 * @author Raxon
 */
public class WatchMecSensorGuard extends GuardBESA{

   @Override
    public boolean funcEvalBool(StateBESA objEvalBool) {
        return true;
    }
    
    @Override
    public void funcExecGuard(EventBESA ebesa) { 
        WatchMecActionData data = (WatchMecActionData) ebesa.getData();
        WatchMecState state = (WatchMecState) this.getAgent().getState();
        switch (data.getAction()) {
            case "enable":
                changeToReadyStatus(state);
            break;    
            case "search":
                if(isStatusDifferentThanRest(state) && isStatusDifferentThanClear(state)){
                    if (isEnergyUnder1Point(state)|| isCharging(state)){
                        reChargeDrone(state);
                    }else if(isJobFinished(state)){
                        returnToBase(state);
                    }else{
                        useDetector(state);
                    }
                }
            break;
            case "evaluate":
                if(isStatusDifferentThanRest(state)){
                    if (isEnergyUnder1Point(state)|| isCharging(state)){
                        reChargeDrone(state);
                    }else if(isJobFinished(state)){
                        returnToBase(state);
                    }else{
                        evaluateAndProceed(data,state);
                    }
                }
            break;
            case "clear":
                if(isStatusDifferentThanRest(state)){
                    if (isEnergyUnder1Point(state)|| isCharging(state)){
                        reChargeDrone(state);
                    }else if(isJobFinished(state)){
                        returnToBase(state);
                    }else{
                        startClearing(state);
                    }
                }
            break;
            case "cleared":
                if(isStatusDifferentThanRest(state)){
                    if (isEnergyUnder1Point(state)|| isCharging(state)){
                        reChargeDrone(state);
                    }else if(isJobFinished(state)){
                        returnToBase(state);
                    }else{
                        beReadyForNextSearch(data,state);
                    }
                }
            break;
        }
    }
    
    private boolean isStatusDifferentThanRest(WatchMecState state){
        return !state.getStatus().equals(SmadhConstants.STATUS_REST);
    }
    
    private boolean isStatusDifferentThanClear(WatchMecState state){
        return !state.getStatus().equals(SmadhConstants.STATUS_CLEARING_PERSONS);
    }
    
    private boolean isJobFinished(WatchMecState state){
        return state.getDroneMap().size()>SmadhConstants.BOTS_WATCHMECS_MAX_EXPLORATION_SCORE;
    }
    
    private boolean isEnergyUnder1Point(WatchMecState state){
        return state.getFuel()<1;
    }
    
    private boolean isCharging(WatchMecState state){
        return state.getStatus().equals(SmadhConstants.STATUS_CHARGING);
    }
    
    private void reChargeDrone(WatchMecState state){
        DataBESA dataAction=new ActionData(this.getAgent().getAlias(), "");
        System.out.println(state.getStatus());
        if(!state.getStatus().equals(SmadhConstants.STATUS_CHARGING)){
            state.setStatus(SmadhConstants.STATUS_CHARGING);
            dataAction = new ActionData(this.getAgent().getAlias(), "charging");
        }
        state.setFuel(state.getFuel()+SmadhConstants.BOTS_REFUEL_UNITS_PER_PT);
        if(state.getFuel()>=SmadhConstants.BOTS_WATCHMECS_FUEL_MAX_CAPACITY){
            state.setStatus(SmadhConstants.STATUS_READY);
            dataAction = new ActionData(this.getAgent().getAlias(), "operating");
        }
        
        reportAction(dataAction);
    }
    
    private void useDetector(WatchMecState state){
        if(state.getStatus().equals(SmadhConstants.STATUS_READY) || 
           state.getStatus().equals(SmadhConstants.STATUS_MOVING)){
            changeToDetectingStatus(state);
            consumeEnergyPerPeriod(state);
            senseWorld(state);
        }
    }
    
    private void evaluateAndProceed(WatchMecActionData data, WatchMecState state){
        if(data.getX()==-1){
            goAnywhere(state);
            changeToReadyStatus(state);
        }else{
            getCloseToTarget(data,state);
        }
        insertPointIntoDronesMap(state);
        //changeToReadyStatus(state);
    }
    
    private void goAnywhere(WatchMecState state){
        changeToMovingStatus(state);
        getNewPosition(state);
        consumeEnergyPerPeriod(state);
        moveDroneAndReport(state);
        insertPointIntoDronesMap(state);
    }
    
    private void changeToRestStatus(WatchMecState state){
        state.setStatus(SmadhConstants.STATUS_REST);
    }
    
    private void changeToDetectingStatus(WatchMecState state){
        state.setStatus(SmadhConstants.STATUS_DETECTING);
    }
    
    private void changeToReadyStatus(WatchMecState state){
        state.setStatus(SmadhConstants.STATUS_READY);
    }
    
    private void changeToMovingStatus(WatchMecState state){
        state.setStatus(SmadhConstants.STATUS_MOVING);
    }
    
    private void changeToClearingStatus(WatchMecState state){
        state.setStatus(SmadhConstants.STATUS_CLEARING_PERSONS);
    }
    
    private void changeToReturnToBaseStatus(WatchMecState state){
        state.setStatus(SmadhConstants.STATUS_RETURNING_TO_BASE);
    }
    
    private void getNewPosition(WatchMecState state){
        Map <String,Integer> newPosition = Movements.simpleRandomMovement(state.getX(), state.getY());
        state.setX(newPosition.get("x"));
        state.setY(newPosition.get("y"));
    }
    
    private void consumeEnergyPerPeriod(WatchMecState state){
        state.setFuel(state.getFuel()-SmadhConstants.BOTS_WATCHMECS_FUEL_CONSUMPTION_PER_PT);
    }
    
    private void moveDroneAndReport(WatchMecState state){
        DataBESA dataAction;
        dataAction = new ActionData(this.getAgent().getAlias(), "move", state.getX(), state.getY());
        reportAction(dataAction);
    }
    
    private void getCloseToTarget(WatchMecActionData data, WatchMecState state){
        changeToMovingStatus(state);
        Pair<Integer,Integer> newStep = getOneStepCloserToTarget(data,state);
        
        double distance=Calculations.calculateDistanceBetweenPoints(data.getX(), data.getY(), state.getX(), state.getY());
        if(distance ==0 && !data.isMarkedTarget()){
            state.setTargetx(data.getX());
            state.setTargety(data.getY());
            startClearing(state);
        }else{
            DataBESA dataAction;
            dataAction = new ActionData(this.getAgent().getAlias(), "move", newStep.getKey(), newStep.getValue());
            state.setX(newStep.getKey());
            state.setY(newStep.getValue());
            reportAction(dataAction);
        }
        
        System.out.println("TAMAÑO MAPA "+ state.getDroneMap().size());
    }
    
    private void startClearing(WatchMecState state){
        changeToClearingStatus(state);
        DataBESA dataAction;
        dataAction = new ActionData(this.getAgent().getAlias(), "clear",
                                    state.getTargetx(),state.getTargety(),
                                    state.getTargetEntityType(),state.getActionForce());
        reportAction(dataAction);
    }
    
    private void beReadyForNextSearch(WatchMecActionData data, WatchMecState state){
        changeToReadyStatus(state);        
        DataBESA dataAction;
        dataAction = new ActionData(data.getAlias(), "cleared",data.getX(),data.getY(),data.getEntityType());
        reportAction(dataAction);
    }
    
    private void insertPointIntoDronesMap(WatchMecState state){
        state.setPointToDroneMap();
    }
    
    private Pair<Integer,Integer> getOneStepCloserToTarget(WatchMecActionData data, WatchMecState state){
        int newx = state.getX();
        int newy = state.getY(); 
        int minex = data.getX();
        int miney = data.getY();
        if(minex - newx > 0){
            newx = newx + 1;
        }else if (minex - newx < 0){
            newx = newx - 1;
        }
        if(miney - newy > 0){
            newy = newy + 1;
        }else if(miney - newy < 0){
            newy = newy - 1;
        }
        return new Pair(newx,newy);
    }
    
    private Pair<Integer,Integer> getOneStepCloserToOrigin(WatchMecState state){
        int newx = state.getX();
        int newy = state.getY(); 
        int destinationx = state.getInitialx();
        int destinationy = state.getInitialy();
        if(destinationx - newx > 0){
            newx = newx + 1;
        }else if (destinationx - newx < 0){
            newx = newx - 1;
        }
        if(destinationy - newy > 0){
            newy = newy + 1;
        }else if(destinationy - newy < 0){
            newy = newy - 1;
        }
        return new Pair(newx,newy);
    }
    
    private void senseWorld(WatchMecState state){
        DataBESA dataAction;
        dataAction = new WatchMecActionData(this.getAgent().getAlias(),"sense",state.getX(),state.getY(),
                                                  SmadhConstants.TRACER_DETECTION_RADIUS,"Persons");
        EventBESA senseQueryToWorld = new EventBESA(SenseQueryWatchMecGuard.class.getName(),dataAction);
        AgHandlerBESA ah;
        try {
            ah = getAgent().getAdmLocal().getHandlerByAlias("WORLD");
            ah.sendEvent(senseQueryToWorld);
        } catch (ExceptionBESA e) {
          ReportBESA.error(e);
        }
    }
    
    private void reportAction(DataBESA dataAction){
        EventBESA graphicUpdateEvent = new EventBESA(UpdateWatchMecGuard.class.getName(), dataAction);
        EventBESA controlReportEvent = new EventBESA(ReportWatchMecGuard.class.getName(), dataAction);
        AgHandlerBESA ah;
        try {
            ah = getAgent().getAdmLocal().getHandlerByAlias("WORLD");
            ah.sendEvent(graphicUpdateEvent);
            ah = getAgent().getAdmLocal().getHandlerByAlias("CONTROL");
            ah.sendEvent(controlReportEvent);
        } catch (ExceptionBESA e) {
            ReportBESA.error(e);
        }
    }
    
    private void returnToBase(WatchMecState state){
        changeToReturnToBaseStatus(state);
        Pair<Integer,Integer> newStep = getOneStepCloserToOrigin(state);
        DataBESA dataAction;
        double distance=Calculations.calculateDistanceBetweenPoints(state.getInitialx(), state.getInitialy(), state.getX(), state.getY());
        if(distance == 0){
            changeToRestStatus(state);
            dataAction = new ActionData(this.getAgent().getAlias(), "rest");
        }else{
            dataAction = new ActionData(this.getAgent().getAlias(), "move", newStep.getKey(), newStep.getValue());
            state.setX(newStep.getKey());
            state.setY(newStep.getValue());
        }
        reportAction(dataAction);
    }
    
}
