package smadh.watchmec.behavior;

import smadh.watchmec.behavior.*;
import BESA.ExceptionBESA;
import BESA.Kernell.Agent.Event.DataBESA;
import BESA.Kernell.Agent.Event.EventBESA;
import BESA.Kernell.Agent.GuardBESA;
import BESA.Kernell.Agent.StateBESA;
import BESA.Kernell.System.Directory.AgHandlerBESA;
import BESA.Log.ReportBESA;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import javafx.util.Pair;
import smadh.config.SmadhConstants;
import smadh.control.ExplorationPoint;
import smadh.control.ExplorationRoute;
import smadh.control.behavior.CommunicationWatchMecGuard;
import smadh.control.behavior.ReportWatchMecGuard;
import smadh.data.ActionData;
import smadh.data.WatchMecActionData;
import smadh.strategies.Movements;
import smadh.watchmec.state.WatchMecState;
import smadh.utils.Calculations;
import smadh.world.behavior.watchmecInteractions.SenseQueryWatchMecGuard;
import smadh.world.behavior.watchmecInteractions.UpdateWatchMecGuard;
import smadh.world.model.Slot;

/**
 *
 * @author Raxon
 */
public class WatchMecCooperativeActionGuard extends GuardBESA{

@Override
    public boolean funcEvalBool(StateBESA objEvalBool) {
        return true;
    }
    
    @Override
    public void funcExecGuard(EventBESA ebesa) { 
        WatchMecActionData data = (WatchMecActionData) ebesa.getData();
        WatchMecState state = (WatchMecState) this.getAgent().getState();
        switch (data.getAction()) {
            case "enable":
                changeToReadyStatus(state);
            break;    
            case "search":
                if(isStatusDifferentThanRest(state) && isStatusDifferentThanClear(state)){
                    if (isEnergyUnder1Point(state)|| isCharging(state)){
                        reChargeDrone(state);
                    }else if(isJobFinished(state)){
                        returnToBase(state);
                    }else{
                        useDetector(state);
                    }
                }
                if(!isStatusDifferentThanClear(state) && state.isEvacuating()){
                    startCarryPersonToSafeZone(state);
                }
            break;
            case "assignRoute":
                assignRoute(data,state);
            break;
            case "returnToBase":
                returnToBase(state);
            break;
            case "evaluate":
                if(isStatusDifferentThanRest(state)){
                    if (isEnergyUnder1Point(state)|| isCharging(state)){
                        reChargeDrone(state);
                    }else if(isJobFinished(state)){
                        returnToBase(state);
                    }else{
                        evaluateAndProceed(data,state);
                    }
                }
            break;
            case "clear":
                if(isStatusDifferentThanRest(state)){
                    if (isEnergyUnder1Point(state)|| isCharging(state)){
                        reChargeDrone(state);
                    }else if(isJobFinished(state)){
                        returnToBase(state);
                    }else{
                        startClearing(state);
                    }
                }
            break;
            case "carryPerson":
                if(isStatusDifferentThanRest(state)){
                    if (isEnergyUnder1Point(state)|| isCharging(state)){
                        reChargeDrone(state);
                    }else if(isJobFinished(state)){
                        returnToBase(state);
                    }else{
                        startCarryPersonToSafeZone(state);
                    }
                }
            break;
            case "executeNextMoveToSavePerson":
                 if(isStatusDifferentThanRest(state)){
                    if (isEnergyUnder1Point(state)|| isCharging(state)){
                        reChargeDrone(state);
                    }else if(isJobFinished(state)){
                        returnToBase(state);
                    }else{
                        executeNextMoveToSavePerson(data,state);
                    }
                }   
            break;
            case "cleared":
                if(isStatusDifferentThanRest(state)){
                    if (isEnergyUnder1Point(state)|| isCharging(state)){
                        reChargeDrone(state);
                    }else if(isJobFinished(state)){
                        returnToBase(state);
                    }else{
                        beReadyForNextSearch(data,state);
                    }
                }
            break;
        }
    }
    
    private boolean isStatusDifferentThanRest(WatchMecState state){
        return !state.getStatus().equals(SmadhConstants.STATUS_REST);
    }
    
    private boolean isStatusDifferentThanClear(WatchMecState state){
        return !state.getStatus().equals(SmadhConstants.STATUS_CLEARING_PERSONS);
    }
    
    private boolean isJobFinished(WatchMecState state){
        return state.getDroneMap().size()>SmadhConstants.BOTS_WATCHMECS_MAX_EXPLORATION_SCORE;
    }
    
    private boolean isEnergyUnder1Point(WatchMecState state){
        return state.getFuel()<1;
    }
    
    private boolean isCharging(WatchMecState state){
        return state.getStatus().equals(SmadhConstants.STATUS_CHARGING);
    }
    
    private void changeToRequestRouteStatus(WatchMecState state){
        state.setStatus(SmadhConstants.STATUS_REQUESTING_ROUTE);
    }
    
    private void reChargeDrone(WatchMecState state){
        DataBESA dataAction=new ActionData(this.getAgent().getAlias(), "");
        System.out.println(state.getStatus());
        if(!state.getStatus().equals(SmadhConstants.STATUS_CHARGING)){
            state.setStatus(SmadhConstants.STATUS_CHARGING);
            dataAction = new ActionData(this.getAgent().getAlias(), "charging");
        }
        state.setFuel(state.getFuel()+SmadhConstants.BOTS_REFUEL_UNITS_PER_PT);
        if(state.getFuel()>=SmadhConstants.BOTS_WATCHMECS_FUEL_MAX_CAPACITY){
            state.setStatus(SmadhConstants.STATUS_READY);
            dataAction = new ActionData(this.getAgent().getAlias(), "operating");
        }
        
        reportAction(dataAction);
    }
    
    private void useDetector(WatchMecState state){
        if(state.getStatus().equals(SmadhConstants.STATUS_READY) || 
           state.getStatus().equals(SmadhConstants.STATUS_MOVING)){
            changeToDetectingStatus(state);
            consumeEnergyPerPeriod(state);
            senseWorld(state);
        }
    }
    
    private void assignRoute(WatchMecActionData data, WatchMecState state){
        if(!state.isAssignedToARoute()){
            state.setExplorationRoute(data.getExplorationRoute());
            state.setAssignedToARoute(true);
            changeToReadyStatus(state);
        }
        
    }
    
    private void evaluateAndProceed(WatchMecActionData data, WatchMecState state){
        if(data.getX()==-1){
            if(state.isAssignedToARoute()){
                goToNextRoutePoint(state);
            }else{
                requestRoute(state);
            }
        }else{
            getCloseToTarget(data,state);
        }
        insertPointIntoDronesMap(state);
    }
    
    private void requestRoute(WatchMecState state){
        changeToRequestRouteStatus(state);
        DataBESA dataAction;       
        dataAction = new ActionData(this.getAgent().getAlias(), "requestRoute");
        requestInfoToControl(dataAction);
    }
    
    
    private void requestInfoToControl(DataBESA dataAction){
        EventBESA requestInfoEvent = new EventBESA(CommunicationWatchMecGuard.class.getName(), dataAction);
        AgHandlerBESA ah;
        try {
            ah = getAgent().getAdmLocal().getHandlerByAlias("CONTROL");
            ah.sendEvent(requestInfoEvent);
        } catch (ExceptionBESA e) {
            ReportBESA.error(e);
        }
    }
   
    
    private void setNextRoutePointToTarget(WatchMecState state){
        ExplorationRoute er = state.getExplorationRoute();
        List<ExplorationPoint> explorationPoints = er.getExplorationpPoints();
        ExplorationPoint epoint = explorationPoints.stream()
                                                   .filter(ep-> !ep.isExplored())
                                                   .findFirst().orElse(null);
        if(Objects.isNull(epoint)){
            er.setCompleted(true);
            state.setExplorationRoute(er);
            state.setAssignedToARoute(false);
            DataBESA dataAction;
            dataAction = new ActionData(this.getAgent().getAlias(),"reportCompletedRoute");
            reportCompletedExplorationRoute(dataAction);
            requestRoute(state);
        }else{
            state.setTargetpointIndex(explorationPoints.indexOf(epoint));
            state.setTargetx(epoint.getX());
            state.setTargety(epoint.getY());
        }
                
    }
    
    
    
    private void reportCompletedExplorationRoute(DataBESA dataAction){
        
        EventBESA requestInfoEvent = new EventBESA(CommunicationWatchMecGuard.class.getName(), dataAction);
        AgHandlerBESA ah;
        try {
            ah = getAgent().getAdmLocal().getHandlerByAlias("CONTROL");
            ah.sendEvent(requestInfoEvent);
        } catch (ExceptionBESA e) {
            ReportBESA.error(e);
        }
    }
    
    private void getCloseToRoutePoint(WatchMecState state){
        Pair<Integer,Integer> newStep = getOneStepCloserToTarget(state);
        DataBESA dataAction;
        double distance=Calculations.calculateDistanceBetweenPoints(state.getTargetx(), state.getTargety(), state.getX(), state.getY());
        if(distance ==0){
            dataAction = new ActionData(this.getAgent().getAlias(), "onRoute");
            setPointAsExplored(state);
        }else{
            dataAction = new ActionData(this.getAgent().getAlias(), "move", newStep.getKey(), newStep.getValue());
            state.setX(newStep.getKey());
            state.setY(newStep.getValue());
        }
        reportAction(dataAction);
    }
    
    private void setPointAsExplored(WatchMecState state){
        ExplorationRoute er = state.getExplorationRoute();
        List<ExplorationPoint> explorationPoints = er.getExplorationpPoints();
        ExplorationPoint epoint = explorationPoints.get(state.getTargetpointIndex());
        epoint.setExplored(true);
        explorationPoints.add(state.getTargetpointIndex(), epoint);
        er.setExplorationPoints(explorationPoints);
        state.setExplorationRoute(er);
    }
    
    
    
    private Pair<Integer,Integer> getOneStepCloserToTarget(WatchMecState state){
        int newx = state.getX();
        int newy = state.getY(); 
        int targetX = state.getTargetx();
        int targetY = state.getTargety();
        if(targetX - newx > 0){
            newx = newx + 1;
        }else if (targetX - newx < 0){
            newx = newx - 1;
        }
        if(targetY - newy > 0){
            newy = newy + 1;
        }else if(targetY - newy < 0){
            newy = newy - 1;
        }
        return new Pair(newx,newy);
    }
    
    private void goAnywhere(WatchMecState state){
        changeToMovingStatus(state);
        getNewPosition(state);
        consumeEnergyPerPeriod(state);
        moveDroneAndReport(state);
        insertPointIntoDronesMap(state);
    }
    
    private void changeToRestStatus(WatchMecState state){
        state.setStatus(SmadhConstants.STATUS_REST);
    }
    
    private void changeToDetectingStatus(WatchMecState state){
        state.setStatus(SmadhConstants.STATUS_DETECTING);
    }
    
    private void changeToReadyStatus(WatchMecState state){
        state.setStatus(SmadhConstants.STATUS_READY);
    }
    
    private void changeToMovingStatus(WatchMecState state){
        state.setStatus(SmadhConstants.STATUS_MOVING);
    }
    
    private void changeToClearingStatus(WatchMecState state){
        state.setStatus(SmadhConstants.STATUS_CLEARING_PERSONS);
    }
    
    private void changeToReturnToBaseStatus(WatchMecState state){
        state.setStatus(SmadhConstants.STATUS_RETURNING_TO_BASE);
    }
    
    private void getNewPosition(WatchMecState state){
        Map <String,Integer> newPosition = Movements.simpleRandomMovement(state.getX(), state.getY());
        state.setX(newPosition.get("x"));
        state.setY(newPosition.get("y"));
    }
    
    private void consumeEnergyPerPeriod(WatchMecState state){
        state.setFuel(state.getFuel()-SmadhConstants.BOTS_WATCHMECS_FUEL_CONSUMPTION_PER_PT);
    }
    
    private void moveDroneAndReport(WatchMecState state){
        DataBESA dataAction;
        dataAction = new ActionData(this.getAgent().getAlias(), "move", state.getX(), state.getY());
        reportAction(dataAction);
    }
    
    private void moveDroneOccupiedAndReport(WatchMecState state){
        DataBESA dataAction;
        dataAction = new ActionData(this.getAgent().getAlias(), "moveWithPerson", state.getX(), state.getY());
        reportAction(dataAction);
    }
    
    private void getCloseToTarget(WatchMecActionData data, WatchMecState state){
        changeToMovingStatus(state);
        Pair<Integer,Integer> newStep = getOneStepCloserToTarget(data,state);
        
        double distance=Calculations.calculateDistanceBetweenPoints(data.getX(), data.getY(), state.getX(), state.getY());
        if(distance ==0 && !data.isMarkedTarget()){
            state.setTargetx(data.getX());
            state.setTargety(data.getY());
            startClearing(state);
        }else{
            DataBESA dataAction;
            dataAction = new ActionData(this.getAgent().getAlias(), "move", newStep.getKey(), newStep.getValue());
            state.setX(newStep.getKey());
            state.setY(newStep.getValue());
            reportAction(dataAction);
        }
        
        System.out.println("TAMAÑO MAPA "+ state.getDroneMap().size());
    }
    
    private void startClearing(WatchMecState state){
       //Change the pic and move to the next setp
        changeToClearingStatus(state);
        DataBESA dataAction;
        dataAction = new ActionData(this.getAgent().getAlias(), "startClearing",
                                    state.getTargetx(),state.getTargety(),
                                    state.getTargetEntityType(),state.getActionForce());
        reportAction(dataAction);
    }
    
    private void beReadyForNextSearch(WatchMecActionData data, WatchMecState state){
        changeToReadyStatus(state);        
        DataBESA dataAction;
        dataAction = new ActionData(data.getAlias(), "cleared",data.getX(),data.getY(),data.getEntityType());
        reportAction(dataAction);
    }
    
    private void insertPointIntoDronesMap(WatchMecState state){
        state.setPointToDroneMap();
    }
    
    private Pair<Integer,Integer> getOneStepCloserToTarget(WatchMecActionData data, WatchMecState state){
        int newx = state.getX();
        int newy = state.getY(); 
        int minex = data.getX();
        int miney = data.getY();
        if(minex - newx > 0){
            newx = newx + 1;
        }else if (minex - newx < 0){
            newx = newx - 1;
        }
        if(miney - newy > 0){
            newy = newy + 1;
        }else if(miney - newy < 0){
            newy = newy - 1;
        }
        return new Pair(newx,newy);
    }
    
    private Pair<Integer,Integer> getOneStepCloserToOrigin(WatchMecState state){
        int newx = state.getX();
        int newy = state.getY(); 
        int destinationx = state.getInitialx();
        int destinationy = state.getInitialy();
        if(destinationx - newx > 0){
            newx = newx + 1;
        }else if (destinationx - newx < 0){
            newx = newx - 1;
        }
        if(destinationy - newy > 0){
            newy = newy + 1;
        }else if(destinationy - newy < 0){
            newy = newy - 1;
        }
        return new Pair(newx,newy);
    }
    
    private void senseWorld(WatchMecState state){
        DataBESA dataAction;
        dataAction = new WatchMecActionData(this.getAgent().getAlias(),"sense",state.getX(),state.getY(),
                                                  SmadhConstants.TRACER_DETECTION_RADIUS,"Persons");
        EventBESA senseQueryToWorld = new EventBESA(SenseQueryWatchMecGuard.class.getName(),dataAction);
        AgHandlerBESA ah;
        try {
            ah = getAgent().getAdmLocal().getHandlerByAlias("WORLD");
            ah.sendEvent(senseQueryToWorld);
        } catch (ExceptionBESA e) {
          ReportBESA.error(e);
        }
    }
    
    private void reportAction(DataBESA dataAction){
        EventBESA graphicUpdateEvent = new EventBESA(UpdateWatchMecGuard.class.getName(), dataAction);
        EventBESA controlReportEvent = new EventBESA(ReportWatchMecGuard.class.getName(), dataAction);
        AgHandlerBESA ah;
        try {
            ah = getAgent().getAdmLocal().getHandlerByAlias("WORLD");
            ah.sendEvent(graphicUpdateEvent);
            ah = getAgent().getAdmLocal().getHandlerByAlias("CONTROL");
            ah.sendEvent(controlReportEvent);
        } catch (ExceptionBESA e) {
            ReportBESA.error(e);
        }
    }
    
    private void returnToBase(WatchMecState state){
        changeToReturnToBaseStatus(state);
        Pair<Integer,Integer> newStep = getOneStepCloserToOrigin(state);
        DataBESA dataAction;
        double distance=Calculations.calculateDistanceBetweenPoints(state.getInitialx(), state.getInitialy(), state.getX(), state.getY());
        if(distance == 0){
            changeToRestStatus(state);
            dataAction = new ActionData(this.getAgent().getAlias(), "rest");
        }else{
            dataAction = new ActionData(this.getAgent().getAlias(), "move", newStep.getKey(), newStep.getValue());
            state.setX(newStep.getKey());
            state.setY(newStep.getValue());
        }
        reportAction(dataAction);
    }
    
    private void startCarryPersonToSafeZone(WatchMecState state){
        state.setEvacuating(true);
        senseWorldForSafeSlots(state);
    }
    
    private void senseWorldForSafeSlots(WatchMecState state){
        DataBESA dataAction;
        dataAction = new WatchMecActionData(getAgent().getAlias(),"senseForFreeSlots",state.getX(),state.getY());
        EventBESA senseQueryToWorld = new EventBESA(SenseQueryWatchMecGuard.class.getName(),dataAction);
        AgHandlerBESA ah;
        try {
            ah = getAgent().getAdmLocal().getHandlerByAlias("WORLD");
            ah.sendEvent(senseQueryToWorld);
        } catch (ExceptionBESA e) {
          ReportBESA.error(e);
        }
    }
    
    private void executeNextMoveToSavePerson(WatchMecActionData data, WatchMecState state){
            if(data.isAllowedToMove()){
                state.setX(data.getX());
                state.setY(data.getY());
                moveDroneOccupiedAndReport(state);
                if(state.getX()== state.getInitialx() && state.getY()== state.getInitialy()){
                    clearPersonInSafeArea(state);
                    state.setEvacuating(false);
                    goToNextRoutePoint(state);
                }
            }else{
                lookForFreeSlotToMove(data,state);
            }
    }
    
    private void clearPersonInSafeArea(WatchMecState state){
        changeToReadyStatus(state);        
        DataBESA dataAction;
        dataAction = new ActionData(getAgent().getAlias(), "clearPersonInSafeArea",state.getX(),state.getY());
        reportAction(dataAction);
        reportCleared(state);
    }
    
    
    private void goToNextRoutePoint(WatchMecState state){
        changeToMovingStatus(state);
        setNextRoutePointToTarget(state);
        getCloseToRoutePoint(state);
        //getNewPosition(state);
        consumeEnergyPerPeriod(state);
        //moveDroneAndReport(state);
        insertPointIntoDronesMap(state);
    }
    
    private void lookForFreeSlotToMove(WatchMecActionData data, WatchMecState state){
        List<Slot> slots = data.getSlots();
        try{
        slots = slots
                .stream()
                .filter(t-> t.isInhabited()==false)
                .collect(Collectors.toList());
        if(slots.isEmpty()){
            state.setTargetx(state.getX());
            state.setTargety(state.getY());
        }else{
            //look for the best slot to move o demine if is that the case(that one which is a step closer to final target)
            Slot bestSlot=null;
            double initialDistance = 400;
            double distance;
            for(Slot sl :slots){
                distance=Calculations.calculateDistanceBetweenPoints(state.getInitialx(), state.getInitialy(),sl.getX(), sl.getY());//ojo aqui
                if(distance<initialDistance){
                    bestSlot = sl;
                    initialDistance=distance;
                }
            }
            if(Objects.nonNull(bestSlot)){
                state.setTargetx(bestSlot.getX());
                state.setTargety(bestSlot.getY());
                tryToPreSetSlotInWorldForMovementAndRequest(state);
            }
            
        }
        }catch(NullPointerException e){
            state.setTargetx(state.getX());
            state.setTargety(state.getY());
        }
        
    }
    
    private void tryToPreSetSlotInWorldForMovementAndRequest(WatchMecState state){
        DataBESA dataAction;
        dataAction = new WatchMecActionData(getAgent().getAlias(),"tryToPresetSlot",state.getTargetx(),state.getTargety());
        EventBESA senseQueryToWorld = new EventBESA(SenseQueryWatchMecGuard.class.getName(),dataAction);
        AgHandlerBESA ah;
        try {
            ah = getAgent().getAdmLocal().getHandlerByAlias("WORLD");
            ah.sendEvent(senseQueryToWorld);
        } catch (ExceptionBESA e) {
          ReportBESA.error(e);
        }
    }
    
    private void reportCleared(WatchMecState state){
        changeToReadyStatus(state);        
        DataBESA dataAction;
        dataAction = new ActionData(getAgent().getAlias(), "cleared",state.getX(),state.getY(),1);
        justReportToControl(dataAction);
    }
    
    private void justReportToControl(DataBESA dataAction){
        EventBESA controlReportEvent = new EventBESA(ReportWatchMecGuard.class.getName(), dataAction);
        AgHandlerBESA ah;
        try {
            ah = getAgent().getAdmLocal().getHandlerByAlias("CONTROL");
            ah.sendEvent(controlReportEvent);
        } catch (ExceptionBESA e) {
            ReportBESA.error(e);
        }
    }
}
